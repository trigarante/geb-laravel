@extends('layouts.appLayout')
@section('content')
<div class="container">
    <h3>Pre candidatos</h3><br>
    <p>Selecciona el rango de fechas a.</p>

        <form  id="agendarCitaForm" method="POST" action="/precandidato">
    
        @csrf
        Inicio
        <input type="date" class="form-control" name="fechainicio" id="fechainicio"><br>
        Fin
        <input type="date" class="form-control" name="fechafin" id="fechafin"><br>
        <button type="submit" class="btn btn-success" id="agendar" >Desplegar</button>        
    </form>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#agendarCitaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var modal = $(this)

        modal.find('.modal-body #id').val(id);
    })
</script>
<script type="text/javascript">
    $('#modificarSolicitudModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var nombre = button.data('nombre')
        var apellido_paterno = button.data('apellido_paterno')
        var apellido_materno = button.data('apellido_materno')
        var email = button.data('email')
        var telefono = button.data('telefono')
        var modal = $(this)

        modal.find('.modal-body #id').val(id);
        modal.find('.modal-body #nombre').val(nombre);
        modal.find('.modal-body #apellido_paterno').val(apellido_paterno);
        modal.find('.modal-body #apellido_materno').val(apellido_materno);
        modal.find('.modal-body #email').val(email);
        modal.find('.modal-body #telefono').val(telefono);
    })
</script>
<script type="text/javascript">
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var modal = $(this)

        modal.find('.modal-body #id').val(id);
    })
</script>
@endsection
<!--@include('solicitudes.agendarCita')
@include('solicitudes.update')-->