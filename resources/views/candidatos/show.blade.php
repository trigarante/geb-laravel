@extends('layouts.appLayout')
@section('content')
    <section class="forms mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            Informacion
                            de {{$candidatos->nombre." ".$candidatos->apellido_paterno." ".$candidatos->apellido_materno}}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nombre:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->nombre}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Apellido Paterno:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->apellido_paterno}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Apellido Materno:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->apellido_materno}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Email:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Telefono Movil:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->telefono_movil}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Competencia:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->competencia}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Estatus:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->estado}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Calificacion de Examen:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->calificacion_examen}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Comentarios:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->comentarios}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha de Registro:</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$candidatos->fecha_ingreso}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
