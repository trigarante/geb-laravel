@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Candidatos</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="candidatos_table" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Celular</th>
                                        <th>Correo</th>
                                        <th>Vacante</th>
                                        <th>Competencia</th>
                                        <th>Calificaion de Examen</th>
                                        <th>Estado</th>
                                        <th>Seguimiento</th>
                                        @if(auth()->user()->grupo_usuarios->candidato == 1)
                                            <th>Accion</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($candidatos as $candidato)

                                        <tr>
                                            <td>
                                                <a href="{{route('candidato.view',$candidato->id)}}">{{$candidato->nombre." ".$candidato->apellido_paterno." ".$candidato->apellido_materno}}</a>
                                            </td>
                                            <td>{{$candidato->telefono_movil}}</td>
                                            <td>{{$candidato->email}}</td>
                                            <td>{{$candidato->vacante}}</td>
                                            <td>{{$candidato->estadio_competencia}}</td>
                                            <td>{{$candidato->calificacion_examen}}</td>
                                            <td>{{$candidato->estado_rh}}</td>
                                            <td>
                                                @if($candidato->siguiente == 0 && $candidato->id_tipo == 2)
                                                    @if(auth()->user()->grupo_usuarios->candidato == 1)
                                                        <button class="btn btn-success"
                                                                style="background-color: #1e8435; border-color: #1e8435;"
                                                                id="crearCapacitacion"
                                                                data-id='{{ $candidato->id }}'
                                                                data-toggle="modal"
                                                                data-target="#crearCapacitacionModal">
                                                            Capacitación
                                                        </button>
                                                    @endif
                                                @elseif($candidato->siguiente == 0 && $candidato->id_tipo == 1)
                                                    <a class="btn btn-success"
                                                       style="background-color: #1e8435; border-color: #1e8435;"
                                                       href="{{route('empleado.create',$candidato->id)}}">Crear
                                                        empleado</a>
                                                @endif
                                            </td>
                                            @if(auth()->user()->grupo_usuarios->candidato == 1)
                                                <td>
                                                    <button class="btn btn-warning"
                                                            style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff"
                                                            data-id='{{ $candidato->id }}'
                                                            data-comentarios='{{$candidato->comentarios}}'
                                                            data-calificacion_examen='{{$candidato->calificacion_examen}}'
                                                            data-competencia='{{$candidato->competencia}}'
                                                            data-id_calificacion_competencias='{{$candidato->id_calificacion_competencias}}'
                                                            data-toggle="modal" data-target="#updateCandidatoModal">
                                                        Modificar
                                                    </button>
                                                    <button class="btn btn-info"
                                                            style="background-color: #2b5182; border-color: #2b5182; "
                                                            data-toggle="modal"
                                                            data-target="#deleteModal"
                                                            data-id="{{$candidato->id}}">Cambiar estatus
                                                    </button>
                                                </td>
                                            @endif
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="crearCapacitacionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Seguimiento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="POST" id="formCapacitacion">
                        @csrf
                        <input type="hidden" class="form-control" name="id" id="id">

                        <div class="form-group">
                            <label for="calificacion">Calificacion</label>
                            <input type="text" id="calificacion" class="form-control" maxlength="2" 
                                   name="calificacion">
                            <div id="errorRollplay"></div>
                        </div>

                        <div class="form-group">
                            <label for="comentarios">Comentarios</label>
                            <textarea type="text" id="comentarios" style="text-transform:uppercase" class="md-textarea form-control" rows="3"
                                      name="comentarios"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success" id="save">Crear Capacitacion</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updateCandidatoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modificar Candidato</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="POST" id="form">
                        @csrf
                        <input type="hidden" class="form-control" name="id" id="id">

                        <div class="from-group">
                            <label for="competencia">Calificacion de Competencias</label>
                            <select name="competencia" id="competencia" style="text-transform:uppercase"  class="custom-select">
                                @foreach($competencias as $competencia)
                                    <option value="{{$competencia->id}}">{{$competencia->estadio}}</option>
                                @endforeach
                            </select>
                            <div id="errorCompetencia"></div>
                        </div>

                        <div class="form-group">
                            <label for="calificacion_examen">Calificacion de Examen</label>
                            <input type="text" id="calificacion_examen" class="form-control"  maxlength="2" 
                              name="calificacion_examen">
                            <div id="errorExamen"></div>
                        </div>

                        <div class="form-group">
                            <label for=" comentarios">Comentarios</label>
                            <textarea type="text" id="comentarios" style="text-transform:uppercase"  class="md-textarea form-control" rows="3"
                                      name="comentarios"></textarea>
                        </div>

                        <button type="submit" class="btn btn-success" id="update">Guardar</button>
                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Cambiar Estatus</p>
                </div>
                <div class="modal-body">
                    <form action="{{route('candidato.destroy')}}" method="POST">
                        @csrf
                        <label for="">Seleccione un Estatus</label>
                        <select name="motivo_baja" id="" class="custom-select">
                            @foreach($motivos as $motivo)
                                <option value="{{$motivo->id}}">{{$motivo->estado}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="id" name="id">

                        <button class="btn  btn-success mt-3" type="submit">Guardar</button>
                        <button type="button" class="btn  btn-warning mt-3" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    <script src="{{asset('DataTables/datatables.min.js')}}"></script>
    <script type="text/javascript">
        $('#crearCapacitacionModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
        })
    </script>

    <script type="text/javascript">
        $('#updateCandidatoModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var calificacion_examen = button.data('calificacion_examen')
            var comentarios = button.data('comentarios')
            var competencia = button.data('competencia')
            var id_calificacion_competencias = button.data('id_calificacion_competencias')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #calificacion_examen').val(calificacion_examen);
            modal.find('.modal-body #comentarios').val(comentarios);
            modal.find('.modal-body #competencia').val(id_calificacion_competencias);
        })
    </script>

    <script type="text/javascript">
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
        })
    </script>
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#candidatos_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            jQuery('#update').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ route('candidato.update')}}",
                    method: 'post',
                    data: jQuery('#form').serialize(),
                    beforeSend: function () {
                        $('#upadte').prop("disabled", true);
                    },
                    success: function (result) {
                        if (result.errors) {
                            $('#update').prop("disabled", false);
                            jQuery('.alert-danger').html('');
                            jQuery.each(result.errors, function (key, value) {
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>' + value + '</li>');
                            });
                        }
                        else {
                            jQuery('.alert-danger').hide();
                            $('#updateCandidatoModal').modal('hide');
                            location.reload();
                        }
                    }
                });
            });
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            jQuery('#save').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ route('capacitacion.store')}}",
                    method: 'post',
                    data: jQuery('#formCapacitacion').serialize(),
                    beforeSend: function () {
                        $('#save').prop("disabled", true);
                    },
                    success: function (result) {
                        if (result.errors) {
                            $('#save').prop("disabled", false);
                            jQuery('.alert-danger').html('');
                            jQuery.each(result.errors, function (key, value) {
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>' + value + '</li>');
                            });
                        }
                        else {
                            jQuery('.alert-danger').hide();
                            $('#crearCapacitacionModal').modal('hide');
                            document.location.href = "{!! route('capacitacion.index'); !!}";
                        }
                    }
                });
            });
        });
    </script>
@endsection
