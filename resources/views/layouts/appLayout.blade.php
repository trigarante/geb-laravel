<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GEB</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{asset('css/fontastic.css')}}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{asset('css/grasp_mobile_progress_circle-1.0.0.min.css')}}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css')}}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{asset('css/style.default.css')}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@yield('styles')
<!-- Favicon-->
    <!--<link rel="shortcut icon" href="img/favicon.ico">-->
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <h2 class="h5">{{ auth()->user()->usuario }}</h2><span>Web Developer</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center">
                    <strong>GE</strong><strong class="text-primary">B</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Menu</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                @if(auth()->user()->grupo_usuarios->vacantes != 0)
                    <li><a href="{{route('solicitud.index')}}"> <i class="icon-home"></i>Solicitudes </a></li>
                @endif
                @if(auth()->user()->grupo_usuarios->precandidatos != 0)
                    <li><a href="{{route('preCandidatos.index')}}"> <i class="icon-form"></i>Pre Candidatos </a></li>
                @endif
                @if(auth()->user()->grupo_usuarios->candidato != 0)
                    <li><a href="{{route('candidato.index')}}"> <i class="fa fa-bar-chart"></i>Candidatos </a></li>
                @endif
                <li><a href="{{route('capacitacion.index')}}"> <i class="fa fa-bar-chart"></i>Capacitación </a></li>
                @if(auth()->user()->grupo_usuarios->seguimiento != 0)
                    <li><a href="{{route('seguimiento.index')}}"> <i class="icon-grid"></i>Seguimiento</a></li>
                @endif
                @if(auth()->user()->grupo_usuarios->empleados != 0)
                    <li><a href="{{route('empleado.index')}}"> <i class="icon-grid"></i>Empleados</a></li>
                @endif
                    <li><a href="{{route('asignacion.index')}}"> <i class="icon-grid"></i>Asignaciones</a></li>

                <li><a href="{{route('superUsuario.grupoIndex')}}"> <i class="icon-grid"></i>Grupos</a></li>

                @if(auth()->user()->grupo_usuarios->catalogos != 0)
                    <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i
                                class="icon-interface-windows"></i>Catalogos</a>
                        <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                            <li>
                                <a href="{{route('catalogos.area.index')}}">Area de Trabajo</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.bolsa.index')}}">Bolsa de Trabajo</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.competencias.index')}}">Competencias</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.empresa.index')}}">Empresa</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.escolaridad.index')}}">Escolaridad</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.etdoCivil.index')}}">Estado Civil</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.etdoEscolar.index')}}">Estado Escolar</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.etdoMotivo.index')}}">Estado Motivo</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.puesto.index')}}">Puesto</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.tipoPuesto.index')}}">Tipo de Puesto</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.pais.index')}}">Pais</a>
                            </li>
                        </ul>
                    </li>
                @endif

                @if(auth()->user()->grupo_usuarios->reportes != 0)
                <li>
                        <a href="#reporteSubmenu" data-toggle="collapse" aria-expanded="false" data-toggle="collapse"><i class="fa fa-bar-chart"></i>Reportes</a>
                        <ul class="collapse list-unstyled" id="reporteSubmenu"> 
                            <li>
                                <a href="{{route('precandidato_reporte.indexreportefechas')}}">Precandidato</a>
                            </li>
                            <li>
                                <a href="{{route('candidatos_reporte.indexreportefechas')}}">Candidato</a>
                            </li>
                            <li>
                                <a href="{{route('empleado_reporte.indexreporteantiguedad')}}">Antigüedad Empleado</a>
                            </li>
                            <li>
                                <a href="{{route('empleado_reporte.indexreportedemografico')}}">Demográfico Empleado</a>
                            </li>
                            <li>
                                <a href="{{route('empleado_reporte.indexreporteimss')}}">Empleados Alta IMSS</a>
                            </li>
                            <li>
                                <a href="{{route('empleado_reporte.indexreporteseguimiento')}}">Seguimiento</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.tipoDocumento.index')}}">Tipo de Documento</a>
                            </li>
                            <li>
                                <a href="{{route('catalogos.vacante.index')}}">Vacantes</a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if(auth()->user()->grupo_usuarios->inventarios != 0)
                    <li>
                        <a href="#asignacionSubmenu" data-toggle="collapse" aria-expanded="false"> <i
                                class="icon-grid"></i>Asignación Equipo</a>
                        <ul class="collapse list-unstyled" id="asignacionSubmenu">


                            @if(auth()->user()->grupo_usuarios->inventarios != 0)

                                <li>
                                    <a href="{{route('inventario.index')}}">Asignación Equipo Ejecutivos</a>
                                </li>
                            @endif
                            <li>
                               <a href="{{route('inventario.admin.index')}}">Asignación Equipo Administrativos</a>
                            </li>
                            
                            <li>
                                <a href="{{route('inventario.monitor.index')}}">Monitor</a>
                            </li>
                            <li>
                                <a href="{{route('inventario.cpu.index')}}">CPU</a>
                            </li>
                            <li>
                                <a href="{{route('inventario.teclado.index')}}">Teclado</a>
                            </li>
                            <li>
                                <a href="{{route('inventario.mouse.index')}}">Mouse</a>
                            </li>
                            <li>
                                <a href="{{route('inventario.tablet.index')}}">Tablet</a>
                            </li>
                            <li>
                                <a href="{{route('inventario.diadema.index')}}">Diadema</a>
                            </li>
                        </ul>
                    </li>
                @endif

            </ul>
        </div>

    </div>
</nav>
<div class="page">
    <!-- navbar-->
    <header class="header">
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i
                                class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
                            <div class="brand-text d-none d-md-inline-block"><strong class="text-primary">GEB</strong>
                            </div>
                        </a></div>
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <!-- Notifications dropdown-->
                        <li class="nav-item dropdown"><a id="notifications" rel="nofollow" data-target="#" href="#"
                                                         data-toggle="dropdown" aria-haspopup="true"
                                                         aria-expanded="false" class="nav-link"><i
                                    class="fa fa-bell"></i><span class="badge badge-warning">12</span></a>
                            <ul aria-labelledby="notifications" class="dropdown-menu">
                                <li><a rel="nofollow" href="#" class="dropdown-item">
                                        <div class="notification d-flex justify-content-between">
                                            <div class="notification-content"><i class="fa fa-envelope"></i>You have 6
                                                new
                                                messages
                                            </div>
                                            <div class="notification-time">
                                                <small>4 minutes ago</small>
                                            </div>
                                        </div>
                                    </a></li>
                                <li><a rel="nofollow" href="#" class="dropdown-item">
                                        <div class="notification d-flex justify-content-between">
                                            <div class="notification-content"><i class="fa fa-twitter"></i>You have 2
                                                followers
                                            </div>
                                            <div class="notification-time">
                                                <small>4 minutes ago</small>
                                            </div>
                                        </div>
                                    </a></li>
                                <li><a rel="nofollow" href="#" class="dropdown-item">
                                        <div class="notification d-flex justify-content-between">
                                            menu
                                            <div class="notification-content"><i class="fa fa-upload"></i>Server
                                                Rebooted
                                            </div>
                                            <div class="notification-time">
                                                <small>4 minutes ago</small>
                                            </div>
                                        </div>
                                    </a></li>
                                <li><a rel="nofollow" href="#" class="dropdown-item">
                                        <div class="notification d-flex justify-content-between">
                                            <div class="notification-content"><i class="fa fa-twitter"></i>You have 2
                                                followers
                                            </div>
                                            <div class="notification-time">
                                                <small>10 minutes ago</small>
                                            </div>
                                        </div>
                                    </a></li>
                                <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center">
                                        <strong> <i class="fa fa-bell"></i>view all notifications </strong></a></li>
                            </ul>
                        </li>
                        <!-- Messages dropdown-->
                        <li class="nav-item dropdown"><a id="messages" rel="nofollow" data-target="#" href="#"
                                                         data-toggle="dropdown" aria-haspopup="true"
                                                         aria-expanded="false" class="nav-link"><i
                                    class="fa fa-envelope"></i><span class="badge badge-info">10</span></a>
                            <ul aria-labelledby="notifications" class="dropdown-menu">
                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                                        <div class="msg-profile"><img src="img/avatar-1.jpg" alt="..."
                                                                      class="img-fluid rounded-circle"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Jason Doe</h3><span>sent you a direct message</span>
                                            <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </a></li>
                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                                        <div class="msg-profile"><img src="img/avatar-2.jpg" alt="..."
                                                                      class="img-fluid rounded-circle"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Frank Williams</h3><span>sent you a direct message</span>
                                            <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </a></li>
                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                                        <div class="msg-profile"><img src="img/avatar-3.jpg" alt="..."
                                                                      class="img-fluid rounded-circle"></div>
                                        <div class="msg-body">
                                            <h3 class="h5">Ashley Wood</h3><span>sent you a direct message</span>
                                            <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </a></li>
                                <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center">
                                        <strong> <i class="fa fa-envelope"></i>Read all messages </strong></a></li>
                            </ul>
                        </li>
                        <!-- Languages dropdown    -->
                        <!-- Log out-->
                        <li class="nav-item"><a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                                class="nav-link logout"> <span
                                    class="d-none d-sm-inline-block">Cerrar sesion</span><i class="fa fa-sign-out"></i></a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- Counts Section -->
    @yield('content')
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>Ahorra Seguros &copy; 2018-2019</p>
                </div>
                <div class="col-sm-6 text-right">
                    <p>Design by Nexos Media</p>
                    <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- JavaScript files-->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/popper.js/umd/popper.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/grasp_mobile_progress_circle-1.0.0.min.js')}}"></script>
<script src="{{asset('vendor/jquery.cookie/jquery.cookie.js')}}"></script>
<script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{asset('js/charts-home.js')}}"></script>
<!-- Main File-->
<script src="{{asset('js/front.js')}}"></script>
@yield('scripts')
</body>
</html>
