<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/styleSide.css')}}">
    <link rel="stylesheet" href="{{asset('mdb/css/mdb.min.css')}}">
    @yield('styles')
    

    <title>GEB</title>
  </head>
  <body>
    

  @yield('content')  

  <!-- SideNav slide-out button -->

<!--/. Sidebar navigation -->
  
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('mdb/js/mdb.min.js')}}">
    </script>
  @yield('scripts')
  </body>
</html>