@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reporte Precandidato
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('reporte_precandidatos.show_fields')
                    <a href="{!! route('reportePrecandidatos.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
