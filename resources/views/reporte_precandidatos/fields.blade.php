<!-- Id Solicitud Rrhh Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_solicitud_rrhh', 'Id Solicitud Rrhh:') !!}
    {!! Form::number('id_solicitud_rrhh', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Imagen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_imagen', 'Id Imagen:') !!}
    {!! Form::number('id_imagen', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellido Paterno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apellido_paterno', 'Apellido Paterno:') !!}
    {!! Form::text('apellido_paterno', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellido Materno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apellido_materno', 'Apellido Materno:') !!}
    {!! Form::text('apellido_materno', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Nacimiento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento:') !!}
    {!! Form::date('fecha_nacimiento', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Genero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('genero', 'Genero:') !!}
    {!! Form::text('genero', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Estado Civil Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_estado_civil', 'Id Estado Civil:') !!}
    {!! Form::number('id_estado_civil', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Escolaridad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_escolaridad', 'Id Escolaridad:') !!}
    {!! Form::number('id_escolaridad', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Etdo Escolaridad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_etdo_escolaridad', 'Id Etdo Escolaridad:') !!}
    {!! Form::number('id_etdo_escolaridad', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Pais Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pais', 'Id Pais:') !!}
    {!! Form::number('id_pais', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cp', 'Cp:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Colonia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('colonia', 'Colonia:') !!}
    {!! Form::text('colonia', null, ['class' => 'form-control']) !!}
</div>

<!-- Calle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('calle', 'Calle:') !!}
    {!! Form::text('calle', null, ['class' => 'form-control']) !!}
</div>

<!-- Numero Exterior Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero_exterior', 'Numero Exterior:') !!}
    {!! Form::text('numero_exterior', null, ['class' => 'form-control']) !!}
</div>

<!-- Numero Interior Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero_interior', 'Numero Interior:') !!}
    {!! Form::text('numero_interior', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Fijo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono_fijo', 'Telefono Fijo:') !!}
    {!! Form::number('telefono_fijo', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Movil Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono_movil', 'Telefono Movil:') !!}
    {!! Form::number('telefono_movil', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Estado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_estado', 'Id Estado:') !!}
    {!! Form::number('id_estado', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Estado Motivo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_estado_motivo', 'Id Estado Motivo:') !!}
    {!! Form::number('id_estado_motivo', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Creacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_creacion', 'Fecha Creacion:') !!}
    {!! Form::date('fecha_creacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Baja Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_baja', 'Fecha Baja:') !!}
    {!! Form::date('fecha_baja', null, ['class' => 'form-control']) !!}
</div>

<!-- Tiempo Traslado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tiempo_traslado', 'Tiempo Traslado:') !!}
    {!! Form::number('tiempo_traslado', null, ['class' => 'form-control']) !!}
</div>

<!-- Medio Traslado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('medio_traslado', 'Medio Traslado:') !!}
    {!! Form::text('medio_traslado', null, ['class' => 'form-control']) !!}
</div>

<!-- Metro Cercano Field -->
<div class="form-group col-sm-6">
    {!! Form::label('metro_cercano', 'Metro Cercano:') !!}
    {!! Form::text('metro_cercano', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('reportePrecandidatos.index') !!}" class="btn btn-default">Cancel</a>
</div>
