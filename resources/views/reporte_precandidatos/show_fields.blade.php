<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $reportePrecandidato->id !!}</p>
</div>

<!-- Id Solicitud Rrhh Field -->
<div class="form-group">
    {!! Form::label('id_solicitud_rrhh', 'Id Solicitud Rrhh:') !!}
    <p>{!! $reportePrecandidato->id_solicitud_rrhh !!}</p>
</div>

<!-- Id Imagen Field -->
<div class="form-group">
    {!! Form::label('id_imagen', 'Id Imagen:') !!}
    <p>{!! $reportePrecandidato->id_imagen !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $reportePrecandidato->nombre !!}</p>
</div>

<!-- Apellido Paterno Field -->
<div class="form-group">
    {!! Form::label('apellido_paterno', 'Apellido Paterno:') !!}
    <p>{!! $reportePrecandidato->apellido_paterno !!}</p>
</div>

<!-- Apellido Materno Field -->
<div class="form-group">
    {!! Form::label('apellido_materno', 'Apellido Materno:') !!}
    <p>{!! $reportePrecandidato->apellido_materno !!}</p>
</div>

<!-- Fecha Nacimiento Field -->
<div class="form-group">
    {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento:') !!}
    <p>{!! $reportePrecandidato->fecha_nacimiento !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $reportePrecandidato->email !!}</p>
</div>

<!-- Genero Field -->
<div class="form-group">
    {!! Form::label('genero', 'Genero:') !!}
    <p>{!! $reportePrecandidato->genero !!}</p>
</div>

<!-- Id Estado Civil Field -->
<div class="form-group">
    {!! Form::label('id_estado_civil', 'Id Estado Civil:') !!}
    <p>{!! $reportePrecandidato->id_estado_civil !!}</p>
</div>

<!-- Id Escolaridad Field -->
<div class="form-group">
    {!! Form::label('id_escolaridad', 'Id Escolaridad:') !!}
    <p>{!! $reportePrecandidato->id_escolaridad !!}</p>
</div>

<!-- Id Etdo Escolaridad Field -->
<div class="form-group">
    {!! Form::label('id_etdo_escolaridad', 'Id Etdo Escolaridad:') !!}
    <p>{!! $reportePrecandidato->id_etdo_escolaridad !!}</p>
</div>

<!-- Id Pais Field -->
<div class="form-group">
    {!! Form::label('id_pais', 'Id Pais:') !!}
    <p>{!! $reportePrecandidato->id_pais !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $reportePrecandidato->cp !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $reportePrecandidato->colonia !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $reportePrecandidato->calle !!}</p>
</div>

<!-- Numero Exterior Field -->
<div class="form-group">
    {!! Form::label('numero_exterior', 'Numero Exterior:') !!}
    <p>{!! $reportePrecandidato->numero_exterior !!}</p>
</div>

<!-- Numero Interior Field -->
<div class="form-group">
    {!! Form::label('numero_interior', 'Numero Interior:') !!}
    <p>{!! $reportePrecandidato->numero_interior !!}</p>
</div>

<!-- Telefono Fijo Field -->
<div class="form-group">
    {!! Form::label('telefono_fijo', 'Telefono Fijo:') !!}
    <p>{!! $reportePrecandidato->telefono_fijo !!}</p>
</div>

<!-- Telefono Movil Field -->
<div class="form-group">
    {!! Form::label('telefono_movil', 'Telefono Movil:') !!}
    <p>{!! $reportePrecandidato->telefono_movil !!}</p>
</div>

<!-- Id Estado Field -->
<div class="form-group">
    {!! Form::label('id_estado', 'Id Estado:') !!}
    <p>{!! $reportePrecandidato->id_estado !!}</p>
</div>

<!-- Id Estado Motivo Field -->
<div class="form-group">
    {!! Form::label('id_estado_motivo', 'Id Estado Motivo:') !!}
    <p>{!! $reportePrecandidato->id_estado_motivo !!}</p>
</div>

<!-- Fecha Creacion Field -->
<div class="form-group">
    {!! Form::label('fecha_creacion', 'Fecha Creacion:') !!}
    <p>{!! $reportePrecandidato->fecha_creacion !!}</p>
</div>

<!-- Fecha Baja Field -->
<div class="form-group">
    {!! Form::label('fecha_baja', 'Fecha Baja:') !!}
    <p>{!! $reportePrecandidato->fecha_baja !!}</p>
</div>

<!-- Tiempo Traslado Field -->
<div class="form-group">
    {!! Form::label('tiempo_traslado', 'Tiempo Traslado:') !!}
    <p>{!! $reportePrecandidato->tiempo_traslado !!}</p>
</div>

<!-- Medio Traslado Field -->
<div class="form-group">
    {!! Form::label('medio_traslado', 'Medio Traslado:') !!}
    <p>{!! $reportePrecandidato->medio_traslado !!}</p>
</div>

<!-- Metro Cercano Field -->
<div class="form-group">
    {!! Form::label('metro_cercano', 'Metro Cercano:') !!}
    <p>{!! $reportePrecandidato->metro_cercano !!}</p>
</div>

