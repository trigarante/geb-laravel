@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Seguimiento</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="seguimiento_table" class="table table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Calificacion Rollplay</th>
                                        <th>Comentarios</th>
                                        <th>Estado</th>
                                        <th>Candidato</th>
                                        @if(auth()->user()->grupo_usuarios->seguimiento == 1)
                                            <th>Accion</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($seguimientos as $seguimiento)
                                        <tr>
                                            <td>
                                                <a href="{{route('seguimiento.view',$seguimiento->id)}}">{{$seguimiento->nombre." ".$seguimiento->apellido_paterno." ".$seguimiento->apellido_materno}}</a>
                                            </td>
                                            <td>{{$seguimiento->telefono_movil}}</td>
                                            <td>{{$seguimiento->email}}</td>
                                            <td>{{$seguimiento->calificacion_rollplay}}</td>
                                            <td>{{$seguimiento->comentarios}}</td>
                                            <td>{{$seguimiento->estado}}</td>
                                            <td>
                                                    @if(auth()->user()->grupo_usuarios->seguimiento == 1)
                                                        <a class="btn btn-success"
                                                           style="background-color: #1e8435; border-color: #1e8435;"
                                                           href="{{route('empleado.create',$seguimiento->id_candidato)}}">Crear
                                                            empleado</a>
                                                    @endif
                                            </td>
                                            @if(auth()->user()->grupo_usuarios->vacantes == 1)
                                                <td>
                                                    <button class="btn btn-warning"
                                                            style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff"
                                                            data-id='{{ $seguimiento->id }}'
                                                            data-toggle="modal" data-target="#updateSeguimientoModal"
                                                            data-calificacion='{{$seguimiento->calificacion_rollplay}}'
                                                            data-comentarios='{{$seguimiento->comentarios}}'>Modificar
                                                    </button>
                                                    <button class="btn btn-info"
                                                            style="background-color: #2b5182; border-color: #2b5182; "
                                                            data-toggle="modal"
                                                            data-target="#deleteModal"
                                                            data-id="{{$seguimiento->id}}">Cambiar estatus
                                                    </button>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="updateSeguimientoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Seguimiento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="POST" id="form">
                        @csrf
                        <input type="hidden" class="form-control" name="id" id="id"><br>
                        <div class="form-group">
                            <label for="calificacion">Calificacion Rolllplay</label>
                            <input type="text" id="calificacion" class="form-control"  name="calificacion" maxlength="2">
                            <div id="calificacionError"></div>
                        </div>
                        <div class="from-group">
                            <label for="comentarios">Comentarios</label>
                            <textarea type="text" id="comentarios" style="text-transform:uppercase" class="form-control" rows="3"
                                      name="comentarios"></textarea>
                        </div>


                        <button type="submit" class="btn btn-success" id="update">Actualizar Seguimiento</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div> 

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Cambiar Estatus</p>
                </div>
                <div class="modal-body">
                    <form action="{{route('seguimiento.destroy')}}" method="POST">
                        @csrf
                        <label for="motivo_baja">Seleccione un Estatus</label>
                        <select name="motivo_baja" id="motivo_baja" class="custom-select">
                            @foreach($motivos as $motivo)
                                <option value="{{$motivo->id}}">{{$motivo->estado}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="id" name="id">

                        <button class="btn  btn-success mt-3" type="submit">Guardar</button>
                        <button type="button" class="btn  btn-warning mt-3" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('DataTables/datatables.min.js')}}"></script>
    <script type="text/javascript">
        $('#updateSeguimientoModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var calificacion = button.data('calificacion')
            var comentarios = button.data('comentarios')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #calificacion').val(calificacion);
            modal.find('.modal-body #comentarios').val(comentarios);
        })
    </script>

    <script type="text/javascript">
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
        })
    </script>
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#seguimiento_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>


    <script>
        jQuery(document).ready(function () {
            jQuery('#update').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ route('seguimiento.update')}}",
                    method: 'post',
                    data: jQuery('#form').serialize(),
                    beforeSend: function () {
                        $('#update').prop("disabled", true);
                    },
                    success: function (result) {
                        if (result.errors) {
                            $('#update').prop("disabled", false);
                            jQuery('.alert-danger').html('');
                            jQuery.each(result.errors, function (key, value) {
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>' + value + '</li>');
                            });
                        }
                        else {
                            jQuery('.alert-danger').hide();
                            $('#updateSeguimientoModal').modal('hide');
                            location.reload();
                        }
                    }
                });
            });
        });
    </script>
@endsection
