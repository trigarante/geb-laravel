@extends('layouts.appLayout')
@section('content')
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12"> 
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            Informacion
                            de {{$seguimiento->nombre." ".$seguimiento->apellido_paterno." ".$seguimiento->apellido_materno}}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" style="text-transform:uppercase" class="form-control" disabled
                                               value="{{$seguimiento->nombre}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Apellido Paterno</label>
                                        <input type="text" style="text-transform:uppercase" class="form-control" disabled
                                               value="{{$seguimiento->apellido_paterno}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Apellido Materno</label>
                                        <input type="text" style="text-transform:uppercase" class="form-control" disabled
                                               value="{{$seguimiento->apellido_materno}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$seguimiento->email}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Telefono Movil</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$seguimiento->telefono_movil}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Calificacion Rollplay</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$seguimiento->calificacion}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Comentarios</label>
                                        <input type="text" style="text-transform:uppercase" class="form-control" disabled
                                               value="{{$seguimiento->comentarios}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha De Registro</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$seguimiento->fecha_ingreso}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
