<div class="modal fade" id="updateSeguimientoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar Seguimiento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="seguimiento/update" method="POST">
          @csrf
          <input type="hidden" class="form-control" name="id" id="id"><br>
          <div class="md-form">
              <input type="text" id="calificacion" class="form-control" name="calificacion">
          </div>
          <div class="md-form">
              <textarea type="text" id="comentarios" class="md-textarea form-control" rows="3" name="comentarios"></textarea>
          </div>
          <button type="submit" class="btn btn-success" id="guardar">Actualizar Seguimiento</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>