@extends('layouts.appLayout')
@section('content')
    <section class="forms">
        <div class="container-fluid">
            <div class="row justify-content-center mt-3">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            Informacion
                            de {{$precandidato->nombre." ".$precandidato->apellido_paterno." ".$precandidato->apellido_materno}}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->nombre}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Apellido Paterno</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->apellido_paterno}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Apellido Materno</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->apellido_materno}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Fecha de Nacimiento</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->fecha_nacimiento}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Email</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Genero</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->genero}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Estado Civil</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->ecivil}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Escolaridad</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->escolaridad}}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Estado Escolar</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->estado_escolar}}" style="text-transform:uppercase">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="nombre">Pais</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->pais}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Codigo Postal</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->cp}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Estado</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->estado}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Delegacion o Muncipio</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->del_mun}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Ciudad</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->ciudad}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Colonia</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->colonia}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Calle</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->calle}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Numero Exterior</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->numero_exterior}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Numero Interior</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->numero_interior}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="nombre">Telefono Fijo</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->telefono_fijo}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Telefono Movil</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->telefono_movil}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Estatus</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->motivo}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Medio de Transporte</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->medio}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Estacion mas Cercana</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->estacion}}" style="text-transform:uppercase">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Tiempo de Traslado</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->tiempo_traslado}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre">Fecha de Registro</label>
                                        <input type="text" class="form-control" disabled
                                               value="{{$precandidato->fecha_creacion}}">
                                    </div>
                                    <div class="img-fluid">
                                        <img src="{{asset('images/'.$precandidato->imagen)}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<label for="">Estado:</label>--}}
    {{--{{$precandidato->estado}} <br>--}}
    {{--<label for="">Estatus:</label>--}}
    {{--{{$precandidato->motivo}} <br>--}}
    {{--<label for="">Fecha de Creacion:</label>--}}
    {{--{{$precandidato->fecha_creacion}} <br>--}}
    {{--<label for="">Tiempo de Traslado:</label>--}}
    {{--{{$precandidato->tiempo_traslado}} <br>--}}
    {{--<label for="">Medio de Traslado:</label>--}}
    {{--{{$precandidato->medio_traslado}} <br>--}}
    {{--<label for="">Metro mas Cercano:</label>--}}
    {{--{{$precandidato->metro_cercano}} <br>--}}
    {{--<label for="">Foto:</label> <br>--}}
    {{--<img src="{{asset($precandidato->imagen)}}" alt="" class="img-fluid" style="width: 300px;">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection
