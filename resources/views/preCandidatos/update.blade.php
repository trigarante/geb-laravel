@extends('layouts.appLayout')
@section('styles')
    <link rel="stylesheet" href="{{asset('/css/bootstrap-combobox.css')}}">
@endsection
@section('content')

    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('preCandidatos.index')}}">Pre Candidatos</a></li>
                <li class="breadcrumb-item active">Registrar Pre Candidato</li>
            </ul>
        </div>
    </div>
    {{--errores--}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <section class="forms">
        <div class="container-fluid">
            <div class="row justify-content-center mt-3">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h4>Crear Candidato</h4>
                        </div>
                        <div class="card-body">
                            <p>Todos los campos son obligatorios.</p>
                            {!! Form::model($precandidatos,['route' => 'preCandidatos.update','method' => 'POST']) !!}
                            {!! Form::token() !!}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('nombre', '') !!}
                                        {!! Form::text('nombre',null,array('class'=>'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('apellido_paterno') !!}
                                        {!! Form::text('apellido_paterno',null,array('class'=>'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('apellido_materno') !!}
                                        {!! Form::text('apellido_materno',null,array('class'=>'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('correo') !!}
                                        {!! Form::text('email',null,array('class'=>'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('fecha_de_nacimiento') !!}
                                        {!! Form::date('fecha_nacimiento',null,array('class'=>'form-control')) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('genero') !!}
                                        <div class="custom-control custom-radio">
                                            {!! Form::radio('genero', 'M', null,array('class'=>'custom-control-input','id'=>'M')) !!}
                                            {!! Form::label('masculino',null,['class' => 'custom-control-label']) !!}
                                        </div>
                                        <div class="custom-control custom-radio">
                                            {!! Form::radio('genero', 'F', null,array('class'=>'custom-control-input','id'=>'F')) !!}
                                            {!! Form::label('femenino',null,['class' => 'custom-control-label']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('estado_civil') !!}
                                        {!!Form::select('id_estado_civil',$estadoCivil,null, array('class'=>'custom-select'))!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('escolaridad') !!}
                                        {!!Form::select('id_escolaridad',$escolaridad,null, array('class'=>'custom-select'))!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('estado_de_escolaridad') !!}
                                        {!!Form::select('id_etdo_escolaridad',$estadoEscolar,null, array('class'=>'custom-select'))!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('pais') !!}
                                        {!!Form::select('id_pais',$paises,null, array('class'=>'custom-select'))!!}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('codigo_postal') !!}
                                        {!! Form::text('cp',null,array('class'=>'form-control','id'=>'cp')) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="colonia">Colonia</label>
                                        <select class="custom-select" name="colonia" id="colonia" >
                                            <option value="{{$precandidatos->colonia}}" selected
                                                    >{{$precandidatos->colonia}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('calle') !!}
                                        {!! Form::text('calle',null,array('class'=>'form-control','id'=>'cp')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('numero_interior') !!}
                                        {!! Form::text('numero_interior',null,array('class'=>'form-control','id'=>'cp')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('numero_exterior') !!}
                                        {!! Form::text('numero_exterior',null,array('class'=>'form-control','id'=>'cp')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('telefono_fijo') !!}
                                        {!! Form::text('telefono_fijo',null,array('class'=>'form-control','id'=>'cp')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('telefono_movil') !!}
                                        {!! Form::text('telefono_movil',null,array('class'=>'form-control','id'=>'cp')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('tiempo_traslado') !!}
                                        {!! Form::text('tiempo_traslado',null,array('class'=>'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('medio_de_traslado') !!}
                                        {!!Form::select('id_medio_traslado',$medios,null, array('class'=>'custom-select','id'=>'medioTraslado'))!!}
                                    </div>
                                    {!! Form::hidden('estado',null,array('class'=>'form-control','id'=>'estado')) !!}
                                    {!! Form::hidden('del_mun',null,array('class'=>'form-control','id'=>'del_mun')) !!}
                                    {!! Form::hidden('ciudad',null,array('class'=>'form-control','id'=>'ciudad')) !!}
                                    {!! Form::hidden('id',null) !!}
                                    {!! Form::hidden('id_solicitud_rrhh',null) !!}
                                    <div class="form-group">
                                        <label for="">Lineas</label>
                                        <select class="custom-select" name="lineaMetro" id="lineaMetro" disabled>
                                            <option value="" selected hidden>{{$precandidatos->linea}} </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Estaciones</label>
                                        <select class="custom-select" name="metroCercano" id="metroCercano"
                                                disabled>
                                            <option value="{{$precandidatos->id_estacion}}" selected
                                                    hidden>{{$precandidatos->estacion}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                {!!Form::submit('Actualizar',array('class'=>'btn btn-success'))  !!}
                                {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--{!!Form::select('estadoCivil',$estadoCivil, $precandidatos->id_estado_civil, array('class'=>'custom-select'))!!}--}}
    <div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content text-center">
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Foto</p>
                </div>
                <div class="modal-body">
                    <video id="video" style="width: 100%;"></video>
                    <br>
                    <button type="button" class="btn btn-success" id="boton">Tomar Foto</button>
                    <p id="estado"></p>
                    <canvas id="canvas" style="display: none;"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>--}}
    {{--<script--}}
    {{--src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>--}}
    {{--<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>--}}
    <script src="{{asset('js/bootstrap-combobox.js')}}"></script>
    <script>
        function tieneSoporteUserMedia() {
            return !!(navigator.getUserMedia || (navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia)
        }

        function _getUserMedia() {
            return (navigator.getUserMedia || (navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia).apply(navigator, arguments);
        }

        // Declaramos elementos del DOM
        var $video = document.getElementById("video"),
            $canvas = document.getElementById("canvas"),
            $boton = document.getElementById("boton"),
            $estado = document.getElementById("estado");
        if (tieneSoporteUserMedia()) {
            _getUserMedia(
                {video: true},
                function (stream) {
                    console.log("Permiso concedido");
                    $video.src = window.URL.createObjectURL(stream);
                    $video.play();

                    //Escuchar el click
                    $boton.addEventListener("click", function () {

                        //Pausar reproducción
                        $video.pause();

                        //Obtener contexto del canvas y dibujar sobre él
                        var contexto = $canvas.getContext("2d");
                        $canvas.width = $video.videoWidth;
                        $canvas.height = $video.videoHeight;
                        contexto.drawImage($video, 0, 0, $canvas.width, $canvas.height);

                        var foto = $canvas.toDataURL(); //Esta es la foto, en base 64
                        $estado.innerHTML = "Enviando foto. Por favor, espera...";
                        var xhr = new XMLHttpRequest();
                        token = document.querySelector('meta[name="csrf-token"]').content;
                        xhr.open("POST", "{{route('preCandidatos.foto')}}", true);
                        xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhr.send(encodeURIComponent(foto)); //Codificar y enviar

                        xhr.onreadystatechange = function () {
                            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                                console.log("La foto fue enviada correctamente");
                                console.log(xhr);
                                $estado.innerHTML = "Foto guardada con éxito";
                                document.getElementById("imagen").src = "../../" + xhr.responseText;
                                document.getElementById("imagenText").value = xhr.responseText;
                            }
                        }


                        //Reanudar reproducción
                        $video.play();
                    });
                }, function (error) {
                    console.log("Permiso denegado o error: ", error);
                    $estado.innerHTML = "No se puede acceder a la cámara, o no diste permiso.";
                });
        } else {
            alert("Lo siento. Tu navegador no soporta esta característica");
            $estado.innerHTML = "Parece que tu navegador no soporta esta característica. Intenta actualizarlo.";
        }
    </script>

    <script type="text/javascript">
        $('#medioTraslado').on('change', function (e) {

            console.log(e);
            var medioId = e.target.value;
            $.get('/ajax-linea?medioId=' + medioId, function (data) {
                console.log(data);
                $('#lineaMetro').prop("disabled", false);
                $('#lineaMetro').empty();
                $('#lineaMetro').append('<option value="0" disable="true" selected="true">Seleccione una Linea</option>');


                $('#metroCercano').prop("disabled", true);
                $('#metroCercano').empty();
                $('#metroCercano').append('<option value="0" disable="true" selected="true">Seleccione una Estación</option>');


                $.each(data, function (index, lineasObj) {
                    $('#lineaMetro').append('<option value="' + lineasObj.lineas + '">' + lineasObj.lineas + '</option>');
                })
            });
        });

        $('#lineaMetro').on('change', function (e) {
            console.log(e);
            var $lineaId = e.target.value;
            var medioId = $('#medioTraslado').val();
            $.get('/ajax-metroCercano?lineaId=' + $lineaId + '&medioId=' + medioId, function (data) {
                console.log(data);
                $('#metroCercano').prop("disabled", false);
                $('#metroCercano').empty();
                $('#metroCercano').append('<option value="0" disable="true" selected="true">Seleccione una Estación</option>');

                $.each(data, function (index, estacionesObj) {
                    $('#metroCercano').append('<option value="' + estacionesObj.id + '">' + estacionesObj.estacion + '</option>');
                })
            });
        });

    </script>

    <script>
        $('#cp').on('focusout', function () {
            var $cp = $('#cp').val();
            $.get('/ajax-buscarCp?cp=' + $cp, function (data) {
                console.log(data);
                $('#colonia').prop("disabled", false);
                $('#colonia').empty();
                $('#colonia').append('<option value="0" disable="true" selected="true">Seleccione una Colonia</option>');
                $.each(data, function (index, coloniasObj) {
                    $('#colonia').append('<option value="' + coloniasObj.asenta + '">' + coloniasObj.asenta + '</option>');
                    $('#estado').val(coloniasObj.estado);
                    $('#ciudad').val(coloniasObj.ciudad);
                    $('#del_mun').val(coloniasObj.del_mun);

                })
            });
        });

    </script>
@endsection
