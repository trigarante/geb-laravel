@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Pre Candidatos</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="precandidato_table">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Celular</th>
                                        <th>Correo</th>
                                        <th>Vacante</th>
                                        <th>Tipo de Puesto</th>
                                        <th>Estado</th>
                                        <th>Candidato</th>
                                        @if(auth()->user()->grupo_usuarios->precandidatos == 1)
                                            <th>Accion</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($precandidatos as $precandidato)
                                        <tr>
                                            <td>
                                                <a href="{{route('preCandidatos.view',$precandidato->id)}}">{{$precandidato->nombre." ".$precandidato->apellido_paterno." ".$precandidato->apellido_materno}}</a>
                                            </td>
                                            <td>{{$precandidato->telefono_movil}}</td>
                                            <td>{{$precandidato->email}}</td>
                                            <td>{{$precandidato->vacante}}</td>
                                            <td>{{$precandidato->tipo}}</td>
                                            <td>{{$precandidato->motivo}}</td>
                                            <td>

                                                @if($precandidato->siguiente == 0)
                                                    @if(auth()->user()->grupo_usuarios->precandidatos == 1)
                                                        <button class="btn btn-success"
                                                                style="background-color: #1e8435; border-color: #1e8435;"
                                                                id="crearCandidato"
                                                                data-id='{{ $precandidato->id }}'
                                                                data-toggle="modal" data-target="#crearCandidatoModal">
                                                            Crear
                                                            Candidato
                                                        </button>
                                                    @endif
                                                @else
                                                    <a href="{{route('candidato.show',$precandidato->id)}}"
                                                       class="btn btn-success"
                                                       style="background-color: #216d32; border-color: #216d32;">Ver
                                                        Candidato</a>
                                                @endif
                                            </td>
                                            @if(auth()->user()->grupo_usuarios->precandidatos == 1)
                                                <td>
                                                    <a class="btn btn-warning"
                                                       style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff"
                                                       href="{{route('preCandidatos.edit',$precandidato->id)}}">Modificar</a>
                                                    <button class="btn btn-info"
                                                            style="background-color: #2b5182; border-color: #2b5182; "
                                                            data-toggle="modal"
                                                            data-target="#deleteModal"
                                                            data-id='{{ $precandidato->id }}'>Cambiar Estatus
                                                    </button>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <div class="modal fade" id="crearCandidatoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Candidato</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="POST" action="{{route('candidato.store')}}" id="form">
                        @csrf
                        <input type="hidden" class="form-control" name="id" id="id">
                        <div class="form-group">
                            <label for="">Calificacion de Competencias</label>
                            <select name="competencia" id="competencia" class="custom-select" style="text-transform:uppercase">
                                <option value="" selected>Calificacion Competencias</option>
                                @foreach($competencias as $competencia)
                                    <option value="{{$competencia->id}}">{{$competencia->estadio}}</option>
                                @endforeach
                            </select>
                            <div id="errorCompetencia"></div>
                        </div>

                        <div class="form-group">
                            <label for="calificacion_examen">Calificacion de Examen</label>
                            <input type="text" id="calificacionExamen" class="form-control" maxlength="2" name="calificacion_examen">
                            <div id="errorCalificacion"></div>
                        </div>

                        <div class="form-group">
                            <label for="form7">Comentarios</label>
                            <textarea type="text" id="comentarios" class="md-textarea form-control" rows="3" style="text-transform:uppercase"
                                      name="comentarios"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success" id="guardar">Crear Candidato</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Cambiar Estatus</p>
                </div>
                <div class="modal-body">
                    <form action="{{route('preCandidatos.destroy')}}" method="POST">
                        @csrf
                        <label for="">Seleccione un Estatus</label>
                        <select name="motivo_baja" id="" class="custom-select">
                            @foreach($motivos as $motivo)
                                <option value="{{$motivo->id}}">{{$motivo->estado}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="id" name="id">

                        <button class="btn  btn-success mt-3" type="submit">Guardar</button>
                        <button type="button" class="btn  btn-warning mt-3" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--Modal: modalConfirmDelete-->
@endsection
@section('scripts')
    <script src="{{asset('DataTables/datatables.min.js')}}"></script>
    <script type="text/javascript">
        $('#crearCandidatoModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
        })
    </script>

    <script type="text/javascript">
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
        })
    </script>
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#precandidato_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            jQuery('#guardar').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ route('candidato.store')}}",
                    method: 'post',
                    data: jQuery('#form').serialize(),
                    beforeSend: function () {
                        $('#guardar').prop("disabled", true);
                    },
                    success: function (result) {
                        if (result.errors) {
                            $('#guardar').prop("disabled", false);
                            jQuery('.alert-danger').html('');
                            jQuery.each(result.errors, function (key, value) {
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>' + value + '</li>');
                            });
                        }
                        else {
                            jQuery('.alert-danger').hide();
                            $('#crearCandidatoModal').modal('hide');
                            document.location.href="{!! route('candidato.index'); !!}";
                        }
                    }
                });
            });
        });
    </script>
@endsection
