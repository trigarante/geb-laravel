@extends('layouts.appLayout')
@section('styles')
    <link rel="stylesheet" href="{{asset('/css/bootstrap-combobox.css')}}">
@endsection
@section('content')

    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('preCandidatos.index')}}">Pre Candidatos</a></li>
                <li class="breadcrumb-item active">Registrar Pre Candidato</li>
            </ul>
        </div>
    </div>
    {{--errores--}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <section class="forms">
        <div class="container-fluid">
            <div class="row justify-content-center mt-3">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h4>Crear Candidato</h4>
                        </div>
                        <div class="card-body">
                            <p>Todos los campos son obligatorios.</p>
                            <form action="{{route('preCandidatos.store')}}" method="post">
                                <input type="hidden" value="{{$id}}" name="idSolicitud">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="nombre">Nombre</label>
                                            <input type="text" class="form-control" name="nombre" id="nombre" style="text-transform:uppercase"  value="{{$solicitudDatos->nombre}}" >
                                        </div>
                                        <div class="form-group">
                                            <label for="apellido_paterno">Apellido Paterno</label>
                                            <input type="text" class="form-control" name="apellido_paterno"
                                                   value="{{$solicitudDatos->apellido_paterno}}" id="apellido_paterno" style="text-transform:uppercase" >
                                        </div>

                                        <div class="form-group">
                                            <label for="apellido_materno">Apellido Materno</label>
                                            <input type="text" class="form-control" name="apellido_materno"
                                                   value="{{$solicitudDatos->apellido_materno}}" id="apellido_materno" style="text-transform:uppercase" >
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email"
                                                   value="{{$solicitudDatos->correo}}"
                                                   id="email" >
                                        </div>

                                        <div class=form-group">
                                            <label for="fechaNacimiento">Fecha de Nacimiento</label>
                                            <input type="date" name="fechaNacimiento" id="fechaNacimiento"
                                                   value="{{ old('fechaNacimiento') }}"
                                                   @if($errors->has('fechaNacimiento')) class="form-control is-invalid"
                                                   @else class="form-control" @endif>
                                        </div>

                                        <div class="form-group">
                                            <label for="fechaNacimiento" class="pt-2">Genero</label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" name="genero" value="M"
                                                       @if (Request::old('genero') == 'M') checked
                                                       @endif id="defaultGroupExample1">
                                                <label class="custom-control-label"
                                                       for="defaultGroupExample1">Masculino</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" name="genero" value="F"
                                                       @if (Request::old('genero') == 'F') checked
                                                       @endif id="defaultGroupExample2">
                                                <label class="custom-control-label"
                                                       for="defaultGroupExample2">Femenino</label>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="estadoCivil">Estado Civil</label>
                                            <select name="estadoCivil" id=""
                                                    @if($errors->has('estadoCivil')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif>
                                                <option value="" selected hidden disabled>Estado Civil</option>
                                                @foreach($estadoCivil as $etoCivil)
                                                    @if (Request::old('estadoCivil') == $etoCivil->id)
                                                        <option value="{{$etoCivil->id}}"
                                                                selected>{{$etoCivil->descripcion}}</option>
                                                    @else
                                                        <option
                                                            value="{{$etoCivil->id}}">{{$etoCivil->descripcion}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="escolaridad">Escolaridad</label>
                                            <select name="escolaridad" id=""
                                                    @if($errors->has('escolaridad')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif>
                                                <option value="" selected hidden disabled>Escolaridad</option>
                                                @foreach($escolaridad as $escolar)
                                                    @if (Request::old('escolaridad') == $escolar->id)
                                                        <option value="{{$escolar->id}}"
                                                                selected>{{$escolar->nivel}}</option>
                                                    @else
                                                        <option value="{{$escolar->id}}">{{$escolar->nivel}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="estadoEscolar">Estado de Escolaridad</label>
                                            <select name="estadoEscolar" id=""
                                                    @if($errors->has('estadoEscolar')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif>
                                                <option value="" selected hidden disabled>Estado de Escolaridad</option>
                                                @foreach($estadoEscolar as $etdoEscolar)
                                                    @if (Request::old('estadoEscolar') == $etdoEscolar->id)
                                                        <option value="{{$etdoEscolar->id}}"
                                                                selected>{{$etdoEscolar->estado}}</option>
                                                    @else
                                                        <option
                                                            value="{{$etdoEscolar->id}}">{{$etdoEscolar->estado}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="pais">Pais de Origen</label>
                                            <select name="pais" id=""
                                                    @if($errors->has('pais')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif>
                                                <option value="" selected hidden disabled>Pais de Origen</option>
                                                @foreach($paises as $pais)
                                                    @if (Request::old('pais') == $pais->id)
                                                        <option value="{{$pais->id}}"
                                                                selected>{{$pais->nombre}}</option>
                                                    @else
                                                        <option value="{{$pais->id}}">{{$pais->nombre}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <img src="" alt="" id="imagen" class="img-fluid mb-3" style="width: 150px;">
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cp">Codigo Postal</label>
                                            <input type="text" @if($errors->has('cp')) class="form-control is-invalid"
                                                   @else class="form-control" @endif name="cp" id="cp"
                                                   value="{{ old('cp') }}"  maxlength="5"> 
                                        </div>

                                        <div class="form-group">
                                            <label for="colonia">Colonia</label>
                                            <select class="custom-select" name="colonia" id="colonia" disabled>
                                                <option value="0" selected hidden> Seleccione una colonia</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="calle">Calle</label>
                                            <input type="text" name="calle" id="calle" style="text-transform:uppercase"  value="{{ old('calle') }}"
                                                   @if($errors->has('calle')) class="form-control is-invalid"
                                                   @else class="form-control" @endif>
                                        </div>

                                        <div class="form-group">
                                            <label for="numeroInt">Numero Interior</label>
                                            <input type="text" name="numeroInt" id="numeroInt"
                                                   value="{{ old('numeroInt') }}"
                                                   @if($errors->has('numeroInt')) class="form-control is-invalid"
                                                   @else class="form-control" @endif>
                                        </div>

                                        <div class="form-group">
                                            <label for="numeroExt">Numero Exterior</label>
                                            <input type="text" name="numeroExt" id="numeroExt"
                                                   value="{{ old('numeroExt') }}"
                                                   @if($errors->has('numeroExt')) class="form-control is-invalid"
                                                   @else class="form-control" @endif>
                                        </div>

                                        <div class="form-group">
                                            <label for="telFijo">Telefono Fijo</label>
                                            <input type="text" name="telFijo" id="telFijo" value="{{ old('telFijo') }}"
                                                   @if($errors->has('telFijo')) class="form-control is-invalid"
                                                   @else class="form-control" @endif maxlength="8">
                                        </div>

                                        <div class="form-group">
                                            <label for="telMovil">Telefono Movil</label>
                                            <input type="text" name="telMovil" id="telMovil"
                                                   value="{{ old('telMovil') }}"
                                                   @if($errors->has('telMovil')) class="form-control is-invalid"
                                                   @else class="form-control" @endif maxlength="10">
                                        </div>

                                        <div class="form-group">
                                            <label for="tiempoTraslado">Tiempo de Traslado en Minutos</label>
                                            <input type="text" name="tiempoTraslado" id="tiempoTraslado"
                                                   value="{{ old('tiempoTraslado') }}"
                                                   @if($errors->has('tiempoTraslado')) class="form-control is-invalid"
                                                   @else class="form-control" @endif maxlength="3">
                                        </div>

                                        <div class="form-group">
                                            <label for="medioTraslado">Medio de Traslado</label>
                                            <select name="medioTraslado" id="medioTraslado"
                                                    @if($errors->has('medioTraslado')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif >
                                                <option value="" disabled selected hidden>Seleccione un medio</option>
                                                @foreach($medios as $medio)
                                                    <option value="{{$medio->id}}">{{$medio->medio}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input type="hidden" id="estado" name="estado">
                                        <input type="hidden" id="ciudad" name="ciudad">
                                        <input type="hidden" id="del_mun" name="del_mun">
                                        <div class="form-group">
                                            <label for="">Lineas</label>
                                            <select class="custom-select" name="lineaMetro" id="lineaMetro" disabled>
                                                <option value="0" selected hidden> Seleccione una Linea</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Estaciones</label>
                                            <select class="custom-select" name="metroCercano" id="metroCercano"
                                                    disabled>
                                                <option value="0" selected hidden> Seleccione una estacion</option>
                                            </select>
                                        </div>


                                        <input type="hidden" id="imagenText" name="imagen">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Registrar" class="btn btn-primary btn-md">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#fotoModal"
                                            type="button">Tomar Foto
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content text-center">
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Foto</p>
                </div>
                <div class="modal-body">
                    <video id="video" style="width: 100%;"></video>
                    <br>
                    <button type="button" class="btn btn-success" id="boton">Tomar Foto</button>
                    <p id="estado"></p>
                    <canvas id="canvas" style="display: none;"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>--}}
    {{--<script--}}
    {{--src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>--}}
    {{--<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>--}}
    <script src="{{asset('js/bootstrap-combobox.js')}}"></script>
    <script>
        function tieneSoporteUserMedia() {
            return !!(navigator.getUserMedia || (navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia)
        }

        function _getUserMedia() {
            return (navigator.getUserMedia || (navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia).apply(navigator, arguments);
        }

        // Declaramos elementos del DOM
        var $video = document.getElementById("video"),
            $canvas = document.getElementById("canvas"),
            $boton = document.getElementById("boton"),
            $estado = document.getElementById("estado");
        if (tieneSoporteUserMedia()) {
            _getUserMedia(
                {video: true},
                function (stream) {
                    console.log("Permiso concedido");
                    $video.src = window.URL.createObjectURL(stream);
                    $video.play();

                    //Escuchar el click
                    $boton.addEventListener("click", function () {

                        //Pausar reproducción
                        $video.pause();

                        //Obtener contexto del canvas y dibujar sobre él
                        var contexto = $canvas.getContext("2d");
                        $canvas.width = $video.videoWidth;
                        $canvas.height = $video.videoHeight;
                        contexto.drawImage($video, 0, 0, $canvas.width, $canvas.height);

                        var foto = $canvas.toDataURL(); //Esta es la foto, en base 64
                        $estado.innerHTML = "Enviando foto. Por favor, espera...";
                        var xhr = new XMLHttpRequest();
                        token = document.querySelector('meta[name="csrf-token"]').content;
                        xhr.open("POST", "{{route('preCandidatos.foto')}}", true);
                        xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhr.send(encodeURIComponent(foto)); //Codificar y enviar

                        xhr.onreadystatechange = function () {
                            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                                console.log("La foto fue enviada correctamente");
                                console.log(xhr);
                                $estado.innerHTML = "Foto guardada con éxito";
                                document.getElementById("imagen").src = "../../" + xhr.responseText;
                                document.getElementById("imagenText").value = xhr.responseText;
                            }
                        }


                        //Reanudar reproducción
                        $video.play();
                    });
                }, function (error) {
                    console.log("Permiso denegado o error: ", error);
                    $estado.innerHTML = "No se puede acceder a la cámara, o no diste permiso.";
                });
        } else {
            alert("Lo siento. Tu navegador no soporta esta característica");
            $estado.innerHTML = "Parece que tu navegador no soporta esta característica. Intenta actualizarlo.";
        }
    </script>

    <script type="text/javascript">
        $('#medioTraslado').on('change', function (e) {

            console.log(e);
            var medioId = e.target.value;
            $.get('/ajax-linea?medioId=' + medioId, function (data) {
                console.log(data);
                $('#lineaMetro').prop("disabled", false);
                $('#lineaMetro').empty();
                $('#lineaMetro').append('<option value="0" disable="true" selected="true">Seleccione una Linea</option>');


                $('#metroCercano').prop("disabled", true);
                $('#metroCercano').empty();
                $('#metroCercano').append('<option value="0" disable="true" selected="true">Seleccione una Estación</option>');


                $.each(data, function (index, lineasObj) {
                    $('#lineaMetro').append('<option value="' + lineasObj.lineas + '">' + lineasObj.lineas + '</option>');
                })
            });
        });

        $('#lineaMetro').on('change', function (e) {
            console.log(e);
            var $lineaId = e.target.value;
            var medioId = $('#medioTraslado').val();
            $.get('/ajax-metroCercano?lineaId=' + $lineaId + '&medioId=' + medioId, function (data) {
                console.log(data);
                $('#metroCercano').prop("disabled", false);
                $('#metroCercano').empty();
                $('#metroCercano').append('<option value="0" disable="true" selected="true">Seleccione una Estación</option>');

                $.each(data, function (index, estacionesObj) {
                    $('#metroCercano').append('<option value="' + estacionesObj.id + '">' + estacionesObj.estacion + '</option>');
                })
            });
        });

    </script>

    <script>
        $('#cp').on('focusout', function () {
            var $cp = $('#cp').val();
            $.get('/ajax-buscarCp?cp=' + $cp, function (data) {
                console.log(data);
                $('#colonia').prop("disabled", false);
                $('#colonia').empty();
                $('#colonia').append('<option value="0" disable="true" selected="true">Seleccione una Colonia</option>');
                $.each(data, function (index, coloniasObj) {
                    $('#colonia').append('<option value="' + coloniasObj.asenta + '">' + coloniasObj.asenta + '</option>');
                    $('#estado').val(coloniasObj.estado);
                    $('#ciudad').val(coloniasObj.ciudad);
                    $('#del_mun').val(coloniasObj.del_mun);

                })
            });
        });

    </script>
@endsection
