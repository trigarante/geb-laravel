@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Capacitacion</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="capacitacion_table">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Vacante</th>
                                        <th>Calificaion</th>
                                        <th>Estado</th>
                                        <th>Seguimiento</th>
                                        <th>Accion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($capacitaciones as $capacitacion)
                                        <tr>
                                            <td>{{$capacitacion->nombre.' '.$capacitacion->apellido_paterno.' '.$capacitacion->apellido_materno}}</td>
                                            <td>{{$capacitacion->telefono_movil}}</td>
                                            <td>{{$capacitacion->email}}</td>
                                            <td>{{$capacitacion->vacante}}</td>
                                            <td>{{$capacitacion->calificacion}}</td>
                                            <td>{{$capacitacion->estado_rh}}</td>
                                            <td>
                                                @if($capacitacion->siguiente == 0)
                                                    <button class="btn btn-success"
                                                            style="background-color: #1e8435; border-color: #1e8435;"
                                                            id="crearSeguimiento"
                                                            data-id='{{ $capacitacion->id }}'
                                                            data-toggle="modal"
                                                            data-target="#crearSeguimientoModal">
                                                        Crear Seguimiento
                                                    </button>
                                                @endif
                                            </td>
                                            <td>

                                                <button class="btn btn-warning"
                                                        style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff"
                                                        data-id='{{ $capacitacion->id }}'
                                                        data-calificacion
                                                        data-comentarios='{{$capacitacion->comentarios}}'
                                                        data-calificacion_examen='{{$capacitacion->calificacion_examen}}'
                                                        data-competencia='{{$capacitacion->competencia}}'
                                                        data-id_calificacion_competencias='{{$capacitacion->id_calificacion_competencias}}'
                                                        data-toggle="modal" data-target="#updateCandidatoModal">
                                                    Modificar
                                                </button>
                                                <button class="btn btn-info"
                                                        style="background-color: #2b5182; border-color: #2b5182; "
                                                        data-toggle="modal"
                                                        data-target="#deleteModal"
                                                        data-id="{{$capacitacion->id}}">Cambiar estatus
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="crearSeguimientoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Seguimiento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="POST" id="formSeguimiento">
                        @csrf
                        <input type="hidden" class="form-control" name="id" id="id">

                        <div class="form-group">
                            <label for="calificacion">Calificacion Rollplay</label>
                            <input type="text" id="calificacion_rollplay" class="form-control"
                                   name="calificacion_rollplay" maxlength="2">
                        </div>

                        <div class="form-group">
                            <label for="polizas_vendidas">Polizas Vendidas</label>
                            <input type="text" id="polizas_vendidas" class="form-control"
                                   name="polizas_vendidas" maxlength="2">
                        </div>

                        <div class="form-group">
                            <label for="prima_neta">Prima Neta</label>
                            <input type="text" id="prima_neta" class="form-control"
                                   name="prima_neta" maxlength="6">
                        </div>

                        <div class="form-group">
                            <label for="campana">Campaña</label>
                            <select name="id_campana" id="id_campana" style="text-transform:uppercase"  class="custom-select">
                                @foreach($campanas as $campana)
                                <option value="{{$campana->id}}">{{$campana->nombre}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="comentarios">Comentarios</label>
                            <textarea type="text" id="comentarios" style="text-transform:uppercase"  class="md-textarea form-control" rows="3"
                                      name="comentarios"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success" id="save">Crear Seguimiento</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

       <div class="modal fade" id="updateCandidatoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modifica Candidato</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" action="{{route('capacitacion.update')}}" id="form">
                        @csrf
                        <input type="hidden" class="form-control" name="id" id="id">
                        <div class="form-group">
                            <label for="competencias">Calificacion de Competencias</label>
                            <select name="competencias" id="competencias" class="custom-select" style="text-transform:uppercase">
                                <option value="" selected>Calificacion Competencias</option>
                                @foreach($competencias as $competencia)
                                    <option value="{{$competencia->id}}">{{$competencia->estadio}}</option>
                                @endforeach
                            </select>
                            <div id="errorCompetencia"></div>
                        </div>

                        <div class="form-group">
                            <label for="calificacion_examen">Calificacion de Examen</label>
                            <input type="text" id="calificacion_examen" class="form-control" maxlength="2" name="calificacion_examen">
                            <div id="errorCalificacion"></div>
                        </div>

                        <div class="form-group">
                            <label for="comentarios">Comentarios</label>
                            <textarea type="text" id="comentarios" class="md-textarea form-control" rows="3" style="text-transform:uppercase"
                                      name="comentarios"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success" id="guardar">ACTUALIZAR CANDIDATO</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

@endsection 

@section('scripts')
    <script src="{{asset('DataTables/datatables.min.js')}}"></script>
    <script type="text/javascript">
        $('#crearSeguimientoModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
        })
    </script>

    <script type="text/javascript">
        $('#updateCandidatoModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var calificacion_examen = button.data('calificacion_examen')
            var competencia = button.data('competencia')
            var comentarios = button.data('comentarios')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #calificacion_examen').val(calificacion_examen);
            modal.find('.modal-body #competencia').val(competencia);
            modal.find('.modal-body #comentarios').val(comentarios);
        })
    </script>
  
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#capacitacion_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            jQuery('#save').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ route('seguimiento.store')}}",
                    method: 'post',
                    data: jQuery('#formSeguimiento').serialize(),
                    beforeSend: function () {
                        $('#save').prop("disabled", true);
                    },
                    success: function (result) {
                        if (result.errors) {
                            $('#save').prop("disabled", false);
                            jQuery('.alert-danger').html('');
                            jQuery.each(result.errors, function (key, value) {
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>' + value + '</li>');
                            });
                        }
                        else {
                            jQuery('.alert-danger').hide();
                            $('#crearSeguimientoModal').modal('hide');
                            document.location.href = "{!! route('seguimiento.index'); !!}";
                        }
                    }
                });
            });
        });
    </script>

@endsection
