@extends('layouts.appLayout')
@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Importar Excel</li>
        </ol>
        <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" class="form-horizontal" method="post"
              enctype="multipart/form-data" id="formuploadajax">
            {{ csrf_field() }}
            <input type="file" name="import_file" id="import_file"/>
            <button class="btn btn-primary" type="submit" id="submit">Subir archivo</button>
        </form>

        <div id="mostrar_loading" style="display: none;" class="pt-5 pl-5">

        </div>

        <div id="mensaje">

        </div>


    </div>

@endsection

@section('scripts')

    <script>
        $(function () {
            $("#formuploadajax").on("submit", function (e) {
                e.preventDefault();
                var f = $(this);
                var formData = new FormData(document.getElementById("formuploadajax"));
                formData.append("dato", "valor");
                //formData.append(f.attr("name"), $(this)[0].files[0]);
                $.ajax({
                    url: "{{ route('import.upload') }}",
                    type: "post",
                    dataType: "html",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        document.getElementById("mensaje").style.display = "none";
                        document.getElementById("mostrar_loading").style.display = "block";
                        document.getElementById("mostrar_loading").innerHTML = "<img src ='{{asset('images/loading.gif')}}' width = '150' heigth = '150'>";
                        $('#submit').attr('disabled', true);
                    },
                    success: function () {
                        document.getElementById("mensaje").innerHTML = 'Completado';
                        document.getElementById("mostrar_loading").style.display = "none";
                        $('#submit').attr('disabled', false);
                    }
                })
            });
        });
    </script>

@endsection
