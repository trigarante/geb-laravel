@extends('layouts.appLayout')
@section('content')
<div class="container pl-5">
	<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('ejecutivo.index')}}">Asignaciones</a></li>
    <li class="breadcrumb-item active">Correo y extensión</li>
</ol>
	<div class="row">
			@if (count($errors) > 0)
    <div class="alert alert-danger">
    	<p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif
				<div class="col-md-6">
					<table id="dtBasicExample" class="table table-condensed" cellspacing="0" width="100%">
						<tr>
							<th>Nombre: </th>
							<th>{{$empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno}} </th>
						</tr>

						<tr>
							<th>Campaña: </th>
							<th>{{$empleado->campana}}</th>
						</tr>
					</table>
				</div>

			
			<form action="{{route('ejecutivo.store')}}" method="POST">
			@csrf
				<div class="row">
					<div class="col-md-6">
						<input type="hidden" value="{{$id}}" name="numEmpleado">

						<div class="form-group">
							<label for="extension">Extensión</label>
							<select name="extension" id="" @if($errors->has('extension')) class="custom-select is-invalid" @else class="custom-select" @endif>
								<option value="" selected>Extensiones disponibles</option>
									@foreach($extension as $ext)
										@if (Request::old('extension') == $ext->id)
							      			<option value="{{$ext->id}}" selected>{{$ext->id}}</option>
										@else
							      			<option value="{{$ext->id}}">{{$ext->id}}</option>
										@endif
									@endforeach
							</select>
						</div>							
					</div>

                                <div class="form-group">
                                    <label for="grupo">Grupo</label>
                                    <select name="correo" id="" @if($errors->has('grupo')) class="custom-select is-invalid" @else class="custom-select" @endif>
										<option value="" selected>Grupos</option>
											@foreach($grupos as $grupo)
												@if (Request::old('grupo') == $grupo->id)
							      					<option value="{{$grupo->id}}" selected>{{$grupo->grupo}}</option>
												@else
							      					<option value="{{$grupo->id}}">{{$grupo->grupo}}</option>
												@endif
											@endforeach
									</select>
                                </div>

                                <div class="form-group">
                                	@if(!$extension->isEmpty())
	                                    <label for="extension">Extensión</label>
	                                    <select name="extension" id="" @if($errors->has('extension')) class="custom-select is-invalid" @else class="custom-select" @endif>
											<option value="" selected>Extensiones disponibles</option>
												@foreach($extension as $ext)
													@if (Request::old('extension') == $ext->id)
											      		<option value="{{$ext->id}}" selected>{{$ext->id}}</option>
													@else
											      		<option value="{{$ext->id}}">{{$ext->id}}</option>
													@endif
												@endforeach
											</select>
									@else
										<label for="extension">No hay extensiones disponibles</label>
										<br>
	                                    <button class="btn btn-info"
                                                data-id=' '
                                                data-toggle="modal" data-target="#nuevaExtension"
                                                type="button"
                                                style="background-color: #2b5182; border-color: #2b5182; ">
                                                    Nueva extensión 
                                        </button>
									@endif

                                </div>

                                <div class="form-group">
                                    <label for="correo">Correo </label>
                                    <select name="correo" id="" @if($errors->has('correo')) class="custom-select is-invalid" @else class="custom-select" @endif>
										<option value="" selected>Correos disponibles</option>
											@foreach($correo as $correo)
												@if (Request::old('correo') == $correo->id)
									      			<option value="{{$correo->id}}" selected>{{$correo->correo}}</option>
												@else
									      			<option value="{{$correo->id}}">{{$correo->correo}}</option>
												@endif
											@endforeach
									</select>
                                </div>
                                
                                <div class="form-group">
                                    <input type="submit" value="Crear usuario" class="btn btn-primary btn-md">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>	

{{--modal nueva extension--}}
    <div class="modal fade" id="nuevaExtension" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nueva extensión</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="agendarCitaForm" method="POST" action="{{route('ejecutivo.nuevaExtension')}}">
                        @csrf
                     
                       <input type="hidden" value="{{$idCampana}}" name="idCampana">
                       <label for="extNC">Extensiones no creadas </label>
                       <select name="extNC" id="" @if($errors->has('extNC')) class="custom-select is-invalid" @else class="custom-select" @endif>
							<option value="" selected>Extensiones</option>
								@foreach($extensionNoCreada as $extNC)
									@if (Request::old('extNC') == $extNC->id)
									    <option value="{{$extNC->id}}" selected>{{$extNC->id}}</option>
									@else
									    <option value="{{$extNC->id}}">{{$extNC->id}}</option>
									@endif
								@endforeach
							</select>							
                        <button type="submit" class="btn btn-success" id="agendar">Crear</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>		
@endsection

