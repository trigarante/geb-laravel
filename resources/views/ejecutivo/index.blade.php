@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Asignaciones</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="precandidato_table">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Área</th>
                                    <th>Campaña</th> 
                                    <th> </th>     
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($empleados as $empleado)
                                    <tr>
                                        <td>{{$empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno}}</td>
                                        <td>{{$empleado->area}}</td>
                                        <td>{{$empleado->campana}}</td>
                                        <td>
                                            <a class="btn btn-success"
                                               style="background-color: #216d32; border-color: #216d32;"
                                               href="{{route('ejecutivo.create',[$empleado->id,$empleado->id_campana])}}">Crear usuario</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection 