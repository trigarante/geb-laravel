@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
     @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Empleado</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="empleado_table" class="table table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Puesto</th>
                                        <th>Tipo de Empleado</th>
                                        <th>Area</th>
                                        <th>Estado</th>
                                        @if(auth()->user()->grupo_usuarios->empleados == 1)
                                            <th>Accion</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($empleados as $empleado)
                                        <tr>
                                            <td>
                                                <a href="{{route('empleado.view',$empleado->id)}}">{{$empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno}}</a>
                                            </td>
                                            <td>{{$empleado->telefono_movil}}</td>
                                            <td>{{$empleado->email}}</td>
                                            <td>{{$empleado->puesto}}</td>
                                            <td>{{$empleado->tipo_usuario}}</td>
                                            <td>{{$empleado->area}}</td>
                                            <td>{{$empleado->estado_rh}}</td>
                                            @if(auth()->user()->grupo_usuarios->empleados == 1)
                                                <td><a href="{{route('empleado.edit',$empleado->id)}}"
                                                       class="btn btn-warning"
                                                       style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff">Modificar</a>
                                                    <button class="btn btn-info"
                                                            style="background-color: #2b5182; border-color: #2b5182; "
                                                            data-toggle="modal" data-target="#deleteModal"
                                                            data-id="{{$empleado->id}}">
                                                        Modificar Estatus
                                                    </button>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Cambiar Estatus</p>
                </div>
                <div class="modal-body">
                    <form action="{{route('empleado.destroy')}}" method="POST">
                        @csrf
                        <label for="">Seleccione un Estatus</label>
                        <select name="motivo_baja" id="" class="custom-select">
                            @foreach($motivos as $motivo)
                                <option value="{{$motivo->id}}">{{$motivo->estado}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="id" name="id">
                        <button class="btn  btn-success mt-3" type="submit">Guardar</button>
                        <button type="button" class="btn  btn-warning mt-3" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('DataTables/datatables.min.js')}}"></script>
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#empleado_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>
    <script type="text/javascript">
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
        })
    </script>

    <script src="{{asset('DataTables/datatables.min.js')}}"></script>

@endsection
