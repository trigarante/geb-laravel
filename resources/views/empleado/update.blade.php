    @extends('layouts.appLayout')
@section('content')
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('empleado.index')}}">Empleados</a></li>
                <li class="breadcrumb-item active">Actualizar Empleado</li>
            </ul>
        </div>
    </div>
    {{--errores--}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="forms">
        <div class="container-fluid">
            <div class="row justify-content-center mt-3">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h4>Actualizar Empleado</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('empleado.update')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" value="{{$empleado->id}}" name="id">
                                        <input type="hidden" value="{{$document->id}}" name="id">
                                        

                                        <div class="form-group">
                                            <div class=form-control">
                                                <label for="fechaImss" >Fecha de Alta de Imss</label>
                                                <input type="date"  class="form-control" name="fechaImss"
                                                       value="{{$empleado->fecha_alta_imss}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="puesto">Puesto</label>
                                            <select name="puesto" id="" class="custom-select">
                                                <option value="{{$empleado->id_puesto}}" selected>{{$empleado->puesto}}</option>
                                                @foreach($puestos as $puesto)
                                                    <option value="{{$puesto->id}}">{{$puesto->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="tipoPuesto">Tipo de Puesto</label>
                                            <select name="tipoPuesto" id="" class="custom-select">
                                                <option value="{{$empleado->id_puesto_tipo}}" selected>{{$empleado->puesto_tipo}}</option>
                                                @foreach($tipoPuestos as $tipoPuesto)
                                                    <option value="{{$tipoPuesto->id}}">{{$tipoPuesto->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="detallePuesto" >Detalle de Puesto</label>
                                            <input type="text"  class="form-control" name="detallePuesto" value="{{$empleado->puesto_detalle}}">
                                        </div>

                                        <div class="form-group">
                                            <label for="area">Area</label>
                                            <select name="area" id="" class="custom-select">
                                                <option value="{{$empleado->id_area}}" selected>{{$empleado->area}}</option>
                                                @foreach($areas as $area)
                                                    <option value="{{$area->id}}">{{$area->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="sueldoDiario" >Sueldo Diario</label>
                                            <input type="text"  class="form-control" name="sueldoDiario" value="{{$empleado->sueldo_diario}}">
                                        </div>

                                          <div class="form-group">
                                            <label for="sueldoMensual" >Sueldo Mensual</label>
                                            <input type="text"  class="form-control" name="sueldoMensual" value="{{$empleado->sueldo_mensual}}">
                                        </div>

                                        <div class="form-group">
                                            <label for="cuentaClabe" >Cuenta Clabe</label>
                                            <input type="text"  class="form-control" name="cuentaClabe" value="{{$empleado->cta_clabe}}">
                                        </div>

                                    </div>



                                    <div class="col-md-6">
                                        

                                      

                                        <div class="form-group">
                                            <label for="banco">Banco</label>
                                            <select name="banco" id="" class="custom-select">
                                                <option value="{{$empleado->id_banco}}" selected>{{$empleado->banco}}</option>
                                                @foreach($bancos as $banco)
                                                    <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="empresa">Empresa</label>
                                            <select name="empresa" id="" class="custom-select">
                                                <option value="{{$empleado->id_empresa}}" selected>{{$empleado->empresa}}</option>
                                                @foreach($empresas as $empresa)
                                                    <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        

                                            <div class="form-group">
                                            <label for="cuentaClabe" >Cuenta Clabe</label>
                                            <input type="text"  class="form-control" name="cuentaClabe" value="{{$empleado->cta_clabe}}">
                                        </div>



                                       

                                        <div class="form-group">
                                            <div class=form-control">
                                                <label for="imss">IMSS</label>
                                                <input type="text" class="form-control" name="imss" maxlength="16" style="text-transform:uppercase" value="{{$imss->documento}}">
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <div class=form-control">
                                                <label for="curp">CURP</label>
                                                <input type="text" class="form-control" name="curp" maxlength="16" style="text-transform:uppercase" value="{{$curp->documento}}">
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <div class=form-control">
                                                <label for="rfc">RFC</label>
                                                <input type="text" class="form-control" name="rfc" maxlength="16" style="text-transform:uppercase" value="{{$rfc->documento}}">
                                            </div>
                                        </div>

                                        



                                        <div class="form-group">
                                            <label for="fechaCambioSueldo" >Fecha de Cambio de Sueldo</label>
                                            <input type="date"  class="form-control" name="fechaCambioSueldo" value="{{$empleado->fecha_cambio_sueldo}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mx-auto pt-2">
                                    <button type="sumbit"  class="btn btn-success">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
