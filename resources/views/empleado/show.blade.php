@extends('layouts.appLayout')
@section('content')
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            Informacion de {{$empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno}}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->nombre}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Apellido Paterno</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->apellido_paterno}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Apellido Materno</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->apellido_materno}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" disabled value="{{$empleado->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Telefono Movil</label>
                                        <input type="text" class="form-control" disabled value="{{$empleado->telefono_movil}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->estado}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Delegacion o Municipio</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->del_mun}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">


                                    <div class="form-group">
                                        <label>Ciudad</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->ciudad}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Colonia</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->colonia}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha de Nacimiento</label>
                                        <input type="text" class="form-control"  disabled value="{{$empleado->fecha_nacimiento}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha de Alta de IMSS</label>
                                        <input type="text" class="form-control" disabled value="{{$empleado->fecha_alta_imss}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha de Ingreso</label>
                                        <input type="text" class="form-control" disabled value="{{$empleado->fecha_ingreso}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Detalle de Puesto</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->puesto_detalle}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Puesto</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->puesto}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">




                                    <div class="form-group">
                                        <label>Tipo de Puesto</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->puesto_tipo}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Area</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->area}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Sueldo Diario</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->sueldo_diario}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Sueldo Mensual</label>
                                        <input type="text" class="form-control" disabled value="{{$empleado->sueldo_mensual}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Cuenta Clabe</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->cta_clabe}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Campaña</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->campana}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Empresa</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase" disabled value="{{$empleado->empresa}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
