@extends('layouts.appLayout')
@section('content')
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('empleado.index')}}">Empleados</a></li>
                <li class="breadcrumb-item active">Registrar Empleado</li>
            </ul>
        </div>
    </div>
    {{--errores--}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="forms">
        <div class="container-fluid">
            <div class="row justify-content-center mt-3">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h4>Crear Empleado</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('empleado.store')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" value="{{$id}}" name="idCandidato">
<div class="form-group">
                                            <div class=form-control">
                                                <label for="fechaImss">Fecha de Alta de Imss</label>
                                                <input type="date" class="form-control" name="fechaImss"
                                                       value="{{ old('fechaImss') }}">
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="puesto">Puesto</label>
                                            <select name="puesto" id="" style="text-transform:uppercase"
                                                    @if($errors->has('puesto')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif >
                                                <option value="" selected hidden>Puesto</option>
                                                @foreach($puestos as $puesto)
                                                    @if (Request::old('puesto') == $puesto->id)
                                                        <option value="{{$puesto->id}}"
                                                                selected>{{$puesto->nombre}}</option>
                                                    @else
                                                        <option value="{{$puesto->id}}">{{$puesto->nombre}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="tipoPuesto">Tipo de Puesto</label>
                                            <select name="tipoPuesto" id="" style="text-transform:uppercase"
                                                    @if($errors->has('tipoPuesto')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif>

                                                <option value="" selected hidden>Tipo de Puesto</option>
                                                @foreach($tipoPuestos as $tipoPuesto)
                                                    @if (Request::old('tipoPuesto') == $tipoPuesto->id)
                                                        <option value="{{$tipoPuesto->id}}"
                                                                selected>{{$tipoPuesto->nombre}}</option>
                                                    @else
                                                        <option
                                                            value="{{$tipoPuesto->id}}">{{$tipoPuesto->nombre}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="detallePuesto">Detalle de Puesto</label>
                                            <input type="text" name="detallePuesto" style="text-transform:uppercase" value="{{ old('detallePuesto') }}"
                                                   @if($errors->has('detallePuesto')) class="form-control is-invalid"
                                                   @else class="form-control" @endif>
                                        </div>

                                        <div class="form-group">
                                            <label for="subArea">Sub Area</label>
                                            <select name="subArea" id="subArea" style="text-transform:uppercase"
                                                    @if($errors->has('subArea')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif>

                                                <option value="" selected hidden>Sub Area</option>
                                                @foreach($subAreas as $subArea)
                                                    @if (Request::old('subArea') == $subArea->id)
                                                        <option value="{{$subArea->id}}"
                                                                selected>{{$subArea->subarea}}</option>
                                                    @else
                                                        <option value="{{$subArea->id}}">{{$subArea->subarea}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="kpi">Kpi</label>
                                            <input type="text" class="form-control" id="kpi" name="kpi" maxlength="4">
                                        </div>

                                        <div class="form-group">
                                            <label for="sueldoDiario">Sueldo Diario</label>
                                            <input type="text" name="sueldoDiario"  value="{{ old('sueldoDiario') }}"
                                                   @if($errors->has('sueldoDiario')) class="form-control is-invalid"
                                                   @else class="form-control" @endif maxlength="5">
                                        </div>

                                        <div class="form-group">
                                            <label for="sueldoMensual">Sueldo Mensual</label>
                                            <input type="text" name="sueldoMensual" value="{{ old('sueldoMensual') }}"
                                                   @if($errors->has('sueldoMensual')) class="form-control is-invalid"
                                                   @else class="form-control" @endif maxlength="10">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="cuentaClabe">Cuenta Clabe</label>
                                            <input type="text" name="cuentaClabe" value="{{ old('cuentaClabe') }}"
                                                   @if($errors->has('cuentaClabe')) class="form-control is-invalid"
                                                   @else class="form-control" @endif maxlength="18">
                                        </div>

                                        <div class="form-group">
                                            <label for="banco">Banco</label>
                                            <select name="banco" id="" style="text-transform:uppercase"
                                                    @if($errors->has('banco')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif>
                                                <option value="" selected hidden>Banco</option>
                                                @foreach($bancos as $banco)
                                                    @if (Request::old('banco') == $banco->id)
                                                        <option value="{{$banco->id}}"
                                                                selected>{{$banco->nombre}}</option>
                                                    @else
                                                        <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="empresa">Empresa</label>
                                            <select name="empresa" id="" style="text-transform:uppercase"
                                                    @if($errors->has('empresa')) class="custom-select is-invalid"
                                                    @else class="custom-select" @endif>
                                                <option value="" selected hidden>Empresa</option>
                                                @foreach($empresas as $empresa)
                                                    @if (Request::old('empresa') == $empresa->id)
                                                        <option value="{{$empresa->id}}"
                                                                selected>{{$empresa->nombre}}</option>
                                                    @else
                                                        <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <div class=form-control">
                                                <label for="fechaAsignacion">Fecha de Asignacion</label>
                                                <input type="date" class="form-control" name="fechaAsignacion"
                                                       value="{{ old('fechaAsignacion') }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class=form-control">
                                                <label for="fechaAsignacion">IMSS</label>
                                                <input type="text" class="form-control" name="imss" maxlength="16" style="text-transform:uppercase"
                                                       value="{{ old('imss') }}" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class=form-control">
                                                <label for="fechaAsignacion">CURP</label>
                                                <input type="text" class="form-control" name="curp" maxlength="18" style="text-transform:uppercase"
                                                       value="{{ old('curp') }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class=form-control">
                                                <label for="fechaAsignacion">RFC</label>
                                                <input type="text" class="form-control" name="rfc" maxlength="13" style="text-transform:uppercase"
                                                       value="{{ old('rfc') }}">
                                            </div>
                                        </div>

                                      

                                    </div>

                                    <div class="row mx-auto pt-2">
                                        <button type="sumbit" class="btn btn-primary btn-md">Registrar</button>
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
