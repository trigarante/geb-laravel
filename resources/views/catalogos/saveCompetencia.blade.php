<div class="modal fade" id="saveCompetencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Catalogo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="{{route('catalogos.saveCompetencia')}}" method="POST">
          @csrf
          <div class="md-form">
              <input type="text" id="escala" class="form-control" name="escala">
              <label for="escala">Escala</label>
          </div>
          <div class="md-form">
              <input type="text" id="estado" class="form-control" name="estado">
              <label for="estado">Estado</label>
          </div>
          <div class="md-form">
              <input type="text" id="descripcion" class="form-control" name="descripcion">
              <label for="descripcion">Descripcion</label>
          </div>
          <button type="submit" class="btn btn-success" id="guardar">Guardar</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>