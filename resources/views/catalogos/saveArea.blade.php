<div class="modal fade" id="saveArea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Catalogo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="{{route('catalogos.saveArea')}}" method="POST">
          @csrf
          <div class="md-form">
              <input type="text" id="nombre" class="form-control" name="nombre">
              <label for="nombre">Nombre</label>
          </div>
          <select name="empresa" id="empresa" class="custom-select">
            <option value="" selected>Empresa</option>
            @foreach($empresas as $empresa)
              <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
            @endforeach
          </select>
          <div class="md-form">
              <textarea type="text" id="descripcion" class="md-textarea form-control" rows="3" name="descripcion"></textarea>
              <label for="descripcion">Decripcion</label>
          </div>

          <button type="submit" class="btn btn-success" id="guardar">Guardar</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>