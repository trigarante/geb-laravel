@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Catalogos/ Vacantes</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <button class="btn btn-success" data-toggle="modal" data-target="#saveArea">Agregar
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <th>Nombre</th>
                                    <th>Sub Area</th>
                                    <th>Puesto</th>
                                    <th>Tipo de Puesto</th>
                                    <th>Tipo de Empleado</th>
                                    <th>Accion</th>
                                    </thead>
                                    <tbody>
                                    @foreach($vacantes as $vacante)
                                        <tr>
                                            <td>{{$vacante->nombre}}</td>
                                            <td>{{$vacante->subarea->subarea}}</td>
                                            <td>{{$vacante->puesto->nombre}}</td>
                                            <td>{{$vacante->tipo_puesto->nombre}}</td>
                                            <td>{{$vacante->tipo_usuario->tipo}}</td>
                                            <td>
                                                <button class="btn btn-warning"
                                                        style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff"
                                                        data-toggle="modal"
                                                        data-target="#modificar" data-id="{{$vacante->id}}"
                                                        data-nombre="{{$vacante->nombre}}"
                                                        data-id_empresa="{{$vacante->id_empresa}}"
                                                        data-descripcion="{{$vacante->descripcion}}">Modificar
                                                </button>
                                                <button class="btn btn-info"
                                                        style="background-color: #2b5182; border-color: #2b5182; " data-toggle="modal"
                                                        data-target="#deleteModal" data-id="{{$vacante->id}}"
                                                        data-catalogo="area">Eliminar
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
