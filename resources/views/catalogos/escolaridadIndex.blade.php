@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Catalogos/ Area</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <button class="btn btn-success" data-toggle="modal" data-target="#saveEscolaridad">Agregar
                            </button>
                        </div>
                        <div class="card-body">
                            <table id="escolaridad_table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Nivel</th>
                                    <th>Accion</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($escolaridades as $escolaridad)
                                    @if($escolaridad->activo == 1)
                                        <tr>
                                            <td>{{$escolaridad->nivel}}</td>
                                            <td>
                                                <button class="btn btn-warning"
                                                        style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff"
                                                        data-toggle="modal"
                                                        data-target="#updateEscolaridad"
                                                        data-id="{{$escolaridad->id}}"
                                                        data-nivel="{{$escolaridad->nivel}}">Modificar
                                                </button>
                                                <button class="btn btn-info"
                                                        style="background-color: #2b5182; border-color: #2b5182; "
                                                        data-toggle="modal"
                                                        data-target="#deleteModal" data-id="{{$escolaridad->id}}"
                                                        data-catalogo="escolaridad">Eliminar
                                                </button>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="saveEscolaridad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Catalogo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form  method="POST" id="form">
                        @csrf
                        <div class="form-group">
                            <label for="nivel">Nivel</label>
                            <input type="text" id="nivel" class="form-control" name="nivel" style="text-transform:uppercase">
                        </div>
                        <button type="submit" class="btn btn-success" id="save">Guardar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updateEscolaridad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modificar Catalogo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form id="formUpdate" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" id="id" name="id">
                            <label for="nivel">Nivel</label>
                            <input type="text" id="nivel" class="form-control" name="nivel" style="text-transform:uppercase">
                        </div>
                        <button type="submit" class="btn btn-success" id="update">Guardar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--Modal: modalConfirmDelete-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">¿Seguro que desea eliminar?</p>
                </div>

                <!--Body-->
                <div class="modal-body">

                    <i class="fa fa-times fa-4x animated rotateIn"></i>
                    <form action="{{route('catalogos.destroy')}}">
                        @csrf
                        <input type="hidden" id="catalogo" name="catalogo">
                        <input type="hidden" id="id" name="id">
                        <button class="btn  btn-outline-danger" type="submit">Si</button>
                    </form>
                    <button type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</button>

                </div>

                <!--Footer-->

            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalConfirmDelete-->
@endsection

@section('scripts')
    <script type="text/javascript">
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var catalogo = button.data('catalogo')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #catalogo').val(catalogo);
        })
    </script>

    <script type="text/javascript">
        $('#updateEscolaridad').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var nivel = button.data('nivel')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #nivel').val(nivel);
        })
    </script>
    <script src="{{asset('/DataTables/datatables.min.js')}}"></script>
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#escolaridad_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            jQuery('#save').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{route('catalogos.saveEscolaridad')}}",
                    method: 'post',
                    data: jQuery('#form').serialize(),
                    beforeSend: function () {
                        $('#save').prop("disabled", true);
                    },
                    success: function (result) {
                        if (result.errors) {
                            $('#save').prop("disabled", false);
                            jQuery('.alert-danger').html('');
                            jQuery.each(result.errors, function (key, value) {
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>' + value + '</li>');
                            });
                        }
                        else {
                            $("#form").get(0).reset();
                            jQuery('.alert-danger').hide();
                            $('#saveEscolaridad').modal('hide');
                            location.reload();
                        }
                    }
                });
            });
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            jQuery('#update').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{route('catalogos.updateEscolaridad')}}",
                    method: 'post',
                    data: jQuery('#formUpdate').serialize(),
                    beforeSend: function () {
                        $('#update').prop("disabled", true);
                    },
                    success: function (result) {
                        if (result.errors) {
                            $('#update').prop("disabled", false);
                            jQuery('.alert-danger').html('');
                            jQuery.each(result.errors, function (key, value) {
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>' + value + '</li>');
                            });
                        }
                        else {

                            jQuery('.alert-danger').hide();
                            $('#updateEscolaridad').modal('hide');
                            location.reload();
                        }
                    }
                });
            });
        });
    </script>
@endsection
