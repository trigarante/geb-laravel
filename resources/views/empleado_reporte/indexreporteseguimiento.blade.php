@extends('layouts.appLayout')
@section('styles')
<!--<link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
{{--mensaje exito--}}
@if(session()->has('mensaje'))
<div class="alert alert-success">
    {{ session()->get('mensaje') }}
</div>
@endif
{{--etiqueta ubicacion--}}
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item active">Reporte Seguimiento</li>
        </ul>
    </div>
</div>
{{--tabla--}}
<section class="mt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">                
                    <div class="card-body">
                        <div class="card-header">                   
                            <button class="btn btn-success btn-sm"
                                    style="background-color: #1e8435; border-color: #1e8435;"
                                    id="mostrarFecha"                                                        
                                    data-toggle="modal" data-target="#rangoFechasModal">Buscar por Fechas
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table id="candidatos_table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Fecha Ingreso</th>
                                        <th>Reclutador</th>                                    
                                        <th>Estado</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($seguimientos as $seguimiento)                                
                                    <tr>
                                        <td>{{$seguimiento->nombre." ".$seguimiento->apellido_paterno." ".$seguimiento->apellido_materno}}</td>
                                        <td>{{ Carbon\Carbon::parse($seguimiento->fecha_ingreso)->format('d-m-Y ') }}</td>
                                        @foreach ($solicitud as $sol)
                                            @if ($sol->id === $seguimiento->id)
                                                @foreach($ejecutivo as $eje)
                                                    @if($sol->id_reclutador===$eje->id) 
                                                        <td> {{$eje->nombre." ".$eje->apellido_paterno." ".$eje->apellido_materno}}</td>                    
                                                    @endif 
                                                @endforeach
                                                
                                            @endif
                                        @endforeach
                                        <td>Reclutador</td>
                                        <td>{{$seguimiento->estado}}</td>                                            
                                    </tr>                                
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>
</section>

<div class="modal fade" id="rangoFechasModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Selecciona el rango de Fechas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="GET" action="{{route('empleado_reporte.indexreporteseguimiento')}}">                        
                <div class="modal-body">           

                    @csrf
                    Inicio
                    <input type="date" class="form-control" name="fechainicio" id="fechainicio">
                    Fin
                    <input type="date" class="form-control" name="fechafin" id="fechafin">


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="agendar" >Desplegar</button>        
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!--Modal: modalFechas-->

@endsection
@section('scripts')
<!--<script src="{{asset('DataTables/datatables.min.js')}}"></script>-->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script>    
    /****************************************
     *       Table Search and Export        *
     ****************************************/


    $(document).ready(function () {
        var mensaje = "Reporte candidato de <?= $fechaInicio ?> a <?= $fechaFin ?>";
        var table = $('#candidatos_table').DataTable({
            dom:
                    "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
            lengthChange: true,
            buttons: ['copy', {
                    extend: 'excel',
                    filename: mensaje,
                    messageTop: mensaje

                },
                {
                    extend: 'pdf',
                    filename: mensaje,
                    messageTop: mensaje

                }]
        });
        table.buttons().container()
                .appendTo('#candidatos_table_wrapper .col-md-6:eq(0)');
    });
</script>
@endsection
