@extends('layouts.appLayout')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
{{--mensaje exito--}}
@if(session()->has('mensaje'))
<div class="alert alert-success">
    {{ session()->get('mensaje') }}
</div>
@endif
{{--etiqueta ubicacion--}}
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item active">Reporte Empleados IMSS</li>
        </ul>
    </div>
</div>
{{--tabla--}}
<section class="mt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header">                         
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="precandidato_table">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Puesto</th>
                                        <th>Área</th>                                    
                                        <th>Campaña</th>                                    
                                        <th>Fecha Ingreso</th>                                    
                                    </tr>
                                </thead>
                                <tbody>                                
                                    @foreach($imssview as $imss)
                                    <tr>
                                        <td>{{$imss->nombre." ".$imss->apellido_paterno." ".$imss->apellido_materno}}</td>
                                        <td>{{$imss->puesto}}</td>
                                        <td>{{$imss->area}}</td>                                    
                                        @if ($imss->id_tipo ===1)
                                        <td>Usuario administrativo</td>

                                        @endif
                                        @if ($imss->id_tipo ===2)
                                            <td>@foreach($ejecutivo as $eje)  

                                                @if ($eje->id_empleado === $imss->id)
                                                    
                                                    @if($eje->campana!= "")
                                                    {{$campana=$eje->campana}}
                                                    @endif
                                                    @else
                                                    Ejecutivo no asignado
                                                    
                                                @endif

                                                   
                                            @endforeach</td>

                                        @endif

                                        <td>{{ Carbon\Carbon::parse($imss->fecha_ingreso)->format('d-m-Y ') }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>
</section>

<!--Modal:-->
@endsection
@section('scripts')

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<script type="text/javascript">
$('#rangoFechasModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    modal.find('.modal-body #id').val(id);
})
</script>

<script>
    /****************************************
     *       Table Search and Export        *
     ****************************************/


    $(document).ready(function () {
        var mensaje = "Reporte IMSS";
        var table = $('#precandidato_table').DataTable({
            dom:
                    "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
            lengthChange: true,
            buttons: ['copy', {
                    extend: 'excel',
                    filename: mensaje,
                    messageTop: mensaje

                },
                {
                    extend: 'pdf',
                    filename: mensaje,
                    messageTop: mensaje

                }]
        });
        table.buttons().container()
                .appendTo('#precandidato_table_wrapper .col-md-6:eq(0)');
    });
</script>
@endsection
