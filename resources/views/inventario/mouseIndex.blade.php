@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje de error--}}
    @if(count($errors)>0)
    <div class="alert alert-danger"> 
        <ul>
          @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
          @endforeach
        </ul>
    </div>
    @endif
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif  
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">INVENTARIO MOUSE</li>
            </ul>
        </div>
    </div> 
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                @if(auth()->user()->grupo_usuarios->inventarios == 1)
                                 <button  class="btn success-color-dark btn-sm"  style="background-color: #44aa46;   border-color: #216d32; " data-toggle="modal" data-target="#saveMouse">
                                 Agregar</button>
                                @endif

                                 <table id="mouse_table" class="table  table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>

                                            <th>Número de folio</th>
                                            <th>Marca</th>
                                            <th>Estado Físico</th>   
                                            <th>Fecha Recepcion</th>
                                            @if(auth()->user()->grupo_usuarios->inventarios == 1)
                                            <th>Accion</th>
                                            @endif 
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($mouseV as $mouse_view)
                                        @if($mouse_view->id_entrega == 1)
                                        <tr>
                                            <td>{{$mouse_view->num_folio}}</td>
                                            <td>{{$mouse_view->marca}}</td>
                                            <td>{{$mouse_view->estado}}</td>
                                            <td>{{$mouse_view->fecha_recepcion}}</td>
                                             <td>
                                                @if(auth()->user()->grupo_usuarios->inventarios == 1)
                                                <button class="btn warning-color-dark btn-sm"  style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff" data-toggle="modal" data-target="#modificar" 
                                                data-id="{{$mouse_view->id}}"
                                                data-num_folio = "{{$mouse_view->num_folio}}" 
                                                data-id_estado = "{{$mouse_view->id_estado}}" 
                                                data-marca = "{{$mouse_view->marca}}" 
                                                data-fecha_recepcion = "{{$mouse_view->fecha_recepcion}}">Modificar</button>
                                                <button class="btn danger-color-dark btn-sm"  data-toggle="modal" data-target="#deleteModal" data-id="{{$mouse_view->id}}"
                                                data-catalogo= "monitor">Eliminar</button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>                          
                          </div>
                     </div>
                 </div>
            </div>
        </div>
</section>



<!--Modal: modalConfirmDelete-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">¿Seguro que desea eliminar?</p>
            </div>

            <!--Body-->
            <div class="modal-body">

                <i class="fa fa-times fa-4x animated rotateIn"></i>
                <form action="{{route('mouse.destroyMouse')}}" >
                    @csrf
                    <input type="hidden" id="catalogo" name="catalogo">
                    <input type="hidden" id="id" name="id"> 
                    <button class="btn  btn-outline-danger" type="submit">Si</button>
                </form>
                <button type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</button>

            </div>

            <!--Footer-->

        </div>
        <!--/.Content-->
    </div>
</div>


<!--Modal: modalConfirmDelete-->

<div class="modal fade" id="saveMouse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear Elemento Inventario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  action="{{route('mouse.saveMouse')}}" method="POST">
                    @csrf
                    @if(count($errors)>0)
                    <div class="alert alert-warning">
                    <strong>Revisa que todos los datos estén correctos antes de guardar.</strong>
                    </div>
                    @endif
                    <div class="md-form">
                        <label for="num_folio">Número de folio</label>
                        <input type="text"  class="form-control" name="num_folio" id="num_folio" 
                         value="{{ old('num_folio') }}" @if($errors->has('num_folio')) 
                        class="form-control is-invalid" @else class="form-control" @endif>
                    
                    </div>
                    <div class="md-form">
                        <label for="marca">Marca</label>
                        <input type="text" id="marca" class="form-control" name="marca"
                        value="{{ old('marca') }}" @if($errors->has('marca')) 
                        class="form-control is-invalid" @else class="form-control" @endif>
                    </div>
                    
                    <label for="estadoinventario">Estado Físico</label>
                    <select name="estadoinventario" id="estadoinventario" class="custom-select" 
                    value="{{ old('estadoinventario') }}" @if($errors->has('estadoinventario')) 
                        class="form-control is-invalid" @else class="form-control" @endif>            
                        @foreach($estadoinventarios as $estadoinventario)
                        <option value="{{$estadoinventario->id}}">{{$estadoinventario->estado}}</option>
                        @endforeach
                    </select>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="guardar">Guardar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Actualizar Datos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  action="{{route('mouse.updateMouse')}}" method="POST">
                    @csrf
                    @if(count($errors)>0)
                    <div class="alert alert-warning">
                    <strong>Revisa que los cambios sean correctos antes de guardar.</strong>
                    </div>
                    @endif
                    <input type="hidden" id="id" name="id">
                    <div class="md-form">
                        <label for="num_folio">Número de folio</label>
                        <input type="text"  class="form-control" name="num_folio" id="num_folio">

                    </div>
                    <div class="md-form">
                        <label for="marca">Marca</label>
                        <input type="text" id="marca" class="form-control" name="marca">
                    </div>
                    
                    <label for="estadoinventario">Estado Físico</label>
                    <select name="estadoinventario" id="estadoinventario" class="custom-select">            
                        @foreach($estadoinventarios as $estadoinventario)
                        <option value="{{$estadoinventario->id}}">{{$estadoinventario->estado}}</option>
                        @endforeach
                    </select>          
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="guardar">Guardar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


@endsection
@section('scripts')

<script src="{{asset('/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var catalogo = button.data('catalogo')
        var modal = $(this)

        modal.find('.modal-body #id').val(id);
        modal.find('.modal-body #catalogo').val(catalogo);
    })
</script>

<script type="text/javascript">
    $('#modificar').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var num_folio = button.data('num_folio')
        var marca = button.data('marca')
        var id_estado = button.data('id_estado')
        //var dimensiones = button.data('dimensiones')
        //var estadoinventario = button.data('estadoinventario')
        var modal = $(this)

        modal.find('.modal-body #id').val(id);
        modal.find('.modal-body #num_folio').val(num_folio);
        modal.find('.modal-body #marca').val(marca);
        modal.find('.modal-body #estadoinventario').val(id_estado);
        //modal.find('.modal-body #dimensiones').val(dimensiones);
        //modal.find('.modal-body #estadoinventario').val(estadoinventario);
    })
</script>
<script src="{{asset('/DataTables/datatables.min.js')}}"></script>
{{--datatables script--}}
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#mouse_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>

   
  

@endsection

