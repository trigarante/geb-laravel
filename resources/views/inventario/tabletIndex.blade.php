@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje de error--}}
    @if(count($errors)>0)
    <div class="alert alert-danger"> 
        <ul>
          @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
          @endforeach
        </ul>
    </div>
    @endif
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif 
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">INVENTARIO TABLET</li>
            </ul>
        </div>
    </div> 
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                    @if(auth()->user()->grupo_usuarios->inventarios == 1)
                                 <button  class="btn success-color-dark btn-sm"  style="background-color: #44aa46;   border-color: #216d32; " data-toggle="modal" data-target="#saveTablet">
                                 Agregar</button>
                                @endif
                                     <table id="tablet_table" class="table  table-bordered" cellspacing="0" width="100%">
                                        <thead>      
                                            <tr>
                                                <th>Número de folio</th>
                                                <th>Procesador</th>
                                                <th>Capacidad</th>    
                                                <th>RAM</th> 
                                                <th>Marca</th>                                              
                                                <th>Telefono</th> 
                                                <th>Estado Físico</th>                                          
                                                <th>Fecha Recepción</th>
                                                <th>Acción</th>                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($tablet as $tablet_view)
                                            @if($tablet_view->id_entrega== 1) 
                                            <tr>
                                                <td>{{$tablet_view->num_folio}}</td>
                                                <td>{{$tablet_view->procesador}}</td>
                                                <td>{{$tablet_view->capacidad}}</td>
                                                <td>{{$tablet_view->ram}}</td>
                                                <td>{{$tablet_view->marca}}</td>
                                                <td>{{$tablet_view->telefono}}</td>
                                                <td>{{$tablet_view->estado}}</td>
                                                <td>{{$tablet_view->fecha_recepcion}}</td>      
                                                <td>
                                                    @if(auth()->user()->grupo_usuarios->inventarios == 1)
                                                    <button class="btn warning-color-dark btn-sm"  style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff" data-toggle="modal" data-target="#modificar" data-id="{{$tablet_view->id}}"
                                                            data-num_folio= "{{$tablet_view->num_folio}}" data-id_estado = "{{$tablet_view->id_estado}}" data-marca = "{{$tablet_view->marca}}" 
                                                            data-procesador = "{{$tablet_view->procesador}}" data-ram = "{{$tablet_view->ram}}" data-fecha_recepcion = "{{$tablet_view->fecha_recepcion}}" data-telefono = "{{$tablet_view->telefono}}" data-capacidad = "{{$tablet_view->capacidad}}" >Modificar</button>
                                                    <button class="btn danger-color-dark btn-sm" data-toggle="modal" data-target="#deleteModal" data-id="{{$tablet_view->id}}"
                                                            data-catalogo= "tablet">Eliminar</button>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>                          
                          </div>
                     </div>
                 </div>
            </div>
        </div>
</section>


 
 <!--Modal: modalConfirmDelete-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">¿Seguro que desea eliminar?</p>
            </div>

            <!--Body-->
            <div class="modal-body">

                <i class="fa fa-times fa-4x animated rotateIn"></i>
                <form action="{{route('tablet.destroyTablet')}}" >
                    @csrf
                    <input type="hidden" id="catalogo" name="catalogo">
                    <input type="hidden" id="id" name="id"> 
                    <button class="btn  btn-outline-danger" type="submit">Si</button>
                </form>
                <button type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</button>

            </div>

            <!--Footer-->

        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: modalConfirmDelete-->



<div class="modal fade" id="saveTablet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear Elemento Inventario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  action="{{route('tablet.saveTablet')}}" method="POST">
                    @csrf
                    @if(count($errors)>0)
                    <div class="alert alert-warning">
                    <strong>Revisa que todos los datos estén correctos antes de guardar.</strong>
                    </div>
                    @endif                    
                    <div class="md-form">
                        <label for="num_folio">Número de folio</label>
                        <input type="text"  class="form-control" name="num_folio" id="num_folio"
                        value="{{ old('num_folio') }}" @if($errors->has('num_folio')) 
                        class="form-control is-invalid" @else class="form-control" @endif>
                    </div>
                    <div class="md-form">
                        <label for="procesador">Procesador</label>
                        <input type="text" id="procesador" class="form-control" name="procesador"
                        value="{{ old('procesador') }}" @if($errors->has('procesador')) 
                        class="form-control is-invalid" @else class="form-control" @endif>
                    </div>
                    <div class="md-form">
                        <label for="capacidad">Capacidad</label>
                        <input type="text" id="capacidad" class="form-control" name="capacidad"
                        value="{{ old('capacidad') }}" @if($errors->has('capacidad')) 
                        class="form-control is-invalid" @else class="form-control" @endif>
                    </div>
                    <div class="md-form">
                        <label for="ram">RAM</label>
                        <input type="text" id="ram" class="form-control" name="ram"
                        value="{{ old('ram') }}" @if($errors->has('ram')) 
                        class="form-control is-invalid" @else class="form-control" @endif>
                    </div>
                    <div class="md-form">
                        <label for="marca">Marca</label>
                        <input type="text" id="marca" class="form-control" name="marca"
                         value="{{ old('marca') }}" @if($errors->has('marca')) 
                        class="form-control is-invalid" @else class="form-control" @endif>
                    </div>
                    <div class="md-form">
                        <label for="telefono">Telefono</label>
                        <input type="text" id="telefono" class="form-control" name="telefono"
                        value="{{ old('telefono') }}" @if($errors->has('telefono')) 
                        class="form-control is-invalid" @else class="form-control" @endif>
                    </div>
                    <label for="estadoinventario">Estado Físico</label>
                    <select name="estadoinventario" id="estadoinventario" class="custom-select" 
                    value="{{ old('estadoinventario') }}" @if($errors->has('estadoinventario')) 
                        class="form-control is-invalid" @else class="form-control" @endif>            
                        @foreach($estadoinventarios as $estadoinventario)
                        <option value="{{$estadoinventario->id}}">{{$estadoinventario->estado}}</option>
                        @endforeach
                    </select>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="guardar">Guardar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Actualizar Datos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  action="{{route('tablet.updateTablet')}}" method="POST" >
                    @csrf
                    @if(count($errors)>0)
                    <div class="alert alert-warning">
                    <strong>Revisa que los cambios sean correctos antes de guardar.</strong>
                    </div>
                    @endif
                    <input type="hidden" id="id" name="id">
                    <div class="md-form">
                        <label for="num_folio">Número de serie</label>
                        <input type="text"  class="form-control" name="num_folio" id="num_folio">
                        <div id="errorNfolio"></div>
                    </div>
                    <div class="md-form">
                        <label for="procesador">Procesador</label>
                        <input type="text" id="procesador" class="form-control" name="procesador">
                    </div>
                    <div class="md-form">
                        <label for="capacidad">Capacidad</label>
                        <input type="text" id="capacidad" class="form-control" name="capacidad">
                    </div>
                    <div class="md-form">
                        <label for="ram">RAM</label>
                        <input type="text" id="ram" class="form-control" name="ram">
                    </div>
                    <div class="md-form">
                        <label for="marca">Marca</label>
                        <input type="text" id="marca" class="form-control" name="marca">
                    </div>
                    <div class="md-form">
                        <label for="telefono">Telefono</label>
                        <input type="text" id="telefono" class="form-control" name="telefono">
                    </div>
                    <label for="estadoinventario">Estado Físico</label>
                    <select name="estadoinventario" id="estadoinventario" class="custom-select">            
                        @foreach($estadoinventarios as $estadoinventario)
                        <option value="{{$estadoinventario->id}}">{{$estadoinventario->estado}}</option>
                        @endforeach
                    </select>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="guardar">Guardar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{asset('/DataTables/datatables.min.js')}}"></script>

<script type="text/javascript">
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var catalogo = button.data('catalogo')
        var modal = $(this)

        modal.find('.modal-body #id').val(id);
        modal.find('.modal-body #catalogo').val(catalogo);
    })
</script>

<script type="text/javascript">
    $('#modificar').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var num_folio = button.data('num_folio')
        var procesador = button.data('procesador')
        var capacidad = button.data('capacidad')
        var ram = button.data('ram')
        var marca = button.data('marca')
        var telefono = button.data('telefono')
        var id_estado = button.data('id_estado')
        //var estadoinventario = button.data('estadoinventario')
        var modal = $(this)

        modal.find('.modal-body #id').val(id);
        modal.find('.modal-body #num_folio').val(num_folio);
        modal.find('.modal-body #procesador').val(procesador);
        modal.find('.modal-body #capacidad').val(capacidad);
        modal.find('.modal-body #ram').val(ram);
        modal.find('.modal-body #marca').val(marca);
        modal.find('.modal-body #telefono').val(telefono);
        modal.find('.modal-body #estadoinventario').val(id_estado);
        
        //modal.find('.modal-body #estadoinventario').val(estadoinventario);
    })
</script>

{{--datatables script--}}
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#tablet_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>


    
@endsection

