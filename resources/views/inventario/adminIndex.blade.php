@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif 
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">ASIGNACIÓN DE EQUIPO PARA ADMINISTRATIVOS</li>
            </ul>
        </div>
    </div>  
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12"> 
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                               
                                <table class="table table-bordered" id="inventario_table">
                                    <thead>
                                            <tr> 
                                                <th>Usuario</th>
                                                <th>Nombre</th>
                                                <th>Correo</th>
                                                <th>Area</th> 
                                               
                                                 @if(auth()->user()->grupo_usuarios->inventarios == 1)
                                                <th>Asignación</th>
                                                @endif

                                            </tr>
                                    </thead> 
                                    <tbody>
                                        @foreach($empleados as $empleado)
                                        @if($empleado->equipo == '0')
                                        <tr>
                                            
                                                <td>{{$empleado->usuario}}</td>
                                                <td>{{$empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno}}</td>
                                                <td>{{$empleado->correo}}</td>
                                                 <td>{{$empleado->area}}</td>
                                                
                                                  
                                                <td>
                                                @if(auth()->user()->grupo_usuarios->inventarios == 1)
                                                    <a   class="btn btn-success"   style="background-color: #44aa46;   border-color: #216d32;" href="{{route('inventario.createad',[$empleado->id,$empleado->id_area])}}">Asignar Equipo</a>
                                                @endif
                                                </td>                    
                                                
                                        </tr>  
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                             </div>                   
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
       <!--Modal: modalConfirmDelete-->
@endsection
@section('scripts')
<script src="{{asset('/DataTables/datatables.min.js')}}"></script>
{{--datatables script--}}
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#inventario_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>
@endsection
