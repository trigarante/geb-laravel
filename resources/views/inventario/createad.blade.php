@extends('layouts.appLayout')
@section('content')
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('inventario.admin.index')}}">Asignación de Equipo</a></li>
                <li class="breadcrumb-item active">Asignar Equipo/Administrativos</li>
            </ul>
        </div> 
    </div>  
    {{--errores--}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  

 <section class="forms">
        <div class="container-fluid">
            <div class="row justify-content-center mt-3">
                <div class="col-lg-8">
                    <div class="card">
                         <div class="card-header d-flex align-items-center">
                            <h4>Asignar Equipo </h4>
                        </div>
                        <div class="card-body">
                            
                        <form action="{{route('inventario.asignar')}}" method="post">                        
                            @csrf
                              <div class="row">
                                <table id="dtBasicExample" class="table table-condensed" cellspacing="0" width="100%">
                                <tr>
                                    <th>Nombre: </th>
                                    <th>{{$empleados->nombre." ".$empleados->apellido_paterno." ".$empleados->apellido_materno}} </th>
                                </tr>                       
                                <tr>
                                    <th>Usuario: </th>
                                    <th>{{$empleados->usuario}}</th>
                                </tr>
                                <tr>
                                    <th>Area: </th>
                                    <th>{{$empleados->area}}</th>
                                </tr>
                                <tr>
                                    <th>Campaña: </th>
                                    <th>{{$empleados->subarea}}</th>
                                </tr>
                                </table>
                               
                                
                                    <input type="hidden" value="{{$id}}" name="id_ejecutivo">
                                    

                                    
                           
                                        <div class="col-md-6">
                                            <div class="form-group">
                                               <label for="">Teclados</label>
                                                <select name="asigna_teclado" id="asigna_teclado" @if($errors->has('$asigna_teclado')) class="custom-select is-invalid" @else class="custom-select" @endif>
                                                <option value="" selected>Teclados disponible</option>
                                                @foreach($teclados as $teclado)                                     
                                                <option value="{{$teclado->id}}">{{$teclado->marca}}{{$teclado->num_folio}}</option>                   
                                                @endforeach
                                                </select>
                                            </div>  
                                            
                                            <div class="form-group">
                                                <label for="">Mouse</label>
                                                <select name="asigna_mouse" id="asigna_mouse" @if($errors->has('$asigna_mouse')) class="custom-select is-invalid" @else class="custom-select" @endif>
                                                <option value="">Mouse disponible</option>
                                                @foreach($mice as $mouse)
                                                <option value="{{$mouse->id}}">{{$mouse->marca}}{{$mouse->num_folio}}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                     
                                              
                                       
                                            <div class="form-group">
                                                <label for="">Monitor</label>
                                                <select name="asigna_monitor" id="asigna_monitor" @if($errors->has('$asigna_monitor')) class="custom-select is-invalid" @else class="custom-select" @endif>
                                                <option value="" selected>Monitor disponible</option>
                                                @foreach($monitores as $monitor)     
                                                <option value="{{$monitor->id}}">{{$monitor->marca}} {{$monitor->num_serie}}</option>
                                                @endforeach
                                                </select>
                                            </div>      
                        
                                                
                                      
                                            <div class="form-group">
                                                <label for="">CPU</label>
                                                <select name="asigna_cpu" id="asigna_cpu" @if($errors->has('$asigna_cpu')) class="custom-select is-invalid" @else class="custom-select" @endif>
                                                <option value="" selected>CPU disponible</option>
                                                @foreach($cpus as $cpu)
                                                          
                                                <option value="{{$cpu->id}}">{{$cpu->marca}} {{$cpu->numero_serie}}</option>
                                            
                                                @endforeach
                                                </select>
                                            </div>
                                                                         
                                         <div class="form-group">
                                         <input type="submit" value="Registrar" class="btn btn-primary btn-md">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('scripts')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
@endsection


 