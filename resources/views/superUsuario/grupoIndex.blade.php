@extends('layouts.appLayout')

@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Grupos</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <button class="btn btn-success" data-toggle="modal" data-target="#grupoModalCreate">Crear
                                Grupo
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="gruposTable">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Descripcion</th>
                                        <th>Permisos</th>
                                        <th>Accion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($grupos as $grupo)
                                        <tr>
                                            <td>{{$grupo->grupo}}</td>
                                            <td>{{$grupo->descripcion}}</td>
                                            <td>
                                                <a href="{{route('superUsuario.permisos',$grupo->id)}}"
                                                   class="btn btn-info">Agregar / Quitar</a>
                                            </td>
                                            <td>
                                                <button class="btn btn-warning">Editar</button>
                                                <button class="btn btn-danger">Eliminar</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="grupoModalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Descripcion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('superUsuario.grupoSave')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="nombre" id="nombre">
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripcion</label>
                            <textarea name="descripcion" id="descripcion" cols="15" rows="5"
                                      class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="subArea">Sub Area</label>
                            <select name="subArea" id="subArea" class="custom-select">
                                @foreach($subAreas as $subArea)
                                    <option value="{{$subArea->id}}">{{$subArea->subarea}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="puesto">Puesto</label>
                            <select name="puesto" id="puesto" class="custom-select">
                                @foreach($puestos as $puesto)
                                    <option value="{{$puesto->id}}">{{$puesto->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tipoPuesto">Tipo de Puesto</label>
                            <select name="tipoPuesto" id="tipoPuesto" class="custom-select">
                                @foreach($tipoPuestos as $tipoPuesto)
                                    <option value="{{$tipoPuesto->id}}">{{$tipoPuesto->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
