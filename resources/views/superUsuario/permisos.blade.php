@extends('layouts.appLayout')

@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a>Grupos</a></li>
                <li class="breadcrumb-item active">Asignar Permisos</li>
            </ul>
        </div>
    </div>
    {{--formulario--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            Asignar Permisos
                        </div>
                        <div class="card-body">
                            <form action="{{route('superUsuario.savePermisos')}}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Vacantes</label>
                                            <select name="vacantes" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->vacantes)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Pre Candidato</label>
                                            <select name="precandidatos" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->precandidatos)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Candidato</label>
                                            <select name="candidato" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->candidato)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Seguimiento</label>
                                            <select name="seguimiento" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->seguimiento)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Empleados</label>
                                            <select name="empleados" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->empleados)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Usuarios</label>
                                            <select name="usuarios" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->usuarios)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Inventarios</label>
                                            <select name="inventarios" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->inventarios)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Contactos</label>
                                            <select name="contactos" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->contactos)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Clientes</label>
                                            <select name="clientes" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->clientes)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Agentes Externos</label>
                                            <select name="agentesExternos" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->agentes_externos)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Polizas</label>
                                            <select name="polizas" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->polizas)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Pagos</label>
                                            <select name="pagos" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->pagos)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Autorizacion</label>
                                            <select name="autorizacion" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->autorizacion)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Verificaciones</label>
                                            <select name="verificaciones" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->verificaciones)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">E&C</label>
                                            <select name="ec" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->ec)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Carga de Polizas</label>
                                            <select name="cargaPolizas" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->carga_polizas)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">

                                        <div class="form-group">
                                            <label for="">Contabilidad</label>
                                            <select name="contabilidad" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->contabilidad)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Nomina</label>
                                            <select name="nomina" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->nomina)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Transferencias</label>
                                            <select name="transferencias" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->tranferencias)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Transferencias Masivas</label>
                                            <select name="transferenciasMasivas" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->tranferencias_masivas)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Siniestros</label>
                                            <select name="siniestros" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->siniestros)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Penalizaciones</label>
                                            <select name="penalizaciones" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->penalizaciones)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Catalogos</label>
                                            <select name="catalogos" id="" class="custom-select">
                                                @foreach($tipoPermisos as $tipoPermiso)
                                                    <option value="{{$tipoPermiso->id}}" @if($tipoPermiso->id == $permisos->catalogos)selected @endif>{{$tipoPermiso->tipo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input type="hidden" value="{{$permisos->id}}" name="id">
                                    </div>
                                </div>
                                <div class="row justify-content-center mt-5">
                                    <button class="btn btn-success">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
