<div class="modal fade" id="modificarSolicitudModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" 
aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar Datos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="{{route('solicitud.update','test')}}" method="POST">
        @csrf
        {{method_field('patch')}}
        <input type="hidden" name="id" id="id">
        <div class="md-form">
          <input type="text"  class="form-control" name="nombre" id="nombre">
          
        </div>
        <div class="md-form">
          <input type="text"  class="form-control" name="apellido_paterno"  
          id="apellido_paterno">
    
        </div>
        <div class="md-form">
          <input type="text"  class="form-control" name="apellido_materno" 
          id="apellido_materno">
          
        </div>
        <div class="md-form">
          <input type="email"  class="form-control" name="email"  id="email">
          
        </div>
        <div class="md-form">
          <input type="text"  class="form-control" name="telefono" id="telefono">
          
        </div>
        <div class="md-form">
        </div>
        <input type="submit" class="btn btn-success" value="Registrar">
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

