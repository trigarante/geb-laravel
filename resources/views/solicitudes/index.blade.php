@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Solicitudes</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            @if(auth()->user()->grupo_usuarios->vacantes == 1)
                                <a href="{{route('solicitud.create')}}" class="btn btn-success display">Crear
                                    Solicitud</a>
                            @endif

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="solicitudes_table">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Vacante</th>
                                        <th>Tipo de Puesto</th>
                                        <th>Fecha de Cita</th>
                                        @if(auth()->user()->grupo_usuarios->vacantes == 1)
                                            <th>Cita</th>
                                        @endif
                                        <th>Pre Candidato</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($solicitudes as $solicitud)
                                        <tr>
                                            <td>{{$solicitud->nombre." ".$solicitud->apellido_paterno." ".$solicitud->apellido_materno}}</td>
                                            <td>{{$solicitud->telefono}}</td>
                                            <td>{{$solicitud->correo}}</td>
                                            <td>{{$solicitud->vacante}}</td>
                                            <td>{{$solicitud->tipo}}</td>
                                            <td>
                                                @if($solicitud->fecha_cita != '')
                                                    {{ Carbon\Carbon::parse($solicitud->fecha_cita)->format('d-m-Y h:i A') }}
                                                @else
                                                    No hay cita agendada
                                                @endif
                                            </td>
                                            @if(auth()->user()->grupo_usuarios->vacantes == 1)
                                                <td>
                                                    @if($solicitud->fecha_cita != '')
                                                        <button class="btn"
                                                                data-id='{{$solicitud->id}}'
                                                                data-toggle="modal"
                                                                data-target="#agendarCitaModal"
                                                                data-fecha='{{Carbon\Carbon::parse($solicitud->fecha_cita)->format('Y-m-d')}}'
                                                                data-hora='{{Carbon\Carbon::parse($solicitud->fecha_cita)->format('H:i')}}'
                                                                style="background-color: #2b5164;
                                                        border-color: #2b5164; color: #ffffff;">Cambiar Cita
                                                        </button>

                                                    @else
                                                        <button class="btn btn-info"
                                                                data-id='{{ $solicitud->id }}'
                                                                data-toggle="modal" data-target="#agendarCitaModal"
                                                                style="background-color: #2b5182; border-color: #2b5182; ">
                                                            Agendar Cita
                                                        </button>
                                                    @endif
                                                </td>
                                            @endif
                                            <td>

                                                @if($solicitud->estado == 0 && $solicitud->fecha_cita !=0)
                                                    @if(auth()->user()->grupo_usuarios->vacantes == 1)
                                                        <a href="{{route('preCandidatos.create',$solicitud->id)}}"
                                                           class="btn btn-success"
                                                           style="background-color: #1e8435; border-color: #1e8435;">Crear
                                                            Precandidato</a>
                                                    @endif
                                                @elseif($solicitud->estado == 1 && $solicitud->fecha_cita !=0)
                                                    <a href="{{route('preCandidatos.show',$solicitud->id)}}"
                                                       class="btn btn-success"
                                                       style="background-color: #216d32; border-color: #216d32;">Ver
                                                        Precandidato</a>
                                                @endif
                                            </td>
                                            {{--@if(auth()->user()->grupo_usuarios->vacantes == 1)--}}
                                                {{--<td>--}}
                                                    {{--<button class="btn btn-warning" data-id='{{ $solicitud->id }}'--}}
                                                            {{--data-nombre='{{ $solicitud->nombre }}'--}}
                                                            {{--data-apellido_paterno='{{ $solicitud->apellido_paterno }}'--}}
                                                            {{--data-apellido_materno='{{ $solicitud->apellido_materno }}'--}}
                                                            {{--data-email='{{ $solicitud->correo }}'--}}
                                                            {{--data-telefono='{{ $solicitud->telefono }}'--}}
                                                            {{--data-toggle="modal" data-target="#modificarSolicitudModal"--}}
                                                            {{--style="background-color: #e6bc12; border-color: #e6bc12; color: #ffffff">--}}
                                                        {{--Modificar--}}
                                                    {{--</button>--}}
                                                {{--</td>--}}
                                            {{--@endif--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    {{--modal cita--}}
    <div class="modal fade" id="agendarCitaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agendar Cita</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="agendarCitaForm" method="POST" action="{{route('solicitud.agendarCita')}}">
                        @csrf
                        <input type="hidden" class="form-control" name="id" id="id"><br>
                        <label for="fecha">Fecha</label>
                        <input type="date" class="form-control" name="fecha" id="fecha"><br>
                        <label for="hora">Hora</label>
                        <input type="time" class="form-control" name="hora" id="hora"><br>
                        <button type="submit" class="btn btn-success" id="agendar">Agendar Cita</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    {{--modal modificar--}}
    <div class="modal fade" id="modificarSolicitudModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Datos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="POST" id="form-update">
                        @csrf
                        {{method_field('patch')}}
                        <input type="hidden" name="id" id="id">
                        <div id="errores">

                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="nombre" id="nombre">
                            <div id="errorNombre"></div>
                        </div>

                        <div class="form-group">
                            <label for="apellido_paterno">Apellido Paterno</label>
                            <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno">
                            <div id="errorApaterno"></div>
                        </div>

                        <div class="form-group">
                            <label for="apellido_materno">Apellido Materno</label>
                            <input type="text" class="form-control" name="apellido_materno" id="apellido_materno">
                            <div id="errorAmaterno"></div>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email">
                            <div id="errorEmail"></div>
                        </div>

                        <div class="form-group">
                            <label for="telefono">Telefono</label>
                            <input type="text" class="form-control" name="telefono" id="telefono" maxlength="10">
                            <div id="errorTelefono"></div>
                        </div>
                        <button type="button" id="guardar" class="btn btn-success">Guardar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('/DataTables/datatables.min.js')}}"></script>
    {{--moda agendar cita script--}}
    <script type="text/javascript">
        $('#agendarCitaModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var fecha = button.data('fecha')
            var hora = button.data('hora')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #fecha').val(fecha);
            modal.find('.modal-body #hora').val(hora);
        })
    </script>
    {{--modal modificar script--}}
    <script type="text/javascript">
        $('#modificarSolicitudModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var nombre = button.data('nombre')
            var apellido_paterno = button.data('apellido_paterno')
            var apellido_materno = button.data('apellido_materno')
            var email = button.data('email')
            var telefono = button.data('telefono')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #nombre').val(nombre);
            modal.find('.modal-body #apellido_paterno').val(apellido_paterno);
            modal.find('.modal-body #apellido_materno').val(apellido_materno);
            modal.find('.modal-body #email').val(email);
            modal.find('.modal-body #telefono').val(telefono);
        })
    </script>
    {{--modal delete script--}}
    <script type="text/javascript">
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
        })
    </script>
    {{--datatables script--}}
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#solicitudes_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            jQuery('#guardar').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ route('solicitud.update','test') }}",
                    method: 'post',
                    data: jQuery('#form-update').serialize(),
                    beforeSend: function () {
                        $('#guardar').prop("disabled", true);
                    },
                    success: function (result) {
                        if (result.errors) {
                            $('#guardar').prop("disabled", false);
                            jQuery('.alert-danger').html('');
                            jQuery.each(result.errors, function (key, value) {
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>' + value + '</li>');
                            });
                        }
                        else {
                            jQuery('.alert-danger').hide();
                            $('#modificarSolicitudModal').modal('hide');
                            location.reload();
                        }
                    }
                });
            });
        });
    </script>
@endsection
