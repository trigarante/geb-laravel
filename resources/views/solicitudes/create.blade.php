@extends('layouts.appLayout')

@section('content')
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('solicitud.index')}}}">Solicitudes</a></li>
                <li class="breadcrumb-item active">Crear Solicitud</li>
            </ul>
        </div>
    </div>
    {{--errores--}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Corrige los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{--formulario solicitud--}}
    <section class="forms">
        <div class="container">
            <div class="row justify-content-center mt-3">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h4>Llena la siguiente solicitud</h4>
                        </div>
                        <div class="card-body">
                            <p>Todos los campos son obligatorios.</p>
                            <form action="{{route('solicitud.store')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" name="nombre"  style="text-transform:uppercase" value="{{ old('nombre') }}"
                                           @if($errors->has('nombre')) class="form-control is-invalid"
                                           @else class="form-control" @endif>
                                </div>
                                <div class="form-group">
                                    <label for="apellido_paterno">Apellido Paterno</label>
                                    <input type="text" name="apellido_paterno"  style="text-transform:uppercase" value="{{ old('apellido_paterno') }}"
                                           @if($errors->has('apellido_paterno')) class="form-control is-invalid"
                                           @else class="form-control" @endif>
                                </div>
                                <div class="form-group">
                                    <label for="apellido_materno">Apellido Materno</label>
                                    <input type="text" name="apellido_materno"   style="text-transform:uppercase" value="{{ old('apellido_materno') }}"
                                           @if($errors->has('apellido_materno')) class="form-control is-invalid"
                                           @else class="form-control" @endif>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" value="{{ old('email') }}"
                                           @if($errors->has('email')) class="form-control is-invalid"
                                           @else class="form-control" @endif>
                                </div>
                                <div class="form-group">
                                    <label for="telefono">Telefono</label>
                                    <input type="text" name="telefono" value="{{ old('telefono') }}"
                                           @if($errors->has('telefono')) class="form-control is-invalid"
                                           @else class="form-control" @endif maxlength="10">
                                </div>
                                <div class="form-group">
                                    <label for="bolsaTrabajo">Bolsa de Trabajo</label>
                                    <select name="bolsaTrabajo" id=""
                                            @if($errors->has('bolsaTrabajo')) class="custom-select is-invalid"
                                            @else class="custom-select" @endif>
                                        <option value="" selected>Bolsa de Trabajo</option>
                                        @foreach($bolsaTrabajo as $bolsa)
                                            @if (Request::old('bolsaTrabajo') == $bolsa->id)
                                                <option value="{{$bolsa->id}}" selected>{{$bolsa->nombre}}</option>
                                            @else
                                                <option value="{{$bolsa->id}}">{{$bolsa->nombre}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="bolsaTrabajo">Vacante</label>
                                    <select name="vacante" id=""
                                            @if($errors->has('vacante')) class="custom-select is-invalid"
                                            @else class="custom-select" @endif>
                                        <option value="" selected>Vacantes</option>
                                        @foreach($vacantes as $vacante)
                                            @if (Request::old('vacante') == $vacante->id)
                                                <option value="{{$vacante->id}}" selected>{{$vacante->nombre}}</option>
                                            @else
                                                <option value="{{$vacante->id}}">{{$vacante->nombre}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Registrar" class="btn btn-primary btn-md">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
