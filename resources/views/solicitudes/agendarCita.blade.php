<div class="modal fade" id="agendarCitaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agendar Cita</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="agendarCitaForm" method="POST" action="/agendarCita">
                    @csrf
                    <input type="hidden" class="form-control" name="id" id="id"><br>
                    <input type="date" class="form-control" name="fecha" id="fecha"><br>
                    <input type="time" class="form-control" name="hora" id="hora"><br>
                    <button type="submit" class="btn btn-success" id="agendar">Agendar Cita</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

