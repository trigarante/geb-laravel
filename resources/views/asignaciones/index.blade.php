@extends('layouts.appLayout')
@section('styles')
    <link href="{{asset('datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
    {{--mensaje exito--}}
    @if(session()->has('mensaje'))
        <div class="alert alert-success">
            {{ session()->get('mensaje') }}
        </div>
    @endif
    {{--etiqueta ubicacion--}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Asignaciones</li>
            </ul>
        </div>
    </div>
    {{--tabla--}}
    <section class="mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Telefono
                                    </th>
                                    <th>
                                        Puesto
                                    </th>
                                    <th>
                                        Tipo de Empleado
                                    </th>
                                    <th>
                                        Asignar
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($empleados as $empleado)
                                        <tr>
                                            <td>{{$empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno}}</td>
                                            <td>{{$empleado->email}}</td>
                                            <td>{{$empleado->telefono_movil}}</td>
                                            <td>{{$empleado->puesto_detalle}}</td>
                                            <td>{{$empleado->tipo_usuario}}</td>
                                            <td>
                                                @if($empleado->id_tipo == 1)
                                                    <button class="btn btn-success" data-toggle="modal"
                                                            data-id_empleado= "{{$empleado->id}}"
                                                            data-id_tipo = "{{$empleado->id_tipo}}"
                                                            data-target="#crearAdministrativoModal">
                                                        Asignar Administrativo
                                                    </button>
                                                @elseif($empleado->id_tipo == 2)
                                                    <button class="btn btn-success" data-toggle="modal"
                                                            data-id_empleado= "{{$empleado->id}}"
                                                            data-id_tipo = "{{$empleado->id_tipo}}"
                                                            data-target="#crearEjecutivoModal">
                                                        Asignar Ejecutivo
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="crearAdministrativoModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Administrativo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    {!! Form::open(['route' => 'asignacion.administrativo.save','method' => 'post']) !!}
                    {!! Form::token() !!}
                    <div class="form-group">
                        {!! Form::label('area') !!}
                        {!!Form::select('id_area',$areas,null, array('class'=>'custom-select','id'=>'medioTraslado'))!!}
                    </div>
                    @if(!$correos->isEmpty())
                    <div class="form-group">
                        {!! Form::label('correo') !!}
                        {!!Form::select('id_correo',$correos,null, array('class'=>'custom-select','id'=>'medioTraslado'))!!}
                    </div>
                        @else
                        <div class="form-group">
                            {!! Form::label('correo','!No hay correos disponibles¡') !!} <br>
                            <button class="btn btn-info">Crear Correo</button>
                        </div>
                    @endif
                    <div class="form-group">
                        {!! Form::label('grupo') !!}
                        {!!Form::select('id_grupo',$grupos,null, array('class'=>'custom-select','id'=>'medioTraslado'))!!}
                    </div>
                    {!! Form::hidden('id_empleado',null,array('id' => 'id_empleado')) !!}
                    {!! Form::hidden('id_tipo',null,array('id' => 'id_tipo')) !!}
                    {!! Form::submit('Asignar',array('class' => 'btn btn-success')) !!}
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="crearEjecutivoModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Administrativo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    {!! Form::open(['route' => 'asignacion.ejecutivo.save','method' => 'post']) !!}
                    {!! Form::token() !!}
                    @if(!$correos->isEmpty())
                        <div class="form-group">
                            {!! Form::label('correo') !!}
                            {!!Form::select('id_correo',$correos,null, array('class'=>'custom-select','id'=>'medioTraslado'))!!}
                        </div>
                    @else
                        <div class="form-group">
                            {!! Form::label('corre','!No hay correos disponibles¡') !!} <br>
                            <button class="btn btn-info">Crear Correo</button>
                        </div>
                    @endif
                    <div class="form-group">
                        {!! Form::label('campaña') !!}
                        {!!Form::select('id_campana',$campanas,null, array('class'=>'custom-select','id'=>'id_campana'))!!}
                    </div>
                        <div class="form-group" id="selectExtensiones">
                            {!! Form::label('extensiones_disponibles') !!}
                            <select class="custom-select" name="id_extension" id="id_extension"
                                    disabled>
                                <option value="0" selected hidden> Seleccione una extension</option>
                            </select>
                        </div>

                        <div class="form-group" id="btnExtension" hidden>
                            {!! Form::label('correo','!No hay extensiones disponibles¡') !!} <br>
                            <button class="btn btn-info">Crear Extension</button>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('DataTables/datatables.min.js')}}"></script>
    <script type="text/javascript">
        $('#crearAdministrativoModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id_empleado = button.data('id_empleado');
            var id_tipo = button.data('id_tipo');
            var modal = $(this);

            modal.find('.modal-body #id_empleado').val(id_empleado);
            modal.find('.modal-body #id_tipo').val(id_tipo);
        })
    </script>
    <script type="text/javascript">
        $('#id_campana').on('change', function (e) {
            console.log(e);
            var $id_campana = e.target.value;
            $.get('/ajax-extension?id_campana=' + $id_campana, function (data) {
                console.log(data.length);

                if (data.length == 0){
                    $('#selectExtensiones').prop("hidden", true);
                    $('#id_extension').empty();
                    $('#btnExtension').prop("hidden", false);

                }
                else {
                    $('#btnExtension').prop("hidden", true);
                    $('#selectExtensiones').prop("hidden", false);
                    $('#id_extension').prop("disabled", false);
                    $('#id_extension').empty();
                    $('#id_extension').append('<option value="0" disable="true" selected="true">Seleccione una Extension</option>');
                    $.each(data, function (index, estacionesObj) {
                        $('#id_extension').append('<option value="' + estacionesObj.id + '">' + estacionesObj.id + '</option>');
                    })
                }

            });
        });

    </script>
@endsection
