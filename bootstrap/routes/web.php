<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// login
Route::get('/','LoginController@index')->name('/');
Route::post('login.credentials','LoginController@credentials')->name('login.credentials');
// solicitud
Route::resource('solicitud', 'SolicitudController');
Route::post('/agendarCita','SolicitudController@agendarCita')->name('solicitud.agendarCita');
// preCandidatos
Route::get('/preCandidatos/create/{id}', 'PreCandidatoController@create')->name('preCandidatos.create');
Route::post('/preCandidatos/store', 'PreCandidatoController@store')->name('preCandidatos.store');
Route::get('/preCandidatos', 'PreCandidatoController@index')->name('preCandidatos.index');
Route::get('/preCandidatos/edit/{id}', 'PreCandidatoController@edit')->name('preCandidatos.edit');
Route::post('/preCandidatos/update', 'PreCandidatoController@update')->name('preCandidatos.update');
Route::post('/preCandidatos/delete', 'PreCandidatoController@destroy')->name('preCandidatos.destroy');
Route::get('/preCandidatos/show/{id}', 'PreCandidatoController@show')->name('preCandidatos.show');
Route::post('/preCandidatos/foto', 'PreCandidatoController@foto')->name('preCandidatos.foto');
// candidato
Route::get('/candidato', 'CandidatoController@index')->name('candidato.index');
Route::post('/candidato/store', 'CandidatoController@store')->name('candidato.store');
Route::get('/candidato/show/{id}', 'CandidatoController@show')->name('candidato.show');	
Route::post('/candidato/update', 'CandidatoController@update')->name('candidato.update');
// seguimiento
Route::post('/seguimiento/store', 'SeguimientoController@store')->name('seguimiento.store');
Route::post('/seguimiento/update', 'SeguimientoController@update')->name('seguimiento.update');
Route::get('/seguimiento', 'SeguimientoController@index')->name('seguimiento.index');
Route::get('/seguimiento/show/{id}', 'SeguimientoController@show')->name('seguimiento.show');
// empleado
Route::get('/empleado', 'EmpleadoController@index')->name('empleado.index');
Route::get('/empleado/create/{id}', 'EmpleadoController@create')->name('empleado.create');
Route::get('/empleado/edit/{id}', 'EmpleadoController@edit')->name('empleado.edit');
Route::post('/empleado/store', 'EmpleadoController@store')->name('empleado.store');
Route::post('/empleado/update', 'EmpleadoController@update')->name('empleado.update');
Route::get('/empleado/show/{id}', 'EmpleadoController@show')->name('empleado.show');
// catalogos index
Route::get('/catalogos/area', 'CatalogosController@areaIndex')->name('catalogos.area.index');
Route::get('/catalogos/bolsa', 'CatalogosController@bolsaIndex')->name('catalogos.bolsa.index');
Route::get('/catalogos/competencias', 'CatalogosController@competenciaIndex')->name('catalogos.competencias.index');
Route::get('/catalogos/empresa', 'CatalogosController@empresaIndex')->name('catalogos.empresa.index');
Route::get('/catalogos/escolaridad', 'CatalogosController@escolaridadIndex')->name('catalogos.escolaridad.index');
Route::get('/catalogos/etdoCivil', 'CatalogosController@estadoCivilIndex')->name('catalogos.etdoCivil.index');
Route::get('/catalogos/etdoEscolar', 'CatalogosController@etdoEscolaridadIndex')->name('catalogos.etdoEscolar.index');
Route::get('/catalogos/etdoMotivo', 'CatalogosController@etdoMotivoIndex')->name('catalogos.etdoMotivo.index');
Route::get('/catalogos/puesto', 'CatalogosController@puestoIndex')->name('catalogos.puesto.index');
Route::get('/catalogos/tipoPuesto', 'CatalogosController@tipoPuestoIndex')->name('catalogos.tipoPuesto.index');
// catalogos delete
Route::get('/catalogos/destroy', 'CatalogosController@destroy')->name('catalogos.destroy');
// catalogos save
Route::post('/catalogos/saveArea', 'CatalogosController@saveArea')->name('catalogos.saveArea');
Route::post('/catalogos/saveBolsa', 'CatalogosController@saveBolsa')->name('catalogos.saveBolsa');
Route::post('/catalogos/saveCompetencia', 'CatalogosController@saveCompetencia')->name('catalogos.saveCompetencia');
Route::post('/catalogos/saveEmpresa', 'CatalogosController@saveEmpresa')->name('catalogos.saveEmpresa');
Route::post('/catalogos/saveEscolaridad', 'CatalogosController@saveEscolaridad')->name('catalogos.saveEscolaridad');
Route::post('/catalogos/saveEtdoCivil', 'CatalogosController@saveEtdoCivil')->name('catalogos.saveEtdoCivil');
Route::post('/catalogos/saveEtdoEscolar', 'CatalogosController@saveEtdoEscolar')->name('catalogos.saveEtdoEscolar');
Route::post('/catalogos/saveEtdoMotivo', 'CatalogosController@saveEtdoMotivo')->name('catalogos.saveEtdoMotivo');
Route::post('/catalogos/savePuesto', 'CatalogosController@savePuesto')->name('catalogos.savePuesto');
Route::post('/catalogos/saveTipoPuesto', 'CatalogosController@saveTipoPuesto')->name('catalogos.saveTipoPuesto');

Route::get('/prueba','TableController@index')->name('prueba');
Route::get('/prueba/data','TableController@getdata')->name('prueba.getdata');