<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/styleSide.css')}}">
    <link rel="stylesheet" href="{{asset('mdb/css/mdb.min.css')}}">
    @yield('styles')
    

    <title>GEB</title>
  </head>
  <body>
    <div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar" class="unique-color">
        <div class="sidebar-header unique-color">
            <h3>GEB</h3>
        </div>

        <ul class="list-unstyled components">
          
            <li>
                <a href="{{route('solicitud.index')}}">Solicitudes</a>
            </li>
            <li>
                <a href="{{route('preCandidatos.index')}}">Pre Candidatos</a>
            </li>
            <li>
                <a href="{{route('candidato.index')}}">Candidatos</a>
            </li>
            <li>
                <a href="{{route('seguimiento.index')}}">Seguimiento</a>
            </li>
            <li>
                <a href="{{route('empleado.index')}}">Empleados</a>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Catalogos</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="{{route('catalogos.area.index')}}">Area de Trabajo</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.bolsa.index')}}">Bolsa de Trabajo</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.competencias.index')}}">Competencias</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.empresa.index')}}">Empresa</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.escolaridad.index')}}">Escolaridad</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.etdoCivil.index')}}">Estado Civil</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.etdoEscolar.index')}}">Estado Escolar</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.etdoMotivo.index')}}">Estado Motivo</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.puesto.index')}}">Puesto</a>
                    </li>
                    <li>
                        <a href="{{route('catalogos.tipoPuesto.index')}}">Tipo de Puesto</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>

    <div id="content">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn unique-color text-white">
                <i class="fas fa-align-left"></i>
                <span>Ocultar/Mostrar</span>
            </button>

        </div>
    </nav>
  @yield('content')  
</div>

</div>
  <!-- SideNav slide-out button -->

<!--/. Sidebar navigation -->
  
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('mdb/js/mdb.min.js')}}">
    </script>
    <script>
      $(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

});
    </script>
  @yield('scripts')
  </body>
</html>