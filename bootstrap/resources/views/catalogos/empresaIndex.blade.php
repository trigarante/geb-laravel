@extends('layouts.appLayout')
@section('content')
	<div class="container">
    <button class="btn btn-success" data-toggle="modal" data-target="#saveEmpresa">Agregar</button>
		<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nombre
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Descripcion
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Accion
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
    </tr>
  </thead>
  <tbody>
  	@foreach($empresas as $empresa)
  	@if($empresa->activo == 1)
    <tr>
      <td>{{$empresa->nombre}}</td>
      <td>{{$empresa->descripcion}}</td>
      <td><button class="btn btn-info">Modificar</button>
       <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$empresa->id}}"
            data-catalogo= "empresa">Eliminar</button></td>
    </tr>
    @endif
    @endforeach
  </tbody>
  
</table>
 {{$empresas->links()}}
	</div>

  <div class="modal fade" id="saveEmpresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Catalogo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="{{route('catalogos.saveEmpresa')}}" method="POST">
          @csrf
          <div class="md-form">
              <input type="text" id="nombre" class="form-control" name="nombre">
              <label for="nombre">Nombre</label>
          </div>
          <div class="md-form">
              <input type="text" id="descripcion" class="form-control" name="descripcion">
              <label for="descripcion">Descripcion</label>
          </div>
          <button type="submit" class="btn btn-success" id="guardar">Guardar</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>

<!--Modal: modalConfirmDelete-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">¿Seguro que desea eliminar?</p>
                </div>

                <!--Body-->
                <div class="modal-body">

                    <i class="fa fa-times fa-4x animated rotateIn"></i>
                  <form action="{{route('catalogos.destroy')}}">
                    @csrf
                        <input type="hidden" id="catalogo" name="catalogo">
                       <input type="hidden" id="id" name="id"> 
                       <button class="btn  btn-outline-danger" type="submit">Si</button>
                    </form>
                    <button type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</button>

                </div>

                <!--Footer-->
               
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalConfirmDelete-->
@endsection

@section('scripts')
 <script type="text/javascript">
      $('#deleteModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var catalogo = button.data('catalogo')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
         modal.find('.modal-body #catalogo').val(catalogo);
      })
    </script>
@endsection
@include('catalogos.delete')
@include('catalogos.saveEmpresa')