@extends('layouts.appLayout')
@section('content')
<div class="container">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">Candidatos</li>
</ol>
<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nombre
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Celular
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Correo
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Competencia
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Calificaion de Examen
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Estado
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Comentarios
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Estatus
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Seguimiento
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Accion
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
    </tr>
  </thead>
  <tbody>
  	@foreach($candidatos as $candidato)
    @if($candidato->id_estado_motivo == 1 || $candidato->id_estado_motivo == 2 || $candidato->id_estado_motivo == 100)
    <tr>
      <td>{{$candidato->nombre." ".$candidato->apellido_paterno." ".$candidato->apellido_materno}}</td>
      <td>{{$candidato->telefono_movil}}</td>
      <td>{{$candidato->email}}</td>
      <td>{{$candidato->competencia}}</td>
      <td>{{$candidato->calificacion_examen}}</td>
      <td>{{$candidato->estado}}</td>
      <td>{{$candidato->comentarios}}</td>
      <td>{{$candidato->comentarios}}</td>
      <td>
        @if($candidato->id_estado == 0)
        <button class="btn unique-color white-text btn-sm" id="crearSeguimiento" data-id='{{ $candidato->id }}' 
          data-toggle="modal" data-target="#crearSeguimientoModal">Crear Seguimiento</button>
        @else
        <a href="{{route('seguimiento.show',$candidato->id)}}" class="btn unique-color white-text btn-sm">Ver Seguimiento</a>
        @endif
      </td>
      <td><button  class="btn warning-color-dark btn-sm" data-id='{{ $candidato->id }}' data-comentarios = '{{$candidato->comentarios}}'
        data-calificacion_examen = '{{$candidato->calificacion_examen}}' data-competencia = '{{$candidato->competencia}}'
        data-id_calificacion_competencia = '{{$candidato->id_calificacion_competencia}}'
          data-toggle="modal" data-target="#updateCandidatoModal">Modificar</button>
        <button class="btn danger-color-dark btn-sm" data-toggle="modal" data-target="#deleteModal" data-id= "{{$candidato->id_estado_motivo}}">Eliminar</button></td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
{{$candidatos->links()}}
</div>

<div class="modal fade" id="crearSeguimientoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Seguimiento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="{{route('seguimiento.store')}}" method="POST">
          @csrf
          <input type="hidden" class="form-control" name="id" id="id"><br>
          <div class="md-form">
              <input type="text" id="calificacion_rollplay" class="form-control" name="calificacion_rollplay">
              <label for="calificacion_rollplay">Calificacion Rollplay</label>
          </div>
          <div class="md-form">
              <textarea type="text" id="comentarios" class="md-textarea form-control" rows="3" name="comentarios"></textarea>
              <label for="comentarios">Comentarios</label>
          </div>
          <button type="submit" class="btn btn-success" id="guardar">Crear Seguimiento</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="updateCandidatoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modificar Candidato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  method="POST" action="{{route('candidato.update')}}">
          @csrf
          <input type="hidden" class="form-control" name="id" id="id"><br>
          <select name="competencia" id="competencia" class="custom-select">
            @foreach($competencias as $competencia)
              <option value="{{$competencia->id}}">{{$competencia->estadio}}</option>
            @endforeach
          </select>
          <div class="md-form">
              <input type="text" id="calificacion_examen" class="form-control" name="calificacion_examen">
              
          </div>
          <div class="md-form">
              <textarea type="text" id="comentarios" class="md-textarea form-control" rows="3" name="comentarios"></textarea>
              
          </div>
          <button type="submit" class="btn btn-success" id="guardar">Crear Candidato</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">¿Seguro que desea eliminar?</p>
                </div>

                <!--Body-->
                <div class="modal-body">

                    <i class="fa fa-times fa-4x animated rotateIn"></i>
                  <form action="{{route('preCandidatos.destroy')}}" method="POST">
                    @csrf
                    <label for="">Motivo de baja</label>
                        <select name="motivo_baja" id="" class="custom-select">
                            @foreach($motivos as $motivo)
                                <option value="{{$motivo->id}}">{{$motivo->estado}}</option>
                            @endforeach
                        </select>
                       <input type="hidden" id="id" name="id"> 
                       <button class="btn  btn-outline-danger" type="submit">Si</button> <button type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</button>
                    </form>
                    

                </div>

                <!--Footer-->
               
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalConfirmDelete-->

@endsection
@section('scripts')
     <script type="text/javascript">
      $('#crearSeguimientoModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
      })
    </script>

    <script type="text/javascript">
      $('#updateCandidatoModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var calificacion_examen = button.data('calificacion_examen')
         var comentarios =button.data('comentarios')
         var competencia =button.data('competencia')
         var id_calificacion_competencia =button.data('id_calificacion_competencia')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
         modal.find('.modal-body #calificacion_examen').val(calificacion_examen);
         modal.find('.modal-body #comentarios').val(comentarios);
         modal.find('.modal-body #competencia').val(id_calificacion_competencia);
      })
    </script>

    <script type="text/javascript">
      $('#deleteModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
      })
    </script>
@endsection