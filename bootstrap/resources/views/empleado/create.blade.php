@extends('layouts.appLayout')
@section('content')
	<div class="container pl-5">
		<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/candidato">Candidatos</a></li>
    <li class="breadcrumb-item active">Crear Candidato</li>
</ol>
		<div class="row">
				@if (count($errors) > 0)
	    <div class="alert alert-danger">
	    	<p>Corrige los siguientes errores:</p>
	        <ul>
	            @foreach ($errors->all() as $message)
	                <li>{{ $message }}</li>
	            @endforeach
	        </ul>
	    </div>
	</div>
	@endif
			<form action="{{route('empleado.store')}}" method="POST">
				@csrf
				<div class="row">
					<div class="col-md-6">

						<input type="hidden" value="{{$id}}" name="idSeguimiento">
						<div class=form-control">
							<label for="fechaImss" >Fecha de Alta de Imss</label>
							<input type="date"  class="form-control" name="fechaImss" value="{{ old('fechaImss') }}">
						</div>

					<label for="puesto">Puesto</label>
					<select name="puesto" id="" class="custom-select">
						<option value="" selected>Puesto</option>
						@foreach($puestos as $puesto)
							<option value="{{$puesto->id}}">{{$puesto->nombre}}</option>
						@endforeach
					</select>

					<label for="tipoPuesto">Tipo de Puesto</label>
					<select name="tipoPuesto" id="" class="custom-select">
						<option value="" selected>Tipo de Puesto</option>
						@foreach($tipoPuestos as $tipoPuesto)
							<option value="{{$tipoPuesto->id}}">{{$tipoPuesto->nombre}}</option>
						@endforeach
					</select>

					<div class="md-form">
						<input type="text"  class="form-control" name="detallePuesto" value="{{ old('detallePuesto') }}">
						<label for="detallePuesto" >Detalle de Puesto</label>
					</div>

					<label for="area">Area</label>
					<select name="area" id="" class="custom-select">
						<option value="" selected>Area</option>
						@foreach($areas as $area)
							<option value="{{$area->id}}">{{$area->nombre}}</option>
						@endforeach
					</select>
					</div>

					<div class="col-md-6">

					<div class="md-form">
						<input type="text"  class="form-control" name="sueldoDiario" value="{{ old('sueldoDiario') }}">
						<label for="sueldoDiario" >Sueldo Diario</label>
					</div>

					<div class="md-form">
						<input type="text"  class="form-control" name="sueldoMensual" value="{{ old('sueldoMensual') }}">
						<label for="sueldoMensual" >Sueldo Mensual</label>
					</div>

					<div class="md-form">
						<input type="text"  class="form-control" name="cuentaClabe" value="{{ old('cuentaClabe') }}">
						<label for="cuentaClabe" >Cuenta Clabe</label>
					</div>
					<label for="banco">Banco</label>
					<select name="banco" id="" class="custom-select">
						<option value="" selected>Banco</option>
						@foreach($bancos as $banco)
							<option value="{{$banco->id}}">{{$banco->nombre}}</option>
						@endforeach
					</select>

					<label for="empresa">Empresa</label>
					<select name="empresa" id="" class="custom-select">
						<option value="" selected>Empresa</option>
						@foreach($empresas as $empresa)
							<option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
						@endforeach
					</select>
					
				</div>
			</div>
			<div class="row mx-auto pt-2">	
				<button type="sumbit"  class="btn btn-success">Registrar</button>
			</div>
			</form>
		</div>
@endsection