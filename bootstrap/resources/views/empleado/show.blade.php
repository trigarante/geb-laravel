@extends('layouts.appLayout')
@section('content')
  <div class="container" style="width: 1000px;">
    <div class="row">
      <div class="col-md-6">
        <label for="nombre">Nombre:</label>
        <label for="">{{$empleado->nombre}}</label> <br>
        <label for="nombre">Apellido Paterno:</label>
        <label for="">{{$empleado->apellido_paterno}}</label> <br>
        <label for="nombre">Apellido Materno:</label>
        <label for="">{{$empleado->apellido_materno}}</label> <br>
        <label for="nombre">Email:</label>
        <label for="">{{$empleado->email}}</label> <br>
        <label for="nombre">Telefono Movil:</label>
        <label for="">{{$empleado->telefono_movil}}</label> <br>
        <label for="nombre">Fecha de Alta de IMSS:</label>
        <label for="">{{$empleado->fecha_alta_imss}}</label> <br>
        <label for="nombre">Fecha de Ingreso:</label>
        <label for="">{{$empleado->fecha_ingreso}}</label> <br>
        <label for="nombre">Detalle de Puesto:</label>
        <label for="">{{$empleado->puesto_detalle}}</label> <br>
        <label for="nombre">Puesto:</label>
        <label for="">{{$empleado->puesto}}</label> <br>
        <label for="nombre">Tipo de Puesto:</label>
        <label for="">{{$empleado->puesto_tipo}}</label> <br>
        <label for="nombre">Area:</label>
        <label for="">{{$empleado->area}}</label> <br>
        <label for="nombre">Sueldo Diario:</label>
        <label for="">{{$empleado->sueldo_diario}}</label> <br>
        <label for="nombre">Sueldo Mensual:</label>
        <label for="">{{$empleado->sueldo_mensual}}</label> <br>
        <label for="nombre">Fecha de Cambio de Sueldo:</label>
        <label for="">{{$empleado->fecha_cambio_sueldo}}</label> <br>
        <label for="nombre">Cuenta Clabe:</label>
        <label for="">{{$empleado->cta_clabe}}</label> <br>
        <label for="nombre">Empresa:</label>
        <label for="">{{$empleado->empresa}}</label> <br>
      </div>
    </div>
  </div>
@endsection