@extends('layouts.appLayout')
@section('content')
	<div class="container">
    <ol class="breadcrumb">
    <li class="breadcrumb-item active">Empleados</li>
</ol>
		<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nombre
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Telefono
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Correo
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Puesto
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Tipo de Puesto
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Area
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Empresa
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Accion
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
    </tr>
  </thead>
  <tbody>
  	@foreach($empleados as $empleado)
    @if($empleado->id_estado_motivo == 1 || $empleado->id_estado_motivo == 2 || $empleado->id_estado_motivo == 100)
    <tr>
      <td>{{$empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno}}</td>
      <td>{{$empleado->telefono_movil}}</td>
      <td>{{$empleado->email}}</td>
      <td>{{$empleado->puesto}}</td>
      <td>{{$empleado->puesto_tipo}}</td>
      <td>{{$empleado->area}}</td>
      <td>{{$empleado->empresa}}</td>
      <td><a href="{{route('empleado.edit',$empleado->id)}}" class="btn warning-color-dark btn-sm text-white">Actualizar</a>
          <button class="btn-sm btn danger-color-dark">Eliminar</button>
      </td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
{{$empleados->links()}}
	</div>
@endsection