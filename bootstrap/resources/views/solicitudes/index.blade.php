@extends('layouts.appLayout')
@section('content')
	<div class="container">
    <ol class="breadcrumb">
    <li class="breadcrumb-item active">Solicitudes</li>
</ol>
    <a href="{{route('solicitud.create')}}" class="btn success-color-dark">Crear Solicitud</a>
		<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nombre
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Telefono
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Correo
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Fecha de Cita
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Cita
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Candidato
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Accion
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
    </tr>
  </thead>
  <tbody>
  	@foreach($solicitudes as $solicitud)
    <tr>
      <td>{{$solicitud->nombre." ".$solicitud->apellido_paterno." ".$solicitud->apellido_materno}}</td>
      <td>{{$solicitud->telefono}}</td>
      <td>{{$solicitud->correo}}</td>
      <td>
      	@if($solicitud->fecha_cita != '')
      		{{$solicitud->fecha_cita}}
		@else
			No hay cita agendada
		@endif
      </td>
      <td>
      	@if($solicitud->fecha_cita != '')
      		<button class="btn unique-color white-text btn-sm" data-id='{{$solicitud->id}}' data-toggle="modal" 
            data-target="#agendarCitaModal">Cambiar Cita</button>
  		@else
  			<button class="btn unique-color white-text btn-sm" data-id='{{ $solicitud->id }}' 
          data-toggle="modal" data-target="#agendarCitaModal">Agendar Cita</button>
		@endif
      </td>
      <td>
      	@if($solicitud->estado == 0 && $solicitud->fecha_cita !=0)
      		<a href="{{route('preCandidatos.create',$solicitud->id)}}" class="btn primary-color-dark text-white btn-sm">Crear Precandidato</a>
  		@elseif($solicitud->estado == 1 && $solicitud->fecha_cita !=0)
  			<a href="{{route('preCandidatos.show',$solicitud->id)}}" class="btn primary-color-dark text-white btn-sm">Ver Precandidato</a>
		@endif
      </td>
      <td>
        <button class="btn warning-color-dark btn-sm" data-id='{{ $solicitud->id }}' data-nombre='{{ $solicitud->nombre }}' 
          data-apellido_paterno='{{ $solicitud->apellido_paterno }}' data-apellido_materno='{{ $solicitud->apellido_materno }}' 
          data-email='{{ $solicitud->correo }}'
          data-telefono='{{ $solicitud->telefono }}'
          data-toggle="modal" data-target="#modificarSolicitudModal">
          Modificar
        </button> 
      </td>
    </tr>
    @endforeach
  </tbody>      
</table>
{{$solicitudes->links()}}
	</div>

  <div class="modal fade" id="agendarCitaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agendar Cita</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  id="agendarCitaForm" method="POST" action="{{route('solicitud.agendarCita')}}">
          @csrf
          <input type="hidden" class="form-control" name="id" id="id"><br>
          <input type="date" class="form-control" name="fecha" id="fecha"><br>
        <input type="time" class="form-control" name="hora" id="hora"><br>
        <button type="submit" class="btn btn-success" id="agendar">Agendar Cita</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modificarSolicitudModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" 
aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar Datos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="{{route('solicitud.update','test')}}" method="POST">
        @csrf
        {{method_field('patch')}}
        <input type="hidden" name="id" id="id">
        <div class="md-form">
          <input type="text"  class="form-control" name="nombre" id="nombre">
          
        </div>
        <div class="md-form">
          <input type="text"  class="form-control" name="apellido_paterno"  
          id="apellido_paterno">
    
        </div>
        <div class="md-form">
          <input type="text"  class="form-control" name="apellido_materno" 
          id="apellido_materno">
          
        </div>
        <div class="md-form">
          <input type="email"  class="form-control" name="email"  id="email">
          
        </div>
        <div class="md-form">
          <input type="text"  class="form-control" name="telefono" id="telefono">
          
        </div>
        <div class="md-form">
        </div>
        <input type="submit" class="btn btn-success" value="Registrar">
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


@endsection
@section('scripts')
     <script type="text/javascript">
      $('#agendarCitaModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
      })
    </script>
    <script type="text/javascript">
      $('#modificarSolicitudModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var nombre = button.data('nombre')
         var apellido_paterno = button.data('apellido_paterno')
         var apellido_materno = button.data('apellido_materno')
         var email = button.data('email')
         var telefono = button.data('telefono')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
         modal.find('.modal-body #nombre').val(nombre);
         modal.find('.modal-body #apellido_paterno').val(apellido_paterno);
         modal.find('.modal-body #apellido_materno').val(apellido_materno);
         modal.find('.modal-body #email').val(email);
         modal.find('.modal-body #telefono').val(telefono);
      })
    </script>
         <script type="text/javascript">
      $('#deleteModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
      })
    </script>
@endsection