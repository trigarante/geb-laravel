@extends('layouts.appLayout')
@section('content')
<div class="container">
	<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('solicitud.index')}}">Solicitudes</a></li>
    <li class="breadcrumb-item active">Crear Solicitud</li>
</ol>
	<div class="row pt-5 mx-auto">
		<h1 class="text-center">Llena la Siguiente Solicitud</h1>
	</div>
	<div class="row">
		<div class="col-md-8 mx-auto">
			@if (count($errors) > 0)
    <div class="alert alert-danger">
    	<p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif

			<form action="{{route('solicitud.store')}}" method="post">
				@csrf
				<div class="md-form">
					<input type="text"  class="form-control" name="nombre" value="{{ old('nombre') }}">
					<label for="nombre" >Nombre</label>
				</div>
				<div class="md-form">
					<input type="text"  class="form-control" name="apellido_paterno" value="{{ old('apellido_paterno') }}">
					<label for="apellido_paterno" >Apellido Paterno</label>
				</div>
				<div class="md-form">
					<input type="text"  class="form-control" name="apellido_materno" value="{{ old('apellido_materno') }}">
					<label for="apellido_materno" >Apellido Materno</label>
				</div>
				<div class="md-form">
					<input type="email"  class="form-control" name="email" value="{{ old('email') }}">
					<label for="email" >Email</label>
				</div>
				<div class="md-form">
					<input type="text"  class="form-control" name="telefono" value="{{ old('telefono') }}">
					<label for="telefono" >Telefono</label>
				</div>
				<div class="md-form">

					<select name="bolsaTrabajo" id="" class="form-control">
						<option value="" selected>Bolsa de Trabajo</option>
						@foreach($bolsaTrabajo as $bolsa)
							<option value="{{$bolsa->id}}">{{$bolsa->nombre}}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn success-color-dark">Registrar</button>
			</form>
		</div>
	</div>
</div>
@endsection