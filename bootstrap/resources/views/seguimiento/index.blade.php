@extends('layouts.appLayout')
@section('content')
		<div class="container">
      <ol class="breadcrumb">
    <li class="breadcrumb-item active">Seguimiento</li>
</ol>
		<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nombre
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Telefono
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Correo
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Calificacion Rollplay
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Comentarios
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Candidato
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Accion
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>

    </tr>
  </thead>
  <tbody>
  	@foreach($seguimientos as $seguimiento)
    @if($seguimiento->id_estado_motivo == 1 || $seguimiento->id_estado_motivo == 2 || $seguimiento->id_estado_motivo == 100)
    <tr>
      <td>{{$seguimiento->nombre." ".$seguimiento->apellido_paterno." ".$seguimiento->apellido_materno}}</td>
      <td>{{$seguimiento->telefono_movil}}</td>
      <td>{{$seguimiento->email}}</td>
      <td>{{$seguimiento->calificacion_rollplay}}</td>
      <td>{{$seguimiento->comentarios}}</td>
      <td>
        @if($seguimiento->id_estado == 1)
        <a href="{{route('empleado.show',$seguimiento->id)}}" class="btn unique-color white-text btn-sm">Ver empleado</a>
        @else
        <a class="btn unique-color white-text btn-sm" href="{{route('empleado.create',$seguimiento->id)}}">Crear empleado</a>
        @endif
      </td>
      <td>
      <button class= "btn warning-color-dark btn-sm" data-id='{{ $seguimiento->id }}' 
          data-toggle="modal" data-target="#updateSeguimientoModal" data-calificacion = '{{$seguimiento->calificacion_rollplay}}'
          data-comentarios = '{{$seguimiento->comentarios}}'>Modificar</button>
      <button class="btn danger-color-dark btn-sm"  data-toggle="modal" data-target="#deleteModal" 
      data-id='{{ $seguimiento->id }}'>Eliminar</button>
    </td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
{{$seguimientos->links()}}
	</div>

  <div class="modal fade" id="updateSeguimientoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar Seguimiento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="{{route('seguimiento.update')}}" method="POST">
          @csrf
          <input type="hidden" class="form-control" name="id" id="id"><br>
          <div class="md-form">
              <input type="text" id="calificacion" class="form-control" name="calificacion">
          </div>
          <div class="md-form">
              <textarea type="text" id="comentarios" class="md-textarea form-control" rows="3" name="comentarios"></textarea>
          </div>
          <button type="submit" class="btn btn-success" id="guardar">Actualizar Seguimiento</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
    <script type="text/javascript">
      $('#updateSeguimientoModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var calificacion = button.data('calificacion')
         var comentarios =button.data('comentarios')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
         modal.find('.modal-body #calificacion').val(calificacion);
         modal.find('.modal-body #comentarios').val(comentarios);
      })
    </script>
  @endsection
  