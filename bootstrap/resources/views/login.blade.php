@extends('layouts.loginLayout')
@section('content')
<div class="container">
	<div class="row justify-content-center pt-5">
		<div class="col-md-6">
			<div class="card">
	<form class="text-center border border-light p-5"  method="POST" action="{{route('login.credentials')}}">
        @csrf
    <p class="h4 mb-4">Iniciar Sesion</p>
    <input type="text" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="E-mail" name="email">
    <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Contraseña" name="password">

    <div class="d-flex justify-content-around">
        <div>
            <!-- Remember me -->
        </div>
       
    </div>

    <!-- Sign in button -->
    <button class="btn btn-info btn-block my-4" type="submit">Iniciar Sesion</button>
</form>
</div>
		</div>
			
	</div>

</div>


<!-- Default form login -->
@endsection