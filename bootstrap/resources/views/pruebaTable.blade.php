@extends('layouts.appLayout')
@section('styles')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>       
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
@endsection
@section('content')
<div class="container">
	<table id="tablaPrueba" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nombre
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
    </tr>
  </thead>
</table>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function () {
			$('#tablaPrueba').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": "{{route('prueba.getdata')}}",
				"columns": [
					{"data": "nombre"}
				]
			});
		});
	</script>
@endsection