<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">¿Seguro que desea eliminar?</p>
                </div>

                <!--Body-->
                <div class="modal-body">

                    <i class="fa fa-times fa-4x animated rotateIn"></i>
                  <form action="{{route('preCandidatos.destroy')}}" method="POST">
                    @csrf
                    <label for="">Motivo de baja</label>
                        <select name="motivo_baja" id="" class="custom-select">
                            @foreach($motivos as $motivo)
                                <option value="{{$motivo->id}}">{{$motivo->estado}}</option>
                            @endforeach
                        </select>
                       <input type="hidden" id="id" name="id"> 
                       <button class="btn  btn-outline-danger" type="submit">Si</button> <button type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</button>
                    </form>
                    

                </div>

                <!--Footer-->
               
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalConfirmDelete-->