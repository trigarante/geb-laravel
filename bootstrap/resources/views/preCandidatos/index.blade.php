@extends('layouts.appLayout')
@section('content')
		<div class="container">
      <ol class="breadcrumb">
    <li class="breadcrumb-item active">Precandidatos</li>
</ol>
		<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nombre
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Celular
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Telefono Fijo
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Correo
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Estado
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Candidato
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Accion
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
    </tr>
  </thead>
  <tbody>
  	@foreach($precandidatos as $precandidato)
    <tr>
      <td>{{$precandidato->nombre." ".$precandidato->apellido_paterno." ".$precandidato->apellido_materno}}</td>
      <td>{{$precandidato->telefono_movil}}</td>
      <td>{{$precandidato->telefono_fijo}}</td>
      <td>{{$precandidato->email}}</td>
      <td>{{$precandidato->estado}}</td>
      <td>
        @if($precandidato->id_estado == 0)
        <button class="btn primary-color-dark btn-sm" id="crearCandidato" data-id='{{ $precandidato->id }}' 
          data-toggle="modal" data-target="#crearCandidatoModal">Crear Candidato</button>
        @else
        <a href="{{route('candidato.show',$precandidato->id)}}"  class="btn primary-color-dark text-white btn-sm">Ver Candidato</a>
        @endif
      </td>
      <td>
        <a class="btn warning-color-dark text-white btn-sm" href="{{route('preCandidatos.edit',$precandidato->id)}}">Modificar</a>
        <button class="btn danger-color-dark btn-sm"  data-toggle="modal" data-target="#deleteModal" 
      data-id='{{ $precandidato->id }}'>Eliminar</button>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$precandidatos->links()}}
	</div>

  <div class="modal fade" id="crearCandidatoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Candidato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  method="POST" action="{{route('candidato.store')}}">
          @csrf
          <input type="hidden" class="form-control" name="id" id="id"><br>
          <select name="competencia" id="competencia" class="custom-select">
            <option value="" selected>Calificacion Competencias</option>
            @foreach($competencias as $competencia)
              <option value="{{$competencia->id}}">{{$competencia->estadio}}</option>
            @endforeach
          </select>
          <div class="md-form">
              <input type="text" id="calificacionExamen" class="form-control" name="calificacion_examen">
              <label for="calificacion_examen">Calificacion de Examen</label>
          </div>
          <div class="md-form">
              <textarea type="text" id="comentarios" class="md-textarea form-control" rows="3" name="comentarios"></textarea>
              <label for="form7">Comentarios</label>
          </div>
          <button type="submit" class="btn btn-success" id="guardar">Crear Candidato</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">¿Seguro que desea eliminar?</p>
                </div>

                <!--Body-->
                <div class="modal-body">

                    <i class="fa fa-times fa-4x animated rotateIn"></i>
                  <form action="{{route('preCandidatos.destroy')}}" method="POST">
                    @csrf
                    <label for="">Motivo de baja</label>
                        <select name="motivo_baja" id="" class="custom-select">
                            @foreach($motivos as $motivo)
                                <option value="{{$motivo->id}}">{{$motivo->estado}}</option>
                            @endforeach
                        </select>
                       <input type="hidden" id="id" name="id"> 
                       <button class="btn  btn-outline-danger" type="submit">Si</button> <button type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</button>
                    </form>
                    

                </div>

                <!--Footer-->
               
            </div>
            <!--/.Content-->
        </div>
    </div>

    <!--Modal: modalConfirmDelete-->
@endsection
@section('scripts')
     <script type="text/javascript">
      $('#crearCandidatoModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
      })
    </script>

    <script type="text/javascript">
      $('#deleteModal').on('show.bs.modal',function (event) {
         var button = $(event.relatedTarget)
         var id = button.data('id')
         var modal = $(this)

         modal.find('.modal-body #id').val(id);
      })
    </script>
@endsection