@extends('layouts.appLayout')
@section('content')
<div class="container pl-5">
	<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('preCandidatos.index')}}">Precandidatos</a></li>
    <li class="breadcrumb-item active">Crear Precandidato</li>
</ol>
	<div class="row">
			@if (count($errors) > 0)
    <div class="alert alert-danger">
    	<p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif
			<form action="{{route('preCandidatos.store')}}" method="POST">
				@csrf
				<div class="row">
					<div class="col-md-6">
						<input type="hidden" value="{{$id}}" name="idSolicitud">
						<div class="md-form">
							<input type="text"  class="form-control" name="nombre" value="{{$solicitudDatos->nombre}}">
							<label for="nombre" >Nombre</label>
						</div>
						<div class="md-form">
							<input type="text"  class="form-control" name="apellido_paterno" value="{{$solicitudDatos->apellido_paterno}}">
							<label for="apellido_paterno" >Apellido Paterno</label>
						</div>
						<div class="md-form">
							<input type="text"  class="form-control" name="apellido_materno" value="{{$solicitudDatos->apellido_materno}}">
							<label for="apellido_materno" >Apellido Materno</label>
						</div>
						<div class="md-form">
							<input type="email"  class="form-control" name="email" value="{{$solicitudDatos->correo}}">
							<label for="email" >Email</label>
						</div>
						<div class=form-control">
							<label for="fechaNacimiento" >Fecha de Nacimiento</label>
							<input type="date"  class="form-control" name="fechaNacimiento" value="{{ old('fechaNacimiento') }}">
						</div><br>
						<label for="fechaNacimiento" >Genero</label>
						<div class="custom-control custom-radio">
						  <input type="radio" class="custom-control-input" name="genero" value="M" id="defaultUnchecked">
						  <label class="custom-control-label" for="defaultUnchecked" >Masculino</label>
						</div>
						<div class="custom-control custom-radio">
						  <input type="radio" class="custom-control-input" name="genero" value="F" id="defaultChecked">
						  <label class="custom-control-label" for="defaultChecked" >Femenino</label>
						</div>
						<label for="estadoCivil">Estado Civil</label>
					<select name="estadoCivil" id="" class="custom-select">
						<option value="" selected>Estado Civil</option>
						@foreach($estadoCivil as $etoCivil)
							<option value="{{$etoCivil->id}}">{{$etoCivil->descripcion}}</option>
						@endforeach
					</select>
					<label for="escolaridad">Escolaridad</label>
					<select name="escolaridad" id="" class="custom-select">
						<option value="" selected>Escolaridad</option>
						@foreach($escolaridad as $escolar)
							<option value="{{$escolar->id}}">{{$escolar->nivel}}</option>
						@endforeach
					</select>
					<label for="estadoEscolar">Estado de Escolaridad</label>
					<select name="estadoEscolar" id="" class="custom-select">
						<option value="" selected>Estado de Escolaridad</option>
						@foreach($estadoEscolar as $etdoEscolar)
							<option value="{{$etdoEscolar->id}}">{{$etdoEscolar->estado}}</option>
						@endforeach
					</select>
					<label for="pais">Pais de Origen</label>
					<select name="pais" id="" class="custom-select">
						<option value="" selected>Pais de Origen</option>
						@foreach($paises as $pais)
							<option value="{{$pais->id}}">{{$pais->nombre}}</option>
						@endforeach
					</select>				
					</div>

					<div class="col-md-6">
					<div class="md-form">
						<input type="text"  class="form-control" name="cp" value="{{ old('cp') }}">
						<label for="cp" >Codigo Postal</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="colonia" value="{{ old('colonia') }}">
						<label for="colonia" >Colonia</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="calle" value="{{ old('calle') }}">
						<label for="calle" >Calle</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="numeroInt" value="{{ old('numeroInt') }}">
						<label for="numeroInt" >Numero Interior</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="numeroExt" value="{{ old('numeroExt') }}">
						<label for="numeroExt" >Numero Exterior</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="telFijo" value="{{ old('telFijo') }}">
						<label for="telFijo" >Telefono Fijo</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="telMovil" value="{{ old('telMovil') }}">
						<label for="telMovil" >Telefono Movil</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="tiempoTraslado" value="{{ old('tiempoTraslado') }}">
						<label for="tiempoTraslado" >Tiempo de Traslado en Minutos</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="medioTraslado" value="{{ old('medioTraslado') }}">
						<label for="medioTraslado" >Medio de Traslado</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="metroCercano" value="{{ old('metroCercano') }}">
						<label for="metroCercano" >Metro mas Cercano</label>
					</div>
					<img src="" alt="" id="imagen" class="img-fluid" style="width: 300px;">
					<input type="text" id="imagenText" name="imagen">
				</div>
			</div>
			<div class="row mx-auto pt-2">	
				<button type="sumbit"  class="btn btn-success">Registrar</button>
				
			</div>
			</form>
			<button  class="btn btn-info" data-toggle="modal" data-target="#fotoModal" >Tomar Foto</button>
</div>
	
    <div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content text-center">
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Foto</p>
                </div>
                <div class="modal-body">
                <video id="video" style="width: 100%;"></video>
                <br>
                <button id="boton">Tomar foto</button>
                <p id="estado"></p>
                <canvas id="canvas" style="display: none;"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script>
	function tieneSoporteUserMedia() {
    return !!(navigator.getUserMedia || (navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia)
}
function _getUserMedia() {
    return (navigator.getUserMedia || (navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia).apply(navigator, arguments);
}

// Declaramos elementos del DOM
var $video = document.getElementById("video"),
	$canvas = document.getElementById("canvas"),
	$boton = document.getElementById("boton"),
	$estado = document.getElementById("estado");
if (tieneSoporteUserMedia()) {
    _getUserMedia(
        {video: true},
        function (stream) {
            console.log("Permiso concedido");
			$video.src = window.URL.createObjectURL(stream);
			$video.play();

			//Escuchar el click
			$boton.addEventListener("click", function(){

				//Pausar reproducción
				$video.pause();

				//Obtener contexto del canvas y dibujar sobre él
				var contexto = $canvas.getContext("2d");
				$canvas.width = $video.videoWidth;
				$canvas.height = $video.videoHeight;
				contexto.drawImage($video, 0, 0, $canvas.width, $canvas.height);

				var foto = $canvas.toDataURL(); //Esta es la foto, en base 64
				$estado.innerHTML = "Enviando foto. Por favor, espera...";
				var xhr = new XMLHttpRequest();	
				 token = document.querySelector('meta[name="csrf-token"]').content;			
				xhr.open("POST", "{{route('preCandidatos.foto')}}", true);				
				xhr.setRequestHeader('X-CSRF-TOKEN', token);
				xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xhr.send(encodeURIComponent(foto)); //Codificar y enviar

				xhr.onreadystatechange = function() {
				    if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
				        console.log("La foto fue enviada correctamente");
				        console.log(xhr);
				        $estado.innerHTML = "Foto guardada con éxito";
				        document.getElementById("imagen").src = "../../"+xhr.responseText;
				        document.getElementById("imagenText").value = xhr.responseText;
				    }
				}

 
				//Reanudar reproducción
				$video.play();
			});
        }, function (error) {
            console.log("Permiso denegado o error: ", error);
            $estado.innerHTML = "No se puede acceder a la cámara, o no diste permiso.";
        });
} else {
    alert("Lo siento. Tu navegador no soporta esta característica");
    $estado.innerHTML = "Parece que tu navegador no soporta esta característica. Intenta actualizarlo.";
}
</script>
@endsection