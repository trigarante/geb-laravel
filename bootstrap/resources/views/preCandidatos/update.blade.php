@extends('layouts.appLayout')
@section('content')
<div class="container pl-5">
	<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('preCandidatos.index')}}">Precandidatos</a></li>
    <li class="breadcrumb-item active">Modificar Precandidato</li>
</ol>
	<div class="row">
			@if (count($errors) > 0)
    <div class="alert alert-danger">
    	<p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif
			<form action="{{route('preCandidatos.update')}}" method="POST">
				@csrf
				<div class="row">
					<div class="col-md-6">
						<input type="hidden" name="id" value="{{$precandidatos->id}}">
						<input type="hidden" name="id_solicitud" value="{{$precandidatos->id_solicitud_rrhh}}">
						<div class="md-form">
							<input type="text"  class="form-control" name="nombre" value="{{$precandidatos->nombre}}">
							<label for="nombre" >Nombre</label>
						</div>
						<div class="md-form">
							<input type="text"  class="form-control" name="apellido_paterno" 
							value="{{$precandidatos->apellido_paterno}}">
							<label for="apellido_paterno" >Apellido Paterno</label>
						</div>
						<div class="md-form">
							<input type="text"  class="form-control" name="apellido_materno" 
							value="{{$precandidatos->apellido_materno}}">
							<label for="apellido_materno" >Apellido Materno</label>
						</div>
						<div class="md-form">
							<input type="email"  class="form-control" name="email" value="{{$precandidatos->email}}">
							<label for="email" >Email</label>
						</div>
						<div class=form-control">
							<label for="fechaNacimiento" >Fecha de Nacimiento</label>
							<input type="date"  class="form-control" name="fechaNacimiento" 
							value="{{$precandidatos->fecha_nacimiento}}">
						</div><br>
						<label for="fechaNacimiento" >Genero</label>
						<div class="custom-control custom-radio">
						  <input type="radio" class="custom-control-input" name="genero" value="M" id="defaultUnchecked" @if($precandidatos->genero == 'M') checked @endif>
						  <label class="custom-control-label" for="defaultUnchecked" >Masculino</label>
						</div>
						<div class="custom-control custom-radio">
						  <input type="radio" class="custom-control-input" name="genero" value="F" id="defaultChecked"
						  @if($precandidatos->genero == 'F') checked @endif>
						  <label class="custom-control-label" for="defaultChecked" >Femenino</label>
						</div>
						<label for="estadoCivil">Estado Civil</label>
					<select name="estadoCivil" id="" class="custom-select">
						<option value="{{$precandidatos->id_estado_civil}}" selected>{{$precandidatos->ecivil}}</option>
						@foreach($estadoCivil as $etoCivil)
							<option value="{{$etoCivil->id}}">{{$etoCivil->descripcion}}</option>
						@endforeach
					</select>
					<label for="escolaridad">Escolaridad</label>
					<select name="escolaridad" id="" class="custom-select">
						<option value="{{$precandidatos->id_escolaridad}}" selected>{{$precandidatos->escolaridad}}</option>
						@foreach($escolaridad as $escolar)
							<option value="{{$escolar->id}}">{{$escolar->nivel}}</option>
						@endforeach
					</select>
					<label for="estadoEscolar">Estado de Escolaridad</label>
					<select name="estadoEscolar" id="" class="custom-select">
						<option value="{{$precandidatos->id_etdo_escolaridad}}" selected>
							{{$precandidatos->estado_escolar}}</option>
						@foreach($estadoEscolar as $etdoEscolar)
							<option value="{{$etdoEscolar->id}}">{{$etdoEscolar->estado}}</option>
						@endforeach
					</select>		
					</div>

					<div class="col-md-6">
					<div class="md-form">
						<input type="text"  class="form-control" name="cp" value="{{$precandidatos->cp}}">
						<label for="cp" >Codigo Postal</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="colonia" value="{{$precandidatos->colonia}}">
						<label for="colonia" >Colonia</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="calle" value="{{$precandidatos->calle}}">
						<label for="calle" >Calle</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="numeroInt" value="{{$precandidatos->numero_interior}}">
						<label for="numeroInt" >Numero Interior</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="numeroExt" value="{{$precandidatos->numero_exterior}}">
						<label for="numeroExt" >Numero Exterior</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="telFijo" value="{{$precandidatos->telefono_fijo}}">
						<label for="telFijo" >Telefono Fijo</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="telMovil" value="{{$precandidatos->telefono_movil}}">
						<label for="telMovil" >Telefono Movil</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="tiempoTraslado" value="{{$precandidatos->tiempo_traslado}}">
						<label for="tiempoTraslado" >Tiempo de Traslado</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="medioTraslado" value="{{$precandidatos->medio_traslado}}">
						<label for="medioTraslado" >Medio de Traslado</label>
					</div>
					<div class="md-form">
						<input type="text"  class="form-control" name="metroCercano" value="{{$precandidatos->metro_cercano}}">
						<label for="metroCercano" >Metro mas Cercano</label>
					</div>
				</div>
			</div>
			<div class="row mx-auto pt-2">	
				<button type="sumbit"  class="btn btn-success">Actualizar</button>
				<button  class="btn btn-info">Tomar Foto</button>
			</div>
			</form>
</div>
@endsection