@extends('layouts.appLayout')
@section('content')
<div class="container-fluid">
	<div class="row mx-auto" style="width: 1000px;">
		<div class="col-md-6"> 
			<label for="">Nombre:</label> 
			{{$precandidato->nombre}} <br>
			<label for="">Apellido Paterno:</label> 
			{{$precandidato->apellido_paterno}} <br>
			<label for="">Apellido Materno:</label> 
			{{$precandidato->apellido_materno}} <br>
			<label for="">Fecha de Nacimiento:</label> 
			{{$precandidato->fecha_nacimiento}} <br>
			<label for="">Email:</label> 
			{{$precandidato->email}} <br>
			<label for="">Genero:</label> 
			{{$precandidato->genero}} <br>
			<label for="">Estado Civil:</label> 
			{{$precandidato->ecivil}} <br>
			<label for="">Escolaridad:</label> 
			{{$precandidato->escolaridad}} <br>
			<label for="">Estado Escolar:</label> 
			{{$precandidato->estado_escolar}} <br>
			<label for="">Pais:</label> 
			{{$precandidato->pais}} <br>
			<label for="">Codigo Postal:</label> 
			{{$precandidato->cp}} <br>
			<label for="">Colonia:</label> 
			{{$precandidato->colonia}} <br>
			<label for="">Calle:</label> 
			{{$precandidato->calle}} <br>
			<label for="">Numero Exterior:</label> 
			{{$precandidato->numero_exterior}} <br>
			<label for="">Numero Interior:</label> 
			{{$precandidato->numero_interior}} <br>
		</div>

		<div class="col-md-6">
			<label for="">Telefono Fijo:</label> 
			{{$precandidato->telefono_fijo}} <br>
			<label for="">Telefono Movil:</label> 
			{{$precandidato->telefono_movil}} <br>
			<label for="">Estado:</label> 
			{{$precandidato->estado}} <br>
			<label for="">Estatus:</label> 
			{{$precandidato->motivo}} <br>
			<label for="">Fecha de Creacion:</label> 
			{{$precandidato->fecha_creacion}} <br>
			<label for="">Tiempo de Traslado:</label> 
			{{$precandidato->tiempo_traslado}} <br>
			<label for="">Medio de Traslado:</label> 
			{{$precandidato->medio_traslado}} <br>
			<label for="">Metro mas Cercano:</label> 
			{{$precandidato->metro_cercano}} <br>
			<label for="">Foto:</label> <br>
			<img src="{{asset($precandidato->imagen)}}" alt="" class="img-fluid" style="width: 300px;">
		</div>
	</div>	
</div>
@endsection