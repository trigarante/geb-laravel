<?php

namespace App\Inventario\InventarioLogs;

use Illuminate\Database\Eloquent\Model;

class MouseLog extends Model {

	protected $connection = 'logs';

    protected $table = 'mouse';
    public $timestamps = false;

    protected $fillable = ['id', 'num_folio', 'marca', 'id_estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega', 'id_sesion', 'fecha_cambio'];

}


