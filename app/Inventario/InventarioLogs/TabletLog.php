<?php

namespace App\Inventario\InventarioLogs;

use Illuminate\Database\Eloquent\Model;

class TabletLog extends Model {

	protected $connection = 'logs';

    protected $table = 'tablets';
    public $timestamps = false;

    protected $fillable = ['id', 'num_folio', 'procesador', 'capacidad', 'ram', 'marca', 'id_estado', 'telefono', 'fecha_recepcion', 'fecha_salida', 'id_entrega','id_sesion' , 'fecha_cambio'];

}
