<?php

namespace App\Inventario\InventarioLogs;

use Illuminate\Database\Eloquent\Model;

class CpuLog extends Model
{
	protected $connection = 'logs';
	
    protected $table= 'cpu';
    public $timestamps = false;

    protected $fillable = ['id', 'numero_serie', 'porcesador', 'hdd', 'ram', 'marca', 'modelo', 'id_estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega','id_sesion','fecha_cambio'];
}

