<?php
namespace App\Inventario\InventarioLogs;

use Illuminate\Database\Eloquent\Model;

class MonitorLog extends Model
{
	protected $connection = 'logs';
	
    protected $table = 'monitor';
    public $timestamps = false;
    
    protected $fillable = ['id', 'num_serie', 'marca', 'dimensiones', 'id_estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega', 'activo','id_sesion','fecha_cambio'];
}