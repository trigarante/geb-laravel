<?php

namespace App\Inventario\InventarioLogs;

use Illuminate\Database\Eloquent\Model;

class InventarioLog extends Model {
	
	protected $connection = 'logs';

    protected $table = 'inventario';
    public $timestamps = false;
    
    protected $fillable = ['id', 'id_ejecutivo', 'id_area', 'cpu', 'monitor', 'teclado', 'mouse', 'diadema', 'tablets', 'fecha_asignacion', 'fecha_liberacion', 'id_sesion', 'fecha_cambio'];

}
