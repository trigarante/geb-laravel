<?php

namespace App\Inventario\InventarioView;

use Illuminate\Database\Eloquent\Model;

class InventarioEmpleadoView extends Model
{
    protected $table= 'inventario_view';

    public $timestamps = false;

    protected $fillable = ['id_usuario', 'id_area', 'CPU num', 'CPU marca', 'Monitor num', 'Monitor marca', 'Teclado num', 'Teclado marca', 'Mouse num', 'Mouse marca', 'Diadema num', 'Diadema marca', 'Tablet num', 'Tablet marca', 'fecha_asignacion', 'fecha_liberacion', 'activo'];
}

