<?php

namespace App\Inventario\InventarioView;

use Illuminate\Database\Eloquent\Model;

class AdminView extends Model {

    protected $table = 'administrativos_view';
    public $timestamps = false;
    
    protected $fillable = ['id', 'id_empleado', 'estado', 'id_subarea_admon', 'fecha_creacion', 'fecha_baja', 'id_correo', 'id_usuario', 'usuario', 'id_grupo', 'id_tipo', 'grupo', 'tipo', 'correo', 'id_candidato', 'fecha_ingreso', 'fecha_alta_imss', 'puesto_detalle', 'id_puesto', 'id_puesto_tipo', 'id_subarea', 'id_estado', 'recontratable', 'id_empresa', 'fecha_asigancion', 'calificacion_examen', 'comentarios', 'id_precandidato', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_nacimiento', 'Edad', 'email', 'genero', 'id_estado_civil', 'ecivil', 'id_escolaridad','escolaridad','id_etdo_escolaridad','estado_escolar','id_pais','cp','estado_mx','del_mun','ciudad','colonia','calle','numero_exterior',' numero_interior','telefono_fijo','telefono_movil','medio','estacion','id_medio_traslado','id_estacion','tiempo_traslado','vacante','pais','id_imagen','imagen','empleado','puesto','tipo_puesto','empresa','estado_rh','banco','subarea','id_area','area','asignado', 'subarea_admon', 'equipo'];

}
