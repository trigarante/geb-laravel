<?php

namespace App\Inventario\InventarioView;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model {

    protected $table = 'empleado_view';
    public $timestamps = false;

    protected $fillable = ['id', 'id_candidato',' fecha_ingreso',' fecha_alta_imss', 'puesto_detalle',' id_puesto', 'id_puesto_tipo', 'id_subarea', 'sueldo_diario', 'sueldo_mensual', 'kpi', 'fecha_cambio_sueldo',' cta_clabe, id_banco', 'id_estado',' fecha_baja',' recontratable',' id_empresa', 'fecha_asigancion', 'calificacion_examen',' comentarios', 'id_precandidato', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_nacimiento', 'Edad', 'email', 'genero', 'id_estado_civil', 'ecivil', 'id_escolaridad', 'escolaridad', 'id_etdo_escolaridad', 'estado_escolar',' id_pais', 'cp', 'estado', 'del_mun', 'ciudad', 'colonia', 'calle', 'numero_exterior', 'numero_interior', 'telefono_fijo', 'telefono_movil',' medio', 'estacion', 'id_medio_traslado', 'id_estacion', 'tiempo_traslado', 'vacante', 'pais', 'id_usuario', 'id_imagen', 'imagen', 'id_calificacion_competencias', 'escala_competencia', 'estadio_competencia', 'descripcion_competencia', 'empleado', 'capacitacion',' puesto', 'tipo_puesto', 'empresa',' estado_rh', 'banco', 'subarea', 'id_area',' area', 'id_tipo', 'tipo_usuario'];
}

