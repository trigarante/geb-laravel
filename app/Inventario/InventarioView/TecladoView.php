<?php

namespace App\Inventario\InventarioView;

use Illuminate\Database\Eloquent\Model;


class TecladoView extends Model {

    protected $table = 'teclado_view';
    public $timestamps = false;
    
    protected $fillable = ['id', 'num_folio', 'marca', 'id_estado','estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega','entregado', 'activo'];

}
