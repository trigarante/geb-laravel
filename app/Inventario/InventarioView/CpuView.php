<?php

namespace App\Inventario\InventarioView;

use Illuminate\Database\Eloquent\Model;

class CpuView extends Model {

    protected $table = 'cpu_view';
    public $timestamps = false;
    protected $fillable = ['id', 'numero_serie', 'porcesador', 'hdd', 'ram', 'marca', 'modelo', 'id_estado','estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega','entregado', 'activo'];

}


