<?php

namespace App\Inventario\InventarioView;

use Illuminate\Database\Eloquent\Model;

class DiademaView extends Model {

    protected $table = 'diadema_view';
    public $timestamps = false;

    protected $fillable = ['id', 'num_folio', 'marca', 'id_estado','estado','fecha_recepcion', 'fecha_salida', 'id_entrega','entregado', 'activo'];
}
