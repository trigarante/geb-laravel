<?php

namespace App\Inventario\InventarioView;

use Illuminate\Database\Eloquent\Model;

class InventarioEjecutivo extends Model {

    protected $table = 'ejecutivo_view';
    public $timestamps = false;
    
    protected $fillable = ['id', 'id_empleado', 'nombre', 'apellido_paterno', 'apellido_materno', 'empresa',' area', 'subarea', 'id_candidato', 'fecha_ingreso', 'puesto_detalle',' id_puesto', 'id_puesto_tipo', 'id_subarea', 'id_estado', 'recontratable', 'id_empresa', 'calificacion_examen', 'comentarios', 'id_precandidato', 'fecha_nacimiento', 'Edad', 'email', 'genero', 'id_estado_civil', 'ecivil', 'id_escolaridad', 'escolaridad', 'id_etdo_escolaridad', 'estado_escolar', 'id_pais', 'cp', 'estado', 'del_mun', 'ciudad', 'colonia',' calle', 'numero_exterior', 'numero_interior', 'telefono_fijo', 'telefono_movil', 'medio', 'estacion', 'id_medio_traslado', 'id_estacion', 'tiempo_traslado', 'vacante', 'pais', 'id_imagen', 'imagen',' empleado', 'puesto','tipo_puesto', 'estado_rh', 'banco',' id_area',' id_tipo', 'tipo_usuario',' estado_ejecutivo', 'fecha_creacion', 'fecha_baja', 'id_correo', 'correo', 'id_extension', 'id_protocolo',' id_perfil_marcacion', 'id_campana', 'campana', 'id_usuario',' id_grupo', 'grupo', 'descripcion', 'usuario',' equipo'];
    
}


