<?php

namespace App\Inventario\InventarioView;
 
use Illuminate\Database\Eloquent\Model;

class TabletView extends Model {

    protected $table = 'tablet_view';
    
    public $timestamps = false;

    protected $fillable = ['id',' num_folio', 'procesador', 'capacidad',' ram',' marca', 'id_estado', 'estado', 'telefono', 'fecha_recepcion', 'fecha_salida', 'id_entrega', 'entregado', 'activo'];

}

