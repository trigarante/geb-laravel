<?php

namespace App\Inventario\InventarioModels;

use Illuminate\Database\Eloquent\Model;

class Diadema extends Model {

    protected $table = 'diadema';
    public $timestamps = false;
    protected $fillable = ['id', 'num_folio', 'marca', 'id_estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega', 'activo'];

}
