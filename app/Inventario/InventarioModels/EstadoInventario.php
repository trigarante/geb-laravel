<?php

namespace App\Inventario\InventarioModels;

use Illuminate\Database\Eloquent\Model;

class EstadoInventario extends Model
{
         protected $table= 'estado_inventario';

    public $timestamps = false;

    protected $fillable = ['id','estado'];
}