<?php

namespace App\Inventario\InventarioModels;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model { 

    protected $table = 'inventario';
    public $timestamps = false;
    protected $fillable = ['id', 'id_usuario', 'id_area','cpu', 'monitor', 'teclado','mouse', 'diadema','tablets', 'fecha_asignacion', 'fecha_liberacion', 'activo'];

}
