<?php

namespace App\Inventario\InventarioModels;

use Illuminate\Database\Eloquent\Model;


class Tablet extends Model {

    protected $table = 'tablets';
    public $timestamps = false;
    protected $fillable = ['id', 'num_folio', 'procesador', 'capacidad', 'ram', 'marca', 'id_estado', 'telefono', 'fecha_recepcion', 'fecha_salida', 'id_entrega', 'activo'];

}


