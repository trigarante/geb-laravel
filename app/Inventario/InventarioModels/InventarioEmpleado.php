<?php

namespace App\Inventario\InventarioModels;

use Illuminate\Database\Eloquent\Model;

class InventarioEmpleado extends Model {

    protected $table = 'ejecutivo';
    public $timestamps = false;

    protected $fillable = ['id', 'usuario', 'nombre', 'correo', 'password', 'numEmpleado', 'estado', 'id_area', 'id_supervisor', 'id_campana', 'fechaIngreso', 'fechaBaja', 'extension', 'telefono', 'token'];
    

}
