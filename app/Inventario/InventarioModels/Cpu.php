<?php

namespace App\Inventario\InventarioModels;

use Illuminate\Database\Eloquent\Model;

class Cpu extends Model {

    protected $table = 'cpu';
    public $timestamps = false;
    protected $fillable = ['id', 'numero_serie', 'porcesador', 'hdd', 'ram', 'marca', 'modelo', 'id_estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega', 'activo'];

}
