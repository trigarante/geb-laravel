<?php

namespace App\Inventario\InventarioModels;

use Illuminate\Database\Eloquent\Model;

class EstadoEntrega extends Model
{
    protected $table = 'entregado';

    public $timestamps = false;

    protected $fillable = ['id','estado'];
}