<?php

namespace App\Inventario\InventarioModels;

use Illuminate\Database\Eloquent\Model;

class Administrativos extends Model {

    protected $table = 'administrativo';
    public $timestamps = false;
    protected $fillable = ['id', 'id_empleado', 'estado', 'id_area', 'fecha_creacion', 'fecha_baja', 'id_correo', 'id_usuario'];

}




