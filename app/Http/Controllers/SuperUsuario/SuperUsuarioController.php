<?php

namespace App\Http\Controllers\SuperUsuario;

use App\GrupoUsuarios;
use App\Catalogos\CatalogosModels\Pais;
use App\Catalogos\CatalogosModels\Puesto;
use App\Catalogos\CatalogosModels\SubArea;
use App\Catalogos\CatalogosModels\TipoPuesto;
use App\TipoPermiso;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuperUsuarioController extends Controller
{
    public function indexGrupo()
    {
        $paises = Pais::get();
        $grupos = GrupoUsuarios::get();
        $puestos = Puesto::get();
        $tipoPuestos = TipoPuesto::get();
        $subAreas = SubArea::get();
        return view('superUsuario.grupoIndex', compact('grupos', 'paises', 'puestos', 'tipoPuestos', 'subAreas'));
    }

    public function permisos($id)
    {
        $tipoPermisos = TipoPermiso::get();
        $permisos = GrupoUsuarios::find($id)->first();
        return view('superUsuario.permisos', compact('permisos','tipoPermisos'));
    }

    public function saveGrupo(Request $request)
    {
        $save = GrupoUsuarios::create([
            'grupo' => $request->nombre,
            'descripcion' => $request->descripcion,
            'id_subarea' => $request->subArea,
            'id_puesto' => $request->puesto,
            'id_tipo_puesto' => $request->tipoPuesto,
        ]);

        if ($save) {
            return redirect()->route('superUsuario.grupoIndex');
        }
    }

    public function savePermisos(Request $request)
    {
        $grupo = GrupoUsuarios::find($request->id);
        $grupo->update([
            'vacantes' => $request->vacantes,
            'precandidatos' => $request->precandidatos,
            'candidato' => $request->candidato,
            'seguimiento' => $request->seguimiento,
            'empleados' => $request->empleados,
            'usuarios' => $request->usuarios,
            'inventarios' => $request->inventarios,
            'contactos' => $request->contactos,
            'clientes' => $request->clientes,
            'agentes_externos' => $request->agentesExternos,
            'polizas' => $request->polizas,
            'pagos' => $request->pagos,
            'autorizacion' => $request->autorizacion,
            'verificaciones' => $request->verificaciones,
            'ec' => $request->ec,
            'carga_polizas' => $request->cargaPolizas,
            'contabilidad' => $request->contabilidad,
            'nomina' => $request->nomina,
            'tranferencias' => $request->transferencias,
            'tranferencias_masivas' => $request->transferenciasMasivas,
            'siniestros' => $request->siniestros,
            'penalizaciones' => $request->penalizaciones,
            'catalogos' => $request->catalogos,
        ]);

        return redirect()->route('superUsuario.grupoIndex')->with('mensaje','Permisos actualizados correctamente');
    }


}
