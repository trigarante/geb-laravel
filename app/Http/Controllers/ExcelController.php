<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Excel;
use App\VentaLeads;
use App\VentaLeadsLog;
use Carbon\Carbon;

class ExcelController extends Controller
{
    public function index()
    {
        return view('import');    
    }

    public function action(Request $request)
    {
        ini_set('max_execution_time', 0);
        $path = $request->import_file->getRealPath(); 
       config(['excel.import.dates.columns' => [
        'fec_registro',
        'fec_expedicion'
    ]]);

      $importar =   Excel::filter('chunk')->load($path)->chunk(250, function($result)
        {

            
            $leads = VentaLeads::get();
            
                foreach ($result as $key ) {
                    foreach ($leads as $lead => $leadValue) {        
                        if ($leadValue->id_lead_aseguradora == $key->cotizacion) {

                            if ($key->e_final == 'S') {
                                $estado = 4;
                            } elseif ($key->e_final == 'E') {
                                $estado = 3;
                            }elseif ($key->e_final == 'C') {
                                $estado = 5;
                            }elseif ($key->e_final == '') {
                                $estado = 2;
                            }

                            
                            // $UNIX_DATE1 = ($key->fec_registro - 25569) * 86400;
                            // $key->fec_registro = gmdate("Y-m-d H:i:s", $UNIX_DATE1);

                            // $UNIX_DATE2 = ($key->fec_expedicion - 25569) * 86400;
                            // $key->fec_expedicion = gmdate("Y-m-d H:i:s", $UNIX_DATE2);

                           


                            if ($leadValue->fecha_envio != $key->fec_registro || $leadValue->status != $estado || 
                             $leadValue->fecha_venta != $key->fec_expedicion || $leadValue->num_poliza != $key->poliza
                           || $leadValue->prima_expedida != $key->prima_neta)
                            {
                               VentaLeadsLog::create([
                                'id' => $leadValue->id,
                                 'id_aseguradora' => 8,
                                'id_lead_aseguradora' => $leadValue->id_lead_aseguradora,
                                'fecha_envio' => $leadValue->fecha_envio,
                                'id_solicitud' => $leadValue->id_solicitud,
                                'id_cotizacion_a' => $leadValue->id_cotizacion_a,
                                'status' => $leadValue->status,
                                'fecha_venta' => $leadValue->fecha_venta,
                                'num_poliza' => $leadValue->num_poliza,
                                'referencia' => $leadValue->referencia,
                                'prima_expedida' => $leadValue->prima_expedida,
                                'id_sesion' => 1,
                               ]);  

                                   $lead = VentaLeads::find($leadValue->id);
                                   $lead->update([
                                'fecha_envio' => $key->fec_registro,
                                'status' => $estado,
                                'fecha_venta' => $key->fec_expedicion,
                                'num_poliza' => $key->poliza,
                                'prima_expedida' => $key->prima_neta,
                               ]);
                           $contAct = 0;      
                        $contAct = $contAct +1; 
                        echo $key->cotizacion.'/'. $leadValue->fecha_envio .'/'. $key->fec_registro.'<br>';
                           }
                        }
                    }
                }

                 foreach ($result as $key ) {
                    $encontrado = false;
                    foreach ($leads as $lead => $leadValue) {        
                        if ($leadValue->id_lead_aseguradora == $key->cotizacion) {
                            $encontrado = true;
                            $break;
                        }
                    }
                    if ($encontrado == false) {
                            if ($key->e_final == 'S') {
                                $estado = 4;
                            } elseif ($key->e_final == 'E') {
                                $estado = 3;
                            }elseif ($key->e_final == 'C') {
                                $estado = 5;
                            }elseif ($key->e_final == '') {
                                $estado = 2;
                            }
                        VentaLeads::create([
                            'id_aseguradora' => 8,
                            'id_lead_aseguradora' => $key->cotizacion,
                            'fecha_envio' => $key->fec_registro,
                            'id_cotizacion_a' => 10,
                            'status' => $estado,
                            'fecha_venta' => $key->fec_expedicion,
                            'num_poliza' => $key->poliza,
                            'prima_expedida' => $key->prima_neta,
                        ]);
                        
                    }
                }
        });


            return view('import')->with('mensaje','Importacion correcta ');
        
        
    }
}