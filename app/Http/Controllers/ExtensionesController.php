<?php

namespace App\Http\Controllers;
use App\ExtensionDisponiblesView;
use Illuminate\Http\Request;

class ExtensionesController extends Controller
{
    public function index()
    {
         
    	$extensiones = ExtensionDisponiblesView::get();
        return view('asignaciones.index', compact('extensiones'));
    }

     

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $candidato = Candidato::where('id_precandidato',$request->id)->count();
        if ($candidato == 1) {
            return redirect('/precandidato')->with('error','Error el precandidato ya tiene seguimiento');
        }
        $save=Candidato::create([
            'id_calificacion_competencias' => $request->competencia,
            'calificacion_examen' => $request->calificacion_examen,
            'id_estado' => 0,
            'id_precandidato' => $request->id,
            'comentarios' => $request->comentarios,
            ]);
        if ($save) {
           $data = PreCandidato::find($request->id);
           $data->id_estado = 1;
           $data->save(); 
           return redirect('/candidato');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidatos = CandidatoView::where('id_precandidato',$id)->first();
        return view('candidatos.show')->with('candidatos',$candidatos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

