<?php

namespace App\Http\Controllers;

use App\Ejecutivo;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $token;
    public function authenticate(Request $request)
    {
        $credentials = $request->only('usuario', 'password');

        try {
            if (!$this->token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        Ejecutivo::whereRaw('usuario = "'.$request->usuario.'"')->update([
            'token' => $this->token
        ]);


    }

    public function register(Request $request)
    {

        $user = Ejecutivo::create([
            'usuario' => $request->get('usuario'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 201);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function pass(Request $request)
    {
        $pass = Hash::make($request->get('password'));
        return response()-> $pass;
    }
}
