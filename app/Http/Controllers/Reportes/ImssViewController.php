<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rh\RhViews\EmpleadoView;
use App\Rh\RhViews\EjecutivoView;

class ImssViewController extends Controller {
    
    /**
     *      * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexReporteImss(Request $request) {
       
        $imssview = EmpleadoView::paginate(10);
        $ejecutivo= EjecutivoView::get();
        return view('empleado_reporte.indexreporteimss', compact('imssview','ejecutivo','now'));
    }

}
