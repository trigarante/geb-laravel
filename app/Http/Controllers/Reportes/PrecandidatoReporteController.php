<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Catalogos\CatalogosModels\Competencia;  
use App\Catalogos\CatalogosModels\EstadoMotivoRh;
use App\Rh\RhViews\PreCandidatoView;
use App\Rh\RhViews\SolicitudView;
use App\Rh\RhViews\EjecutivoView;

class PrecandidatoReporteController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($fechainicio, $fechafin) {
        return view('precandidato_reporte.index');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexreportefechas(Request $request) {
        $motivos = EstadoMotivoRh::where('activo',1)->get();
        $competencias= Competencia::where('activo',1)->get();
        $fechaInicio = $request->fechainicio;
        $fechaFin = $request->fechafin;
        $fechaInicioHoras = $request->fechainicio . ' 00:00:00';
        if($fechaInicio && $fechaFin){




            $precandidatos=PreCandidatoView::where('fecha_creacion', '>=', $fechaInicio . ' 00:00:00')
                    ->where('fecha_creacion', '<=', $fechaFin . ' 23:59:59')->orderBy('fecha_creacion', 'DESC')
                    ->paginate(10);
            $solicitud = SolicitudView::where('fecha', '>=', $fechaInicio. ' 00:00:00')
                ->where('fecha', '<=', $fechaFin . ' 23:59:59')->orderBy('fecha', 'DESC')
                ->get();
            $ejecutivo = EjecutivoView::where('fecha_ingreso', '>=', $fechaInicio. ' 00:00:00')
                ->where('fecha_ingreso', '<=', $fechaFin . ' 23:59:59')->orderBy('fecha_ingreso', 'DESC')
                ->get();
        }else{
            $precandidatos=PreCandidatoView::paginate(10);
            $solicitud = SolicitudView::get();
            $ejecutivo = EjecutivoView::get();
        }
        
        return view('precandidato_reporte.indexreportefechas',compact('precandidatos','competencias','motivos', 'fechaInicio', 'fechaFin','solicitud','ejecutivo' ));
        
    }

}
