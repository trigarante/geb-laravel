<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rh\RhViews\SeguimientoView;
use App\Rh\RhViews\EjecutivoView;
use App\Rh\RhViews\SolicitudView;

class SeguimientoReporteController extends Controller {

    /**
     *      * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexReporteSeguimiento(Request $request) {

        $fechaInicio = $request->fechainicio;
        $fechaFin = $request->fechafin;

        if ($fechaInicio && $fechaFin) {
            $seguimientos = SeguimientoView::where('fecha_ingreso', '>=', $fechaInicio. ' 00:00:00')
                    ->where('fecha_ingreso', '<=', $fechaFin. ' 23:59:59')->orderBy('fecha_ingreso', 'DESC')
                    ->paginate(10);
             $solicitud = SolicitudView::where('fecha', '>=', $fechaInicio. ' 00:00:00')
                ->where('fecha', '<=', $fechaFin. ' 23:59:59')->orderBy('fecha', 'DESC')
                ->get();
            $ejecutivo = EjecutivoView::where('fecha_ingreso', '>=', $fechaInicio. ' 00:00:00')
                ->where('fecha_ingreso', '<=', $fechaFin. ' 23:59:59')->orderBy('fecha_ingreso', 'DESC')
                ->get();
        } else {
            $seguimientos = SeguimientoView::get();
            $solicitud = SolicitudView::get();
            $ejecutivo = EjecutivoView::get();
        }
        return view('empleado_reporte.indexreporteseguimiento', compact('seguimientos', 'fechaInicio', 'fechaFin','solicitud','ejecutivo'));
    }

}
