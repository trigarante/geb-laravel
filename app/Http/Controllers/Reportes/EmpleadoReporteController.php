<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use App\Rh\RhViews\EmpleadoView;
use App\Rh\RhViews\SolicitudView;
use App\Rh\RhViews\EjecutivoView;
use Illuminate\Http\Request;
use App\Catalogos\CatalogosModels\Competencia;  
use App\Catalogos\CatalogosModels\EstadoMotivoRh;
class EmpleadoReporteController extends Controller
{
     /**
     *      * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexReporteEmpleado(Request $request) {

        $fechaInicio = $request->fechainicio;
        $fechaFin = $request->fechafin;
        if ($fechaInicio && $fechaFin) {
            $empleados = EmpleadoView::whereNotNull('fecha_ingreso')
                    ->where('fecha_ingreso', '>=', $fechaInicio." 00:00:00")
                    ->where('fecha_ingreso', '<=', $fechaFin." 23:59:59")->orderBy('fecha_ingreso', 'DESC')
                    ->paginate(10);
            $solicitud = SolicitudView::where('fecha', '>=', $fechaInicio)
                ->where('fecha', '<=', $fechaFin)->orderBy('fecha_creacion', 'DESC')
                ->get();
            $ejecutivo = EjecutivoView::where('fecha_ingreso', '>=', $fechaInicioHoras)
                    ->where('fecha_ingreso', '<=', $fechaFinHoras)->orderBy('fecha_ingreso', 'DESC')
                    ->paginate(10);
        } else {
            $empleados = EmpleadoView::whereNotNull('fecha_ingreso')
                    ->paginate(10);
            $solicitud = SolicitudView::get();
            $ejecutivo= EjecutivoView::get();
        }
        return view('empleado_reporte.indexreporteempleado', compact('empleados', 'fechaInicio', 'fechaFin','solicitud','ejecutivo'));
    }
}

