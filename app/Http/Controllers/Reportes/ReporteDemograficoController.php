<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use App\Rh\RhViews\EmpleadoView;
use App\Rh\RhViews\EjecutivoView;
use Illuminate\Http\Request;

class ReporteDemograficoController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */      
    public function indexReporteFechas(Request $request) {        
        $fechaInicio = $request->fechainicio;
        $fechaFin = $request->fechafin;
        $fechaInicioHoras = $request->fechainicio;
        $fechaFinHoras = $request->fechafin ;
        $campana;
        if ($request->fechainicio && $request->fechafin) {
            $empleados = EmpleadoView::where('fecha_ingreso', '>=', $fechaInicioHoras . ' 00:00:00')
                    ->where('fecha_ingreso', '<=', $fechaFinHoras . ' 23:59:59')->orderBy('fecha_ingreso', 'DESC')
                    ->paginate(10);
            $ejecutivo = EjecutivoView::where('fecha_ingreso', '>=', $fechaInicioHoras . ' 00:00:00')
                    ->where('fecha_ingreso', '<=', $fechaFinHoras . ' 23:59:59')->orderBy('fecha_ingreso', 'DESC')
                    ->paginate(10);
        } else {//quiza se tiene que quitar el paginate
        $empleados = EmpleadoView::paginate(10);
        $ejecutivo= EjecutivoView::get();
            
        }
        return view('empleado_reporte.indexreportedemografico', compact('empleados', 'fechaInicio', 'fechaFin','ejecutivo','campana'));
    }
}
