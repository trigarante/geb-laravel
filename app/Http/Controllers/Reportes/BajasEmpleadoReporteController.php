<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use App\Rh\RhViews\EmpleadoView;
use App\Rh\RhViews\EjecutivoView;
use Illuminate\Http\Request;

class BajasEmpleadoReporteController extends Controller
{
     /**
     *      * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexReporteBajasEmpleado(Request $request) {

        $fechaInicio = $request->fechainicio;
        $fechaFin = $request->fechafin;
        if ($fechaInicio && $fechaFin) {
            $empleadosbaja = EmpleadoView::whereNotNull('fecha_baja')
                    ->where('fecha_baja', '>=', $fechaInicio)
                    ->where('fecha_baja', '<=', $fechaFin)->orderBy('fecha_baja', 'DESC')
                    ->paginate(10);
            $ejecutivo = EjecutivoView::where('fecha_ingreso', '>=', $fechaInicioHoras)
                    ->where('fecha_ingreso', '<=', $fechaFinHoras)->orderBy('fecha_ingreso', 'DESC')
                    ->paginate(10);
        } else {
            $empleadosbaja = EmpleadoView::whereNotNull('fecha_baja')
                    ->paginate(10);
            $ejecutivo= EjecutivoView::get();

        }
        return view('empleado_reporte.indexreportebajaempleado', compact('empleadosbaja', 'fechaInicio', 'fechaFin','ejecutivo'));
    }
}
