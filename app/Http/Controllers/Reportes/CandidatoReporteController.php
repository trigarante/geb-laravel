<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rh\RhModels\Candidato;
use App\Rh\RhLogs\CandidatoLog;
use App\Catalogos\CatalogosModels\Competencia;
use App\Rh\RhViews\CandidatoView;
use App\Rh\RhModels\PreCandidato;
use App\Rh\RhLogs\PreCandidatoLog;
use App\Catalogos\CatalogosModels\EstadoMotivoRh;
use App\Rh\RhViews\SolicitudView;
use App\Rh\RhViews\EjecutivoView;

class CandidatoReporteController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    
    public function indexreportefechas(Request $request) {
        $motivos = EstadoMotivoRh::where('activo', 1)->get();
        $competencias = Competencia::where('activo', 1)->get();
        $fechaInicio = $request->fechainicio;
        $fechaFin = $request->fechafin;
        $fechaInicioHoras = $request->fechainicio . ' 00:00:00';
        $fechaFinHoras = $request->fechafin . ' 23:59:59';
        if ($request->fechainicio && $request->fechafin) {
            $candidatos = CandidatoView::where('fecha_ingreso', '>=', $fechaInicioHoras. ' 00:00:00')
                    ->where('fecha_ingreso', '<=', $fechaFinHoras)->orderBy('fecha_ingreso', 'DESC')
                    ->paginate(10);
            $solicitud = SolicitudView::where('fecha', '>=', $fechaInicio. ' 00:00:00')
                ->where('fecha', '<=', $fechaFin. ' 23:59:59')->orderBy('fecha', 'DESC')
                ->get();
             $ejecutivo = EjecutivoView::where('fecha_ingreso', '>=', $fechaInicio. ' 00:00:00')
                ->where('fecha_ingreso', '<=', $fechaFin. ' 23:59:59')->orderBy('fecha_ingreso', 'DESC')
                ->get();
        } else {
            $candidatos = CandidatoView::get();
            $solicitud = SolicitudView::get();
            $ejecutivo = EjecutivoView::get();
        }
        return view('candidatos_reporte.indexreportefechas', compact('candidatos', 'competencias', 'motivos', 'fechaInicio', 'fechaFin','solicitud','ejecutivo'));
    }

}
