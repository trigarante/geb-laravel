<?php

namespace App\Http\Controllers;

use App\Catalogos\CatalogosModels\Pais;
use App\Catalogos\CatalogosModels\TipoDocumento;
use Illuminate\Http\Request;
use App\Catalogos\CatalogosModels\Area;
use App\Catalogos\CatalogosModels\BolsaTrabajo;
use App\Catalogos\CatalogosModels\Competencia;
use App\Catalogos\CatalogosModels\Empresa;
use App\Catalogos\CatalogosModels\Escolaridad;
use App\Catalogos\CatalogosModels\EstadoCivil;
use App\Catalogos\CatalogosModels\EstadoEscolaridad;
use App\Catalogos\CatalogosModels\EstadoMotivoRh;
use App\Catalogos\CatalogosModels\Puesto;
use App\Catalogos\CatalogosModels\TipoPuesto;
use App\Catalogos\CatalogosModels\Vacante;
use App\Rh\RhModels\SubArea;
use App\Catalogos\CatalogosLogs\AreaLog;
use App\Catalogos\CatalogosLogs\BolsaTrabajoLog;
use App\Catalogos\CatalogosLogs\CompetenciaLog;
use App\Catalogos\CatalogosLogs\EmpresaLog;
use App\Catalogos\CatalogosLogs\EscolaridadLog;
use App\Catalogos\CatalogosLogs\EstadoCivilLog;
use App\Catalogos\CatalogosLogs\EstadoEscolaridadLog;
use App\Catalogos\CatalogosLogs\EstadoMotivoRhLog;
use App\Catalogos\CatalogosLogs\PuestoLog;
use App\Catalogos\CatalogosLogs\TipoPuestoLog;
use Illuminate\Support\Facades\Validator;

class CatalogosController extends Controller
{
    // index catalogos
    public function areaIndex()
    {
        $areas = Area::get();
        $empresas = Empresa::get();
        return view('catalogos.areaIndex', compact('areas', 'empresas'));
    }

    public function bolsaIndex()
    {
        $bolsas = BolsaTrabajo::get();
        return view('catalogos.bolsaIndex', compact('bolsas'));
    }

    public function competenciaIndex()
    {
        $competencias = Competencia::get();
        return view('catalogos.competenciaIndex', compact('competencias'));
    }

    public function empresaIndex()
    {
        $empresas = Empresa::get();
        return view('catalogos.empresaIndex', compact('empresas'));
    }

    public function escolaridadIndex()
    {
        $escolaridades = Escolaridad::get();
        return view('catalogos.escolaridadIndex', compact('escolaridades'));
    }

    public function estadoCivilIndex()
    {
        $estados = EstadoCivil::get();
        return view('catalogos.estadoCivilIndex', compact('estados'));
    }

    public function etdoEscolaridadIndex()
    {
        $estados = EstadoEscolaridad::get();
        return view('catalogos.etdoEscolaridadIndex', compact('estados'));
    }

    public function etdoMotivoIndex()
    {
        $estados = EstadoMotivoRh::get();
        return view('catalogos.etdoMotivoIndex', compact('estados'));
    }

    public function puestoIndex()
    {
        $puestos = Puesto::get();
        return view('catalogos.puestoIndex', compact('puestos'));
    }

    public function tipoPuestoIndex()
    {
        $tipoPuestos = TipoPuesto::get();
        return view('catalogos.tipoPuestoIndex', compact('tipoPuestos'));
    }

    public function paisIndex()
    {
        $paises = Pais::get();
        return view('catalogos.paisIndex', compact('paises'));
    }


    public function tipoDocumentoIndex()
    {
        $paises = Pais::get();
        $documentos = TipoDocumento::get();
        return view('catalogos.tipoDocumentoIndex', compact('documentos', 'paises'));
    }

    public function vacanteIndex()
    {
        $vacantes = Vacante::get();
        return view('catalogos.vacanteIndex',compact('vacantes','subAreas','puestos','tipoPuestos'));
    }

    // delete catalogos
    public function destroy(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        switch ($catalogo) {
            case 'area':
                $area = Area::find($id);
                $area->update([
                    'activo' => 0,
                ]);
                // inserta en el log 
                if ($area) {
                    return redirect()->route('catalogos.area.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                // redirige al index 

                break;

            case 'bolsa':
                $bolsa = BolsaTrabajo::find($id);
                $bolsa->update([
                    'activo' => 0,
                ]);
                // inserta en el log 
                if ($bolsa) {
                    return redirect()->route('catalogos.bolsa.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                // redirige al index

                break;

            case 'competencia':
                $competencia = Competencia::find($id);
                $competencia->update([
                    'activo' => 0,
                ]);
                // inserta en el log 
                if ($competencia) {
                    return redirect()->route('catalogos.competencias.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                // redirige al index

                break;

            case 'empresa':
                $empresa = Empresa::find($id);
                $empresa->update([
                    'activo' => 0,
                ]);
                // inserta en el log 
                if ($empresa) {
                    return redirect()->route('catalogos.empresa.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                // redirige al index

                break;

            case 'escolaridad':
                $escolaridad = Escolaridad::find($id);
                $escolaridad->update([
                    'activo' => 0,
                ]);
                // inserta en el log 
                if ($escolaridad) {
                    return redirect()->route('catalogos.escolaridad.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                // redirige al index

                break;

            case 'etdoCivil':
                $etdoCivil = EstadoCivil::find($id);
                $etdoCivil->update([
                    'activo' => 0,
                ]);
                // inserta en el log 
                if ($etdoCivil) {
                    return redirect()->route('catalogos.etdoCivil.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                break;

            case 'etdoEscolar':
                $etdoEscolar = EstadoEscolaridad::find($id);
                $etdoEscolar->update([
                    'activo' => 0,
                ]);
                // inserta en el log
                if ($etdoEscolar) {
                    return redirect()->route('catalogos.etdoEscolar.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                // redirige al index

                break;

            case 'etdoMotivo':
                $etdoMotivo = EstadoMotivoRh::find($id);
                $etdoMotivo->update([
                    'activo' => 0,
                ]);
                // inserta en el log
                if ($etdoMotivo) {
                    return redirect()->route('catalogos.etdoMotivo.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                break;
            case 'puesto':
                $puesto = Puesto::find($id);
                $puesto->update([
                    'activo' => 0,
                ]);
                // inserta en el log
                if ($puesto) {
                    return redirect()->route('catalogos.puesto.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                break;
            case 'tipoPuesto':
                $tipoPuesto = tipoPuesto::find($id);
                $tipoPuesto->update([
                    'activo' => 0,
                ]);
                // inserta en el log
                if ($tipoPuesto) {
                    return redirect()->route('catalogos.tipoPuesto.index')->with('mensaje', 'Catalogo eliminado correctamente');
                }
                break;

            default:
                return redirect('catalogos/area');
                break;
        }
    }


  
    // save catalogos
    function saveArea(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
            'empresa' => 'required'
        ], [
            'nombre.required' => 'Se requiere un nombre',
            'descripcion.required' => 'Se requiere una descripcion',
            'empresa.required' => 'Se requiere una empresa'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = Area::create([
            'nombre' => strtoupper($request->nombre),
            'descripcion' => strtoupper($request->descripcion),
            'id_empresa' => $request->empresa,
            'activo' => 1,
        ]);


    }

    public function saveBolsa(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'tipo' => 'required',

        ], [
            'nombre.required' => 'Se requiere un nombre',
            'tipo.required' => 'Se requiere un tipo',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = BolsaTrabajo::create([
            'nombre' => strtoupper($request->nombre),
            'tipo' => $request->tipo,
            'activo' => 1,
        ]);


    }

    public function saveCompetencia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'escala' => 'required',
            'estado' => 'required',
            'descripcion' => 'required'
        ], [
            'escala.required' => 'Se requiere una escala',
            'estado.required' => 'Se requiere un estado',
            'descripcion.required' => 'Se requiere una descripcion'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = Competencia::create([
            'escala' => $request->escala,
            'estadio' => strtoupper($request->estado),
            'descripcion' => strtoupper($request->descripcion),
            'activo' => 1,
        ]);


    }

    public function saveEmpresa(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
            'descripcion.required' => 'Se requiere una descripcion',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = Empresa::create([
            'nombre' => strtoupper($request->nombre),
            'descripcion' => strtoupper($request->descripcion),
            'id_grupo' => 1,
            'activo' => 1,
        ]);

    }

    public function saveEscolaridad(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nivel' => 'required',
        ], [
            'nivel.required' => 'Se requiere un nivel',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = Escolaridad::create([
            'nivel' => strtoupper($request->nivel),
            'activo' => 1,
        ]);
    }

    public function saveEtdoCivil(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required',
        ], [
            'descripcion.required' => 'Se requiere una descripcion',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = EstadoCivil::create([
            'descripcion' => strtoupper($request->descripcion),
            'activo' => 1,
        ]);

    }

    public function saveEtdoEscolar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'estado' => 'required',
        ], [
            'estado.required' => 'Se requiere un estado',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = EstadoEscolaridad::create([
            'estado' => $request->estado,
            'activo' => 1,
        ]);

    }

    public function saveEtdoMotivo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'estado' => 'required',
        ], [
            'estado.required' => 'Se requiere un estado',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        if (isset($request->seguimiento))
            $seguimiento = $request->seguimiento;
        else
            $seguimiento = 0;

        if (isset($request->preCandidato))
            $preCandidato = $request->preCandidato;
        else
            $preCandidato = 0;

        if (isset($request->candidato))
            $candidato = $request->candidato;
        else
            $candidato = 0;

        if (isset($request->empleado))
            $empleado = $request->empleado;
        else
            $empleado = 0;

        $save = EstadoMotivoRh::create([
            'estado' => strtoupper($request->estado),
            'pre-candidato' => $preCandidato,
            'candidato' => $candidato,
            'seguimiento' => $seguimiento,
            'empleado' => $empleado,
            'id_estado_rh' => 0,
            'activo' => 1,
        ]);

    }

    public function savePuesto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = Puesto::create([
            'nombre' => strtoupper($request->nombre),
            'activo' => 1,
        ]);

    }

    public function saveTipoPuesto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = TipoPuesto::create([
            'nombre' => strtoupper($request->nombre),
            'activo' => 1,
        ]);

    }

    public function savePais(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = Pais::create([
            'nombre' => strtoupper($request->nombre),
            'activo' => 1,
        ]);

    }

    public function saveTipoDocumento(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'pais' => 'required',
            'descripcion' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
            'pais.required' => 'Se requiere un pais',
            'descripcion.required' => 'Se requiere una descripcion',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = TipoDocumento::create([
            'nombre' => strtoupper($request->nombre),
            'id_pais' => $request->pais,
            'descripcion' => strtoupper($request->descripcion),
            'activo' => 1
        ]);

    }

public function updatePais(Request $request)
    {
        $$validator = Validator::make($request->all(), [
            'nombre' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $save = pais::find($request->id);
        $save->update([
            'nombre' => strtoupper($request->nombre),
            
        ]);

    }

    // update catalogos
    public function updateArea(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
            'empresa' => 'required'
        ], [
            'nombre.required' => 'Se requiere un nombre',
            'descripcion.required' => 'Se requiere una descripcion',
            'empresa.required' => 'Se requiere una empresa'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $area = Area::find($request->id);
        $area->update([
            'nombre' => strtoupper($request->nombre),
            'descripcion' => strtoupper($request->descripcion),
            'id_empresa' => $request->empresa,
        ]);


    }

    public function updateBolsa(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'tipo' => 'required',

        ], [
            'nombre.required' => 'Se requiere un nombre',
            'tipo.required' => 'Se requiere un tipo',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $bolsa = BolsaTrabajo::find($request->id);
        $bolsa->update([
            'nombre' => strtoupper($request->nombre),
            'tipo' => $request->tipo,
        ]);

    }

    public function updateCompetencia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'escala' => 'required',
            'estado' => 'required',
            'descripcion' => 'required'
        ], [
            'escala.required' => 'Se requiere una escala',
            'estado.required' => 'Se requiere un estado',
            'descripcion.required' => 'Se requiere una descripcion'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $competencia = Competencia::find($request->id);
        $competencia->update([
            'escala' => $request->escala,
            'estadio' => strtoupper($request->estado),
            'descripcion' => strtoupper($request->descripcion),
        ]);


    }

    public function updateEmpresa(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
            'descripcion.required' => 'Se requiere una descripcion',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $empresa = Empresa::find($request->id);
        $empresa->update([
            'nombre' => strtoupper($request->nombre),
            'descripcion' => strtoupper($request->descripcion),
        ]);

    }

    public function updateEscolaridad(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nivel' => 'required',
        ], [
            'nivel.required' => 'Se requiere un nivel',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $escolaridad = Escolaridad::find($request->id);
        $escolaridad->update([
            'nivel' =>strtoupper($request->nivel),
        ]);
    }

    public function updateEtdoCivil(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required',
        ], [
            'descripcion.required' => 'Se requiere una descripcion',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $etdoCivil = EstadoCivil::find($request->id);
        $etdoCivil->update([
            'descripcion' => strtoupper($request->descripcion)
        ]);

    }

    public function updateEtdoEscolar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'estado' => 'required',
        ], [
            'estado.required' => 'Se requiere un estado',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $etdoEscolar = EstadoEscolaridad::find($request->id);
        $etdoEscolar->update([
            'estado' => strtoupper($request->estado)
        ]);


    }

    public function updateEtdoMotivo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'estado' => 'required',
        ], [
            'estado.required' => 'Se requiere un estado',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $etdoMotivo = EstadoMotivoRh::find($request->id);
        $etdoMotivo->update([
            'estado' => strtoupper($request->estado),
        ]);


    }

    public function updatePuesto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $puesto = Puesto::find($request->id);
        $puesto->update([
            'nombre' => strtoupper($request->nombre),
        ]);

    }

    public function updateTipoPuesto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
        ], [
            'nombre.required' => 'Se requiere un nombre',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $tipoPuesto = TipoPuesto::find($request->id);
        $tipoPuesto->update([
            'nombre' => strtoupper($request->nombre),
        ]);


    }


}