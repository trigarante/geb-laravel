<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AsignacionControler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = Asignaciones::get();
        return view('asignaciones.index',compact('asignacion'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(EmpleadoRequest $request)
    {

       // $save= 
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empleado =EmpleadoView::where('id_seguimiento',$id)->first();
        return view('empleado.show',compact('empleado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado = EmpleadoView::find($id);
        $bancos = Banco::where('activo',1)->get();
        $puestos = Puesto::where('activo',1)->get();
        $tipoPuestos = TipoPuesto::where('activo',1)->get();
        $areas = Area::where('activo',1)->get();
        $empresas = Empresa::where('activo',1)->get();
        return view('empleado.update')->with(['puestos'=>$puestos,'id'=>$id,'tipoPuestos'=>$tipoPuestos,'areas'=>$areas,
            'empresas'=>$empresas,'bancos'=>$bancos, 'empleado'=>$empleado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $empleado = Empleado::find($request->id);
        $empleado->update([
            'fecha_alta_imss' => $request->fechaImss,
            'puesto_detalle' => $request->detallePuesto,
            'id_puesto' => $request->puesto,
            'id_puesto_tipo' => $request->tipoPuesto,
            'id_area' => $request->area,
            'sueldo_diario' => $request->sueldoDiario,
            'sueldo_mensual' => $request->sueldoMensual,
            'fecha_cambio_sueldo' => $request->fechaCambioSueldo,
            'cta_clabe' => $request->cuentaClabe,
            'id_empresa' => $request->empresa
        ]);
        return redirect()->route('empleado.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
