<?php


namespace App\Http\Controllers\Inventario;

use App\Http\Requests\TecladoRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\Inventario\InventarioModels\EstadoInventario; 
use App\Inventario\InventarioView\TecladoView;
use App\Inventario\InventarioLogs\TecladoLog;
use App\Inventario\InventarioModels\Teclado;
 


class TecladoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function tecladoIndex() {
        $teclados = TecladoView::where('activo','=',1)->get();
        $estadoinventarios = EstadoInventario::get(); //Aqui se debe poner el estado
        //return view('inventarioempleado.monitorIndex',compact('areas','empresas'));
        return view('inventario.tecladoIndex', compact('teclados', 'estadoinventarios'));
    }
    
 function saveTeclado(TecladoRequest $request) {
   
  
        $save = Teclado::create([
            'num_folio' => $request->num_folio,
            'marca' => $request->marca,
            'id_estado' => $request->estadoinventario,
            'id_entrega'=> 1,
            'activo'=>1,
            ]);
            if ($save) {
           TecladoLog::create([
                'id' => $save->id,
                'num_folio' => $save->num_folio,
                'marca' => $save->marca,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                'id_sesion' => 0,
            ]);
        }

             return redirect()->route('inventario.teclado.index')->with('mensaje','Registro creado  correctamente');
    }
    public function updateTeclado(Request $request) {
        $this->validate($request, [
            'num_folio' => 'regex:/^[\w-]*$/|min:6|max:22|required',
        ], [ 
              'num_folio.regex' => 'El numero de folio solo permite caracteres alfanumericos',
              'num_folio.required' => 'Se requiere un numero  de folio',
              'num_folio.min' => 'el numero de folio debe tener como minimo 6 caracteres',
              'num_folio.max' => 'el numero de folio debe tener como maximo 22 caracteres',
              ]);

        $save = Teclado::find($request->id);
        $save->update([
        'num_folio' => $request->num_folio,
        'marca' => $request->marca,
        'id_estado'=> $request->estadoinventario,
            
        ]); 
            if ($save) {
            TecladoLog::create([
                'id' => $save->id,
                'num_folio' => $save->num_folio,
                'marca' => $save->marca,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                'id_sesion' => 0,
            ]);
        }
        return redirect()->route('inventario.teclado.index');
    }

    public function destroyTeclado(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        $save = Teclado::find($id);
        $save->update([
             'activo' => 0,
             ]);
                  
                return redirect()->route('inventario.teclado.index');
    }
}
