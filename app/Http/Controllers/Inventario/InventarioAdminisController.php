<?php

namespace App\Http\Controllers\Inventario;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;  
use App\Inventario\InventarioView\InventarioEjecutivo; 
use App\Inventario\InventarioModels\EstadoInventario;
use App\Inventario\InventarioView\InventarioView;
use App\Inventario\InventarioView\AdminView;
use App\Inventario\InventarioLogs\InventarioLog;
use App\Inventario\InventarioModels\Inventario;
use App\Inventario\InventarioModels\Monitor;
use App\Inventario\InventarioModels\Teclado;  
use App\Inventario\InventarioModels\Mouse;
use App\Inventario\InventarioModels\Cpu;


class InventarioAdminisController extends Controller 
{

    public function index() {
        $empleados = AdminView::where('estado','=',1)->get();
          
      
       // $empleados = Ejecutivo::where('estado','=',1)->get();
        return view('inventario.adminIndex', compact('empleados'));
    } 

        public function create($id)
    {   
        $mice= Mouse::where(['id_entrega' => 1, 'activo' => 1])->get();
        $monitores = Monitor::where(['id_entrega' => 1, 'activo' => 1])->get();
        $cpus = Cpu::where(['id_entrega' => 1, 'activo' => 1])->get();
        $teclados = Teclado::where(['id_entrega' => 1, 'activo' => 1])->get();
        
        
        $empleados = AdminView::find($id);
        return view('inventario.createad')->with(['id'=>$id,'empleados'=>$empleados,'mice'=>$mice,'monitores'=>$monitores,'cpus'=>$cpus,'teclados'=>$teclados]); 
    } 


    public function asignar(Request $request) {
    

        $save = Inventario::create([
                    'id' => $request->id,
                    'id_usuario' => $request->id_usuario,
                    'id_area' => $request->id_area,
                    'cpu' => $request->asigna_cpu,
                    'monitor' => $request->asigna_monitor,
                    'teclado' => $request->asigna_teclado,
                    'mouse' => $request->asigna_mouse,
                ]);
                    
        //Actualiza el estado de la entrega para los diferentes dispositivos
        if ($request->asigna_cpu) {
            $cpuactualiza = Cpu::find($request->asigna_cpu);
            $cpuactualiza->update([
                'id_entrega' => 2
            ]);
        }
        if ($request->asigna_monitor) {
            $monitoractualiza = Monitor::find($request->asigna_monitor);
            $monitoractualiza->update([
                'id_entrega' => 2
            ]);
        }
        if ($request->asigna_teclado) {
            $tecladoactualiza = Teclado::find($request->asigna_teclado);
            $tecladoactualiza->update([
                'id_entrega' => 2
            ]);
        }
        if ($request->asigna_mouse) {
            $mouseactualiza = Mouse::find($request->asigna_mouse);
            $mouseactualiza->update([
                'id_entrega' => 2
            ]);
        }
      
        //Fin actualiza el estado de la entrega para los diferentes dispositivos
        //Si la información ser guarda regresar al listado de ejecutivos a los cuales se asigna equipo
        // if ($save) {
        //     InventarioLog::Create([
        //             'id' => $save->id,
        //             'id_ejecutivo' => $save->id,
        //             'id_area' => $save->id_area,
        //             'cpu' => $save->asigna_cpu,
        //             'monitor' => $save->asigna_monitor,
        //             'teclado' => $save->asigna_teclado,
        //             'mouse' => $save->asigna_mouse,
                    
        //             'id_sesion' => 0,
        //         ]);
        //     
        // }
        return redirect(route('inventario.admin.index'));
    } 
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request->id;
        $catalogo = $request->catalogo;
        
    }

    //Fin index dispositivos
}
