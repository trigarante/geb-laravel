<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Requests\DiademaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Inventario\InventarioModels\EstadoInventario;
use App\Inventario\InventarioView\DiademaView;
use App\Inventario\InventarioLogs\DiademaLog;
use App\Inventario\InventarioModels\Diadema;




class DiademaController extends Controller
{
   
     public function DiademaIndex() {
        $diadema = DiademaView::where('activo','=',1)->get();
        $estadoinventarios = EstadoInventario::get(); //Aqui se debe poner el estado
        //return view('inventarioempleado.monitorIndex',compact('areas','empresas'));
        return view('inventario.DiademaIndex', compact('diadema', 'estadoinventarios'));
    }
      function saveDiadema(DiademaRequest $request) {
           
  
           $save = Diadema::create([
            'num_folio' => $request->num_folio,
            'marca' => $request->marca,
            'id_estado' => $request->estadoinventario,
            'id_entrega' => 1,
            'activo' => 1,
        ]);
        if ($save) {
            DiademaLog::create([
                'id' => $save->id,
                'num_folio' => $save->num_folio,
                'marca' => $save->marca,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                //'activo' => $save->activo,
                'id_sesion' => 0,
            ]);
        } 
        return redirect()->route('inventario.diadema.index');
    
    }

    public function updateDiadema(DiademaRequest $request) {

        $save = Diadema::find($request->id);
        $save->update([
            'num_folio' => $request->num_folio,
            'marca' => $request->marca,
            'id_estado' => $request->estadoinventario
        ]);
        if ($save) {
            DiademaLog::create([
                'id' => $save->id,
                'num_folio' => $save->num_folio,
                'marca' => $save->marca,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                // 'activo' => $save->activo,
                'id_sesion' => 0,
            ]);
        }
        return redirect()->route('inventario.diadema.index');   
         
    }

    public function destroyDiadema(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        $save= Diadema::find($id);
        $save->update([
           'activo' => 0,
            ]);
        
            return redirect()->route('inventario.diadema.index');
    }
}
