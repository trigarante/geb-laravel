<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Controllers\Controller;
use App\Http\Requests\CpuRequest;
use Illuminate\Http\Request;
use App\Inventario\InventarioModels\EstadoInventario;
use App\Inventario\InventarioView\CpuView;
use App\Inventario\InventarioLogs\CpuLog;
use App\Inventario\InventarioModels\Cpu; 


class CpuController extends Controller
{

     public function cpuIndex() {
        $cpus = CpuView::where('activo','=',1)->get(); 
        $estadoinventarios = EstadoInventario::get(); //Aqui se debe poner el estado
        //return view('inventarioempleado.monitorIndex',compact('areas','empresas'));
        return view('inventario.cpuIndex', compact('cpus', 'estadoinventarios'));
    } 
     
 
        function saveCpu(CpuRequest $request) {

       $save = Cpu::create([
            'numero_serie' => $request->numero_serie,
            'porcesador' => $request->porcesador,
            'hdd' => $request->hdd,
            'ram' => $request->ram,
            'marca' => $request->marca,
            'modelo' => $request->modelo,
            'id_estado'=> $request->estadoinventario,               
            'id_entrega' => 1,
             ]);
       if ($save) {
            CpuLog::create([
            'id' => $save->id,
            'numero_serie' => $save->numero_serie,
            'porcesador' => $save->porcesador,
            'hdd' => $save->hdd,
            'ram' => $save->ram,
            'marca' => $save->marca,
            'modelo' => $save->modelo,
            'id_estado'=> $save->id_estado,
            'id_entrega' => $save->id_entrega,
            'id_sesion' => 0,
            ]);
        }

        return redirect()->route('inventario.cpu.index')->with('mensaje','Registro creado  correctamente');
        
    }
    public function updateCpu(Request $request) {
        $this->validate($request, [
            'numero_serie' => 'regex:/^[\w-]*$/|min:6|max:22|required',
            'porcesador' => 'required',   
        ], [ 
              'numero_serie.regex' => 'El numero de serie solo permite caracteres alfanumericos',
              'numero_serie.required' => 'se requiere un numero  de serie',
              'numero_serie.min' => 'el numero de serie debe tener como minimo 6 caracteres',
              'numero_serie.max' => 'el numero de serie debe tener como maximo 10 caracteres',
              'porcesador.required' => 'Se requiere porcesador'
            ]);

        $save = Cpu::find($request->id);
        $save->update([
            'numero_serie' => $request->numero_serie,
            'porcesador' => $request->porcesador,
            'hdd' => $request->hdd, 
            'ram' => $request->ram,
            'marca' => $request->marca,
            'modelo' => $request->modelo,
            'id_estado'=> $request->estadoinventario,
            
        ]);
if ($save) { 
            CpuLog::create([
            'id' => $save->id,
            'numero_serie' => $save->numero_serie,
            'porcesador' => $save->porcesador,
            'hdd' => $save->hdd,
            'ram' => $save->ram,
            'marca' => $save->marca,
            'modelo' => $save->modelo,
            'id_estado'=> $save->id_estado,
            'id_entrega' => $save->id_entrega,
            'id_sesion' => 0,
            ]);
        }        return redirect()->route('inventario.cpu.index');
    }

    public function destroyCpu(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        $save = Cpu::find($id);
        $save->update([
             'activo' => 0,
             ]);
              
                return redirect()->route('inventario.cpu.index');
    }
}
