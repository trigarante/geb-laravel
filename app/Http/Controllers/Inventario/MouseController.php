<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Requests\MouseRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Inventario\InventarioModels\EstadoInventario;
use App\Inventario\InventarioView\MouseView;
use App\Inventario\InventarioLogs\MouseLog;
use App\Inventario\InventarioModels\Mouse;

 

class MouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 public function mouseIndex() {
        
        $mouseV = MouseView::where('activo','=',1)->get();
        $estadoinventarios = EstadoInventario::get(); //Aqui se debe poner el estado
        //return view('inventarioempleado.monitorIndex',compact('areas','empresas'));
        return view('inventario.mouseIndex', compact('mouseV', 'estadoinventarios'));
    }
      function saveMouse(MouseRequest $request) {
       
           $save =  Mouse::create([
            'num_folio' => $request->num_folio,
            'marca' => $request->marca,
            'id_estado' => $request->estadoinventario,
            'id_entrega' => 1,
            'activo' => 1,
        ]);
        if ($save) {
            MouseLog::create([
                'id' => $save->id,
                'num_folio' => $save->num_folio,
                'marca' => $save->marca,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                //'activo' => $save->activo,
                'id_sesion' => 0,
            ]);
        }
        return redirect()->route('inventario.mouse.index');
    
    
    }

    public function updateMouse(Request $request) {
        $this->validate($request, [
            'num_folio' => 'regex:/^[\w-]*$/|min:6|max:22|required',
        ], [ 
              'num_folio.regex' => 'El numero de folio solo permite caracteres alfanumericos',
              'num_folio.required' => 'Se requiere un numero  de folio',
              'num_folio.min' => 'el numero de folio debe tener como minimo 6 caracteres',
              'num_folio.max' => 'el numero de folio debe tener como maximo 22 caracteres',
              ]);
   

       $save = Mouse::find($request->id);
        $save->update([
            'num_folio' => $request->num_folio,
            'marca' => $request->marca,
            'id_estado' => $request->estadoinventario,
        ]);
        if ($save) {
            MouseLog::create([
                'id' => $save->id,
                'num_folio' => $save->num_folio,
                'marca' => $save->marca,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                // 'activo' => $save->activo,
                'id_sesion' => 0,
            ]);
        }
        return redirect()->route('inventario.mouse.index');   
    }

    public function destroyMouse(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        $save= Mouse::find($id);
        $save->update([
           'activo' => 0,
            ]);
        
            return redirect()->route('inventario.mouse.index');

    }
}
