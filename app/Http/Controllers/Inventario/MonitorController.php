<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Requests\MonitorRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\Inventario\InventarioModels\EstadoInventario; 
use App\Inventario\InventarioModels\EstadoEntrega;
use App\Inventario\InventarioView\MonitorView;
use App\Inventario\InventarioLogs\MonitorLog;
use App\Inventario\InventarioModels\Monitor;





class MonitorController extends Controller 
{
        public function monitorIndex() {
        $monitores = MonitorView::where('activo','=',1)->get();
        // 'id_entrega' => 1
        $estadoentrega = EstadoEntrega::get();
        $estadoinventarios = EstadoInventario::get(); //Aqui se debe poner el estado
        //return view('inventarioempleado.monitorIndex',compact('areas','empresas'));
        return view('inventario.monitorIndex', compact('monitores', 'estadoinventarios','estadoentrega'));
    }
      

    //guarda monitor en inventario
    public function saveMonitor(MonitorRequest $request) {
 
       $save =  Monitor::create([
            'num_serie' => $request->num_serie,
            'marca' => $request->marca,
            'dimensiones' => $request->dimensiones,
            'id_estado' => $request->estadoinventario,
            'id_entrega' => 1,
            'activo' => 1,
        ]);
        if ($save) {
            MonitorLog::create([
                'id' => $save->id,
                'num_serie' => $save->num_serie,
                'marca' => $save->marca,
                'dimensiones' => $save->dimensiones,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                // 'activo' => $save->activo,
                'id_sesion' => 0,
            ]);
        }
        return redirect()->route('inventario.monitor.index')->with('mensaje','Registro creado  correctamente');
    
}

    public function updateMonitor(Request $request) {

         $this->validate($request, [
            'num_serie' => 'regex:/^[\w-]*$/|min:6|max:22|required',
            
        ], [ 
              'num_serie.regex' => 'El numero de serie solo permite caracteres alfanumericos',
              'num_serie.required' => 'se requiere un num de serie',
              'num_serie.min' => 'el numero de serie debe tener como minimo 6 caracteres',
              'num_serie.max' => 'el numero de serie debe tener como maximo 10 caracteres',
]);
   
            
        $save = Monitor::find($request->id);
        $save->update([
            'num_serie' => $request->num_serie,
            'marca' => $request->marca,
            'dimensiones' => $request->dimensiones,
            'id_estado' => $request->estadoinventario,
           
        ]);
        if ($save) {
            MonitorLog::create([
                'id' => $save->id,
                'num_serie' => $save->num_serie,
                'marca' => $save->marca,
                'dimensiones' => $save->dimensiones,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                'id_sesion' => 0,
            ]);
        }
        return redirect()->route('inventario.monitor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function destroyMonitor(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        $save= Monitor::find($id);
        $save->update([
           'activo' => 0,
            ]);
        
            return redirect()->route('inventario.monitor.index');

    }
}
