<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Requests\TabletRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Inventario\InventarioModels\EstadoInventario; 
use App\Inventario\InventarioView\TabletView;
use App\Inventario\InventarioLogs\TabletLog;
use App\Inventario\InventarioModels\Tablet;




class TabletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function tabletIndex() {
        $tablet = TabletView::where('activo','=',1)->get();
        $estadoinventarios = EstadoInventario::get(); //Aqui se debe poner el estado
        //return view('inventarioempleado.monitorIndex',compact('areas','empresas'));
        return view('inventario.tabletIndex', compact('tablet', 'estadoinventarios'));
    }
      function saveTablet(TabletRequest $request) {
      // $this->validate($request, [
      //       'num_folio' => 'regex:/^[\w-]*$/|min:6|max:22|required',
      //       'marca' => 'required',
      //   ], [ 
      //         'num_folio.regex' => 'El numero de folio solo permite caracteres alfanumericos',
      //         'num_folio.required' => 'Se requiere un numero  de folio',
      //         'num_folio.min' => 'el numero de folio debe tener como minimo 6 caracteres',
      //         'num_folio.max' => 'el numero de folio debe tener como maximo 22 caracteres',
      //         'marca.required' => 'Se requiere de la marca', 
      //         'telefono.required' => 'Se requiere un telefono',
      //         'telefono.digits_between' => 'El telefono tiene que ser numerico entre 8 y 10 digitos',
            
      //         ]);

          $save =  Tablet::create([
            'num_folio' => $request->num_folio,
            'procesador'=> $request->procesador,
            'capacidad'=> $request->capacidad,
            'ram' => $request->ram,
            'marca' => $request->marca,
            'telefono'=> $request->telefono,
            'id_estado' => $request->estadoinventario,
            'id_entrega' => 1,
            'activo' => 1,
        ]);
        if ($save) {
            TabletLog::create([
                'id' => $save->id,
                'num_folio' => $save->num_folio,
                'procesador'=> $save->procesador,
                'capacidad'=> $save->capacidad,
                'ram' => $save->ram,
                'marca' => $save->marca,
                'telefono' => $save->telefono,
                'id_estado' => $save->id_estado,
                'id_entrega' => $save->id_entrega,
                // 'activo' => 1,     
                'id_sesion' => 0,
            ]);
        }
        return redirect()->route('inventario.tablet.index');
    
    }

    public function updateTablet(TabletRequest $request) {
       
         
         $save = Tablet::find($request->id);
        $save->update([
            'num_folio' => $request->num_folio,
            'procesador'=> $request->procesador,
            'capacidad'=> $request->capacidad,
            'ram' => $request->ram,
            'marca' => $request->marca,
            'telefono' => $request->telefono,
            'id_estado' => $request->estadoinventario,

        ]);
        if ($save) {
            TabletLog::create([
                 'id' => $save->id,
                 'num_folio' => $save->num_folio,
                 'procesador'=> $save->procesador,
                 'capacidad'=> $save->capacidad,
                 'ram' => $save->ram,
                 'marca' => $save->marca,
                 'telefono' => $save->telefono,
                 'id_entrega' => $save->id_entrega,
                 'id_estado' => $save->id_estado,
                 'id_sesion' => 0,
            ]);
            return redirect()->route('inventario.tablet.index');
    }
}

 public function destroyTablet(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        $save= Tablet::find($id);
        $save->update([
           'activo' => 0,
            ]);
        
            return redirect()->route('inventario.tablet.index');

    }
 }
