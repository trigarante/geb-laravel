<?php
 
namespace App\Http\Controllers\Rh;

use App\Catalogos\CatalogosModels\EstadoMotivoRh;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rh\RhModels\Seguimiento;
use App\Rh\RhLogs\SeguimientoLog;
use App\Catalogos\CatalogosModels\Competencia;
use App\Rh\RhViews\SeguimientoView;
use App\Rh\RhModels\Candidato;
use App\Rh\RhModels\PreCandidato;
use App\Rh\RhModels\Empleado;
use App\Rh\RhLogs\CandidatoLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SeguimientoController extends Controller
{

    public function index()
    {
        $motivos = EstadoMotivoRh::where('activo',1)->get();
        $seguimientos = SeguimientoView::where('siguiente',0)->where('id_estado',100)->get();
        return view('seguimiento.index', compact('seguimientos','motivos'));
    }


    public function create()
    {

    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'calificacion_rollplay' => 'required|integer',
            'comentarios' => 'required'
        ], [
            'calificacion_rollplay.required' => 'Es requerida una calificacion',
            'calificacion_rollplay.integer' => 'La calificacion debe ser numerica',
            'comentarios.required' => 'Es necesario un comentario   '
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $seguimiento = Seguimiento::where('id_capacitacion', $request->id)->count();
        if ($seguimiento == 1) {
            return redirect()->route('candidato.index')->with('error', 'Error el precandidato ya tiene seguimiento');
        }
        $save = Seguimiento::create([
            'calificacion_rollplay' => $request->calificacion_rollplay,
            'id_capacitacion' => $request->id,
            'comentarios' => mb_strtoupper($request->comentarios,'utf-8'),
            'id_estado' => 100,
            'id_coach' => Auth::user()->id,
            'id_campana'=> $request->id_campana
        ]);
        if ($save)
            return redirect()->route('seguimiento.index')->with('mensaje', 'Seguimiento creado correctamente');
        else
            return redirect()->route('seguimiento.index')->with('mensaje', 'Error al crear seguimiento');
    }

    public function show($id)
    {
        $seguimiento = SeguimientoView::where('id_candidato', $id)->first();
        return view('seguimiento.show', compact('seguimiento'));

    }

    public function view($id)
    {
        $seguimiento = SeguimientoView::find($id);
        return view('seguimiento.show', compact('seguimiento'));
    }

    public function edit($id)
    {

    }


    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'calificacion' => 'required|integer',
            'comentarios' => 'required'
        ], [
            'calificacion.required' => 'Es requerida una calificacion',
            'calificacion.integer' => 'La calificacion debe ser numerica',
            'comentarios.required' => 'Es necesario un comentario   '
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $seguimiento = Seguimiento::find($request->id);
        $update = $seguimiento->update([
            'calificacion_rollplay' => $request->calificacion,
            'comentarios' => mb_strtoupper($request->comentarios,'utf-8'),
        ]);
        if ($update)
            return redirect()->route('seguimiento.index')->with('mensaje', 'Seguimiento actualizado correctamente');
        else
            return redirect()->route('seguimiento.index')->with('mensaje', 'Error al actualizar seguimiento');
    }


    public function destroy(Request $request)
    {
        $seguimiento = Seguimiento::find($request->id);
        if ($seguimiento) {
            $seguimiento->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('seguimiento.index')->with('mensaje', 'Seguimiento actualizado correctamente');
        }

        $candidato = Candidato::find($seguimiento->id_candidato);
        if ($candidato) {
            $candidato->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('seguimiento.index')->with('mensaje', 'Seguimiento actualizado correctamente');
        }

        $precandidato = PreCandidato::find($candidato->id_precandidato);
        if ($precandidato) {
            $precandidato->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('seguimiento.index')->with('mensaje', 'Pre Seguimiento actualizado correctamente');
        }

        $empleado = Empleado::where('id_seguimiento', $request->id)->first();
        if ($empleado) {
            $empleado->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('seguimiento.index')->with('mensaje', 'Seguimiento actualizado correctamente');
        }
        return redirect()->route('seguimiento.index')->with('mensaje', 'Seguimiento actualizado correctamente');
    }
}
