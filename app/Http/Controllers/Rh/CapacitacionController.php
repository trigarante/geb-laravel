<?php

namespace App\Http\Controllers\Rh;

use App\Asignaciones\AsignacionesModels\Campana;

use App\Http\Controllers\Controller;
use App\Rh\RhModels\Capacitacion;
use App\Catalogos\CatalogosModels\Competencia;
use Illuminate\Http\Request; 
use App\Rh\RhViews\CapacitacionView;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Validator;


class CapacitacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campanas = Campana::where('activo',1)->get();
        $capacitaciones = CapacitacionView::where('siguiente',0)->get();
        $competencias = Competencia::where('activo', 1)->get();
        return view('capacitacion.index',compact('capacitaciones','campanas','competencias'));
    }
// <label for="motivo_baja">Seleccione un Estatus</label>
//                         <select name="motivo_baja" id="motivo_baja" class="custom-select">
//                             @foreach($motivos as $motivo)
//                                 <option value="{{$motivo->id}}">{{$motivo->estado}}</option>
//                             @endforeach
//                         </select>
//     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'calificacion' => 'required',
            'comentarios' => 'required'
        ], [
            'calificacion.required' => 'Se requiere de una calificaion',
            'comentarios.required' => 'Es necesario un comentario'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

         Capacitacion::create([
            'id_candidato' => $request->id,
            'calificacion' => $request->calificacion,
             'comentarios' => strtoupper($request->comentarios),
             'id_capacitador' => Auth::user()->id
        ]);
    } 

    
    public function update(Request $request)
    {
        
        $update = Capacitacion::find($request->id);
        $updateCap = $update->update([

             'calificacion' => $request->calificacion_examen,
             'comentarios' => strtoupper($request->comentarios),
             
        ]);

        if ($update)
              return redirect()->route('capacitacion.index')->with('mensaje','Modificado correctamente');

       
    }


     
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
