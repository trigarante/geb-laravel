<?php

namespace App\Http\Controllers\Rh;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rh\RhModels\Administrativo;
use App\Auth\Usuario;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdministrativoController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'area' => 'required',
            'id_correo' => 'required'
        ], [
            'area.required' => 'Es requerida un area',
            'id_correo.required' => 'Es necesario un correo   '
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $administrativo = Administrativo::create([
            'id_empleado' => $request->idEmpleado,
            'estado' => 1,
            'id_area' => $request->area,
            'id_correo' => $request->id_correo,
        ]);

        if ($administrativo){
            $correo = Administrativo::find($administrativo->id)->asignaciones()->select('correo')->first();
Usuario::create([
    'usuario' => $correo->correo,
    'password' => Hash::make('trigarante')
]);
        }
    }
}
