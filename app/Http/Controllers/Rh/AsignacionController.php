<?php

namespace App\Http\Controllers\Rh;

use App\Catalogos\CatalogosModels\Area;
use App\Rh\RhModels\Asignacion;
use App\Http\Controllers\Controller;
use App\Auth\Usuario;
use App\Rh\RhModels\Administrativo;
use App\Rh\RhViews\EmpleadoView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\GrupoUsuarios;
use App\Asignaciones\AsignacionesModels\Extension;
use App\Asignaciones\AsignacionesModels\Campana;
use Illuminate\Support\Facades\Input;

class AsignacionController extends Controller
{

    public function index()
    {
        $extensiones = Extension::where('id_estado',2)->get();
        $campanas = Campana::where('activo',1)->get()->pluck('nombre','id');
        $empleados = EmpleadoView::where('asignado',0)->get();
        $correos = Asignacion::where('activo', 1)->get()->pluck('correo', 'id');
        $areas = Area::where('activo', 1)->get()->pluck('nombre', 'id');
        $grupos = GrupoUsuarios::where('activo',1)->get()->pluck('grupo','id');
        return view('asignaciones.index', compact('empleados', 'areas', 'correos','grupos','extensiones','campanas'));
    }

    public function saveAdministrativo(Request $request)
    {
        $administrativo = Administrativo::create([
            'id_empleado' => $request->id_empleado,
            'estado' => 1,
            'id_area' => $request->id_area,
            'id_correo' => $request->id_correo
        ]);
        if ($administrativo){
            $asignacion = Asignacion::find($request->id_correo);
            $asignacion->update([
                'activo' => 0
            ]);
            $correo = Administrativo::find($administrativo->id)->asignaciones()->select('correo')->first();
          $usuario =  Usuario::create([
                'usuario' => $correo->correo,
                'password' => Hash::make('trigarante'),
                'id_tipo' => $request->id_tipo,
                'id_grupo' => $request->id_grupo
            ]);
          if ($usuario)
              return redirect()->route('asignacion.index')->with('mensaje','Asignacion Correcta');
        }

    }

    public function saveEjectuvio()
    {

    }

    public function getExtension()
    {
        $id_campana = Input::get('id_campana');
        $extensiones = Extension::where('id_campana',$id_campana)->where('id_estado',2)->get();
        return response()->json($extensiones);
    }


}
