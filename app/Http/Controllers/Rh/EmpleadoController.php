<?php

namespace App\Http\Controllers\Rh;

use App\Asignaciones\AsignacionesModels\Campana;
use App\Asignaciones\AsignacionesModels\Ejecutivo;
use App\Http\Controllers\Controller;
use App\Rh\RhViews\CandidatoView;
use App\Rh\RhLogs\SeguimientoLog;
use App\Catalogos\CatalogosModels\SubArea;
use Illuminate\Http\Request;
use App\Catalogos\CatalogosModels\Puesto;
use App\Catalogos\CatalogosModels\TipoPuesto;
use App\Catalogos\CatalogosModels\Area;
use App\Catalogos\CatalogosModels\Empresa;
use App\Rh\RhModels\Seguimiento;
use App\Rh\RhModels\Empleado;
use App\Rh\RhModels\Candidato;
use App\Rh\RhModels\DocumentosRh;
use App\Catalogos\CatalogosModels\EstadoMotivoRh;
use App\Rh\RhModels\PreCandidato;
use App\Rh\RhLogs\EmpleadoLog;
use App\Rh\RhModels\Asignacion;
use App\Rh\RhViews\EmpleadoView;
use App\Banco;
use App\Http\Requests\EmpleadoRequest;

class EmpleadoController extends Controller
{

    public function index()
    {
        $areas = Area::get();
        $correos = Asignacion::where('activo', 0)->get();
        $empleados = EmpleadoView::get()->where('id_estado', 1);
        $motivos = EstadoMotivoRh::where('activo', 1)->where('empleado', 1)->get();
        return view('empleado.index', compact('empleados', 'motivos', 'areas', 'correos'));
    }

    public function create(Request $request)
    { 
        $campanas = Campana::where('activo', 1)->get();
        $bancos = Banco::where('activo', 1)->get();
        $puestos = Puesto::where('activo', 1)->get();
        $tipoPuestos = TipoPuesto::where('activo', 1)->get();
        $subAreas = SubArea::where('activo', 1)->get();
        $empresas = Empresa::where('activo', 1)->get();
        $id = $request->id;
        return view('empleado.create', compact('puestos', 'bancos', 'tipoPuestos', 'subAreas', 'empresas', 'id', 'campanas'));
    }

    public function store(EmpleadoRequest $request)
    {
        $empleado = Empleado::where('id_candidato', $request->idCandidato)->count();
        if ($empleado > 0) {
            return redirect()->route('seguimiento.index')->with('error', 'Error el empleado ya tiene seguimiento');
        }

        $curp=DocumentosRh::where('documento',$request->curp)->count();
        if ($curp > 0) {
            return redirect()->route('empleado.create',$request->idCandidato)->with('error', 'Curp ya se encuntra registrado');
        }

        $candidato = CandidatoView::find($request->idCandidato);
        $save = Empleado::create([
            'id_candidato' => $request->idCandidato,
            'fecha_alta_imss' => $request->fechaImss,
            'id_puesto' => strtoupper($request->puesto),
            'id_puesto_tipo' => strtoupper($request->tipoPuesto),
            'puesto_detalle' => strtoupper($request->detallePuesto),
            'id_subarea' => strtoupper($request->subArea),
            'sueldo_diario' => $request->sueldoDiario,
            'sueldo_mensual' => $request->sueldoMensual,
            'cta_clabe' => $request->cuentaClabe,
            'banco' => strtoupper($request->banco),
            'id_estado' => 1,
            'recontratable' => 1,
            'id_empresa' => strtoupper($request->empresa),
            'fecha_asigancion' => $request->fechaAsignacion,
            'kpi' => $request->kpi,
            'tipo_usuario' => $candidato->id_tipo,
        ]);


        $saveImss = DocumentosRh::create([
            'documento' => strtoupper($request->imss),
            'id_documento' => 1,
            'id_empleado' => $save->id
        ]); 

         if ($saveImss) {
                $saveCurp = DocumentosRh::create([
                'documento' => strtoupper($request->curp),
                'id_documento' => 2,
                'id_empleado' => $save->id
            ]);
        }
        if ($saveCurp) {
            DocumentosRh::create([
                'documento' => strtoupper($request->rfc),
                'id_documento' => 3,
                'id_empleado' => $save->id
            ]);
        }
        if ($save) {
           $saveEjecutivo = Ejecutivo::create([
                'id_empleado' => $save->id,
                'estado' => 1,
                'id_campana' => $request->id_campana
            ]);
           if ($saveEjecutivo)
               return redirect()->route('empleado.index')->with('mensaje', 'Empleado creado correctamente');
        } else
            return redirect()->route('empleado.index')->with('error', 'Error al crear el empleado');

    }

    public function show($id)
    {
        $empleado = EmpleadoView::where('id_empleado', $id)->first();
        return view('empleado.show', compact('empleado'));
    }

    public function edit($id)
    {
        $empleado = EmpleadoView::find($id);
        $document= DocumentosRh::find($id);
        $bancos = Banco::where('activo', 1)->get();
        $puestos = Puesto::where('activo', 1)->get();
        $tipoPuestos = TipoPuesto::where('activo', 1)->get();
        $areas = Area::where('activo', 1)->get();
        $empresas = Empresa::where('activo', 1)->get();
        $imss = DocumentosRh::where('id_documento',1)
                              ->where('id_empleado',$id)->first();

        $curp = DocumentosRh::where('id_documento',2)
                              ->where('id_empleado',$id)->first();

        $rfc = DocumentosRh::where('id_documento',3)
                             ->where('id_empleado',$id)->first();

        return view('empleado.update')->with(['puestos' => $puestos, 'id' => $id, 'tipoPuestos' => $tipoPuestos, 'areas' => $areas,'empresas' => $empresas, 'bancos' => $bancos, 'empleado' => $empleado,'imss' => $imss, 'curp' => $curp, 'rfc' => $rfc,'document' => $document]);
    }

    public function update(Request $request)
    {
        $empleado = Empleado::find($request->id); 
       
        $update = $empleado->update([
            'fecha_alta_imss' => $request->fechaImss,
            'puesto_detalle' => $request->detallePuesto,
            'id_puesto' => $request->puesto,
            'id_puesto_tipo' => $request->tipoPuesto,
            'id_area' => $request->area,
            'sueldo_diario' => $request->sueldoDiario,
            'sueldo_mensual' => $request->sueldoMensual,
            'fecha_cambio_sueldo' => $request->fechaCambioSueldo,
            'cta_clabe' => $request->cuentaClabe,
            'id_empresa' => $request->empresa,
            'banco' => $request->banco
        ]);
        $documento=DocumentosRh::find($request->id);
        
        $UpdateImss = $documento->update([
            'documento' => strtoupper($request->imss),
            'id_documento' => 1,
            'id_empleado' => $request->id
            
        ]); 

         if ($UpdateImss) {
                $UpdateCurp = $documento->update([
                'documento' => strtoupper($request->curp),
                'id_documento' => 2,
                'id_empleado' => $request->id
              
            ]);
        }
        if ($UpdateCurp) {
             $UpdateRfc = $documento->update([
                'documento' => strtoupper($request->rfc),
                'id_documento' => 3,
                'id_empleado' => $request->id
                
            ]);
        }
         

        if ($update)
            return redirect()->route('empleado.index')->with('mensaje', 'Empleado actualizado correctamente');
        else
            return redirect()->route('empleado.index')->with('error', 'Error al actualizar empleado');
    }

    public function destroy(Request $request)
    {

        $empleado = Empleado::find($request->id);
        if ($empleado) {
            $empleado->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('candidato.index')->with('mensaje', 'Empleado actualizado correctamente');
        }

        $seguimiento = Seguimiento::find($empleado->id_seguimiento);
        if ($seguimiento) {
            $seguimiento->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('candidato.index')->with('mensaje', 'Empleado actualizado correctamente');
        }

        $candidato = Candidato::find($seguimiento->id_candidato);
        if ($candidato) {
            $candidato->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('candidato.index')->with('mensaje', 'Empleado actualizado correctamente');
        }

        $preCandidato = PreCandidato::find($candidato->id_precandidato);
        if ($preCandidato) {
            $preCandidato->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('candidato.index')->with('mensaje', 'Empleado actualizado correctamente');
        }
        return redirect()->route('candidato.index')->with('mensaje', 'Empleado actualizado correctamente');
    }

    public function view($id)
    {
        $empleado = EmpleadoView::find($id);
        return view('empleado.show', compact('empleado'));
    }
}
