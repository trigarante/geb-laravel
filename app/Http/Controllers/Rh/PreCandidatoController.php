<?php

namespace App\Http\Controllers\Rh;

use App\Catalogos\CatalogosModels\CpSepomex;
use App\EstacionesLineas;
use App\Http\Controllers\Controller;
use App\Rh\RhModels\Candidato;
use App\Rh\RhModels\Empleado;
use App\Rh\RhModels\Seguimiento;
use Illuminate\Http\Request;
use App\Catalogos\CatalogosModels\EstadoCivil;
use App\Catalogos\CatalogosModels\Escolaridad;
use App\Catalogos\CatalogosModels\Pais;
use App\Catalogos\CatalogosModels\Competencia;
use App\Catalogos\CatalogosModels\EstadoEscolaridad;
use App\Rh\RhModels\PreCandidato;
use App\Rh\RhLogs\PreCandidatoLog;
use App\Rh\RhViews\PreCandidatoView;
use App\Catalogos\CatalogosModels\EstadoMotivoRh;
use App\ImagenUsuario;
use App\ImagenUsuarioLog;
use App\Http\Requests\PreCandidatoRequest;
use App\Rh\RhModels\SolicitudRrhh;
use App\Rh\RhLogs\SolicitudRrhhLog;
use App\MedioTransporte;
use Illuminate\Support\Facades\Input;
 
class PreCandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $motivos = EstadoMotivoRh::where('activo', 1)->where('pre-candidato',1)->get();
        $competencias = Competencia::where('activo', 1)->get();
        $precandidatos = PreCandidatoView::where('siguiente',0)->where('id_estado',100)->get();
        return view('preCandidatos.index', compact('precandidatos', 'competencias', 'motivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $estadoCivil = EstadoCivil::where('activo', 1)->get();
        $escolaridad = Escolaridad::where('activo', 1)->get();
        $estadoEscolar = EstadoEscolaridad::where('activo', 1)->get();
        $paises = Pais::where('activo', 1)->get();
        $solicitudDatos = SolicitudRrhh::find($id);
        $medios = MedioTransporte::get();
        $estaciones = EstacionesLineas::get();
        return view('preCandidatos.create',compact('id','estadoCivil','escolaridad','estadoEscolar','paises',
            'medioTransporte','estaciones','solicitudDatos','medios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PreCandidatoRequest $request)
    {
        $preCandidato = PreCandidato::where('id_solicitud_rrhh', $request->idSolicitud)->count();
        if ($preCandidato == 1) {
            return redirect('/solicitud')->with('error', 'Error solictud asignada a otro precandidato');
        }
        $guardarImagen = ImagenUsuario::create([
            'url' => 'images/1',
        ]);

        $idImagen = $guardarImagen->id;
        $save = PreCandidato::create([
            'id_solicitud_rrhh' => $request->idSolicitud,
            'id_imagen' => 1,
            'nombre' => mb_strtoupper($request->nombre,'utf-8'),
            'apellido_paterno' => mb_strtoupper($request->apellido_paterno,'utf-8'),
            'apellido_materno' => mb_strtoupper($request->apellido_materno,'utf-8'),
            'fecha_nacimiento' => mb_strtoupper($request->fechaNacimiento,'utf-8'),
            'email' => $request->email,
            'genero' => $request->genero,
            'id_estado_civil' => strtoupper($request->estadoCivil), 
            'id_escolaridad' => strtoupper($request->escolaridad),
            'id_etdo_escolaridad' => strtoupper($request->estadoEscolar),
            'id_pais' => strtoupper($request->pais),
            'cp' => $request->cp,
            'colonia' => mb_strtoupper($request->colonia,'utf-8'),
            'calle' => mb_strtoupper($request->calle,'utf-8'),
            'numero_exterior' => $request->numeroExt,
            'numero_interior' => $request->numeroInt,
            'telefono_fijo' => $request->telFijo,
            'telefono_movil' => $request->telMovil,
            'id_estado' => 100,
            'tiempo_traslado' => $request->tiempoTraslado,
            'id_medio_traslado' => strtoupper($request->medioTraslado),
            'id_estacion' => strtoupper($request->metroCercano),
            'estado' => strtoupper($request->estado),
            'del_mun' => strtoupper($request->del_mun),
            'ciudad' => strtoupper($request->ciudad)
        ]);
        if ($save) {
            $data = SolicitudRrhh::find($request->idSolicitud);
            $data->estado = 1;
            $data->save();
            return redirect()->route('preCandidatos.index')->with('mensaje', 'Precandidato creado correctamenmte');
        } else {
            return redirect()->route('preCandidatos.index')->with('mensaje', 'Error al crear precandidato');
        }

    }


    public function show($id)
    {
        $precandidato = PreCandidatoView::where('id_solicitud_rrhh', $id)->first();
        return view('preCandidatos.show')->with('precandidato', $precandidato);
    }

    public function view($id)
    {
        $precandidato = PreCandidatoView::find($id);
        return view('preCandidatos.show')->with('precandidato', $precandidato);
    }

    public function edit($id)
    {
        $medios = MedioTransporte::get()->pluck('medio','id');
        $estadoCivil = EstadoCivil::where('activo', 1)->get()->pluck('descripcion', 'id');
        $escolaridad = Escolaridad::where('activo', 1)->get()->pluck('nivel','id');
        $estadoEscolar = EstadoEscolaridad::where('activo', 1)->get()->pluck('estado','id');
        $precandidatos = PreCandidatoView::find($id);
        $paises = Pais::where('activo', 1)->get()->pluck('nombre','id');
        $estaciones = EstacionesLineas::get();
        return view('preCandidatos.update', compact('estadoCivil', 'escolaridad', 'estadoEscolar', 'precandidatos',
            'paises', 'medios', 'estaciones'));
    }

    public function update(Request $request)
    {
        $this->validate(request(), [
            'email' => ['required', 'email', 'unique:precandidato,email,' . request()->get("id")],
            'nombre' => ['required', 'regex:/^[\pL\s\-]+$/u'],
            'apellido_paterno' => ['required', 'regex:/^[\pL\s\-]+$/u'],
            'apellido_materno' => ['required', 'regex:/^[\pL\s\-]+$/u'],
            'fecha_nacimiento' => ['required'],
            'id_estado_civil' => ['required'],
            'id_escolaridad' => ['required'],
            'id_etdo_escolaridad' => ['required'],
            'id_pais' => ['required'],
            'cp' => ['required', 'digits:5'],
            'colonia' => ['required', 'string'],
            'calle' => ['required', 'string'],
            // 'telefono_fijo' => ['required', 'digits:8'],
            'telefono_movil' => ['required', 'digits:10'],
            'tiempo_traslado' => ['required', 'integer']

        ],
            [
                'nombre.required' => 'El campo nombre es requerido',
                'nombre.regex' => 'El formato del nombre es incorrecto',
                'apellido_paterno.required' => 'El campo apellido paterno es requerido',
                'apellido_paterno.regex' => 'El formato del apellido paterno es incorrecto',
                'apellido_materno.required' => 'El campo apellido materno es requerido',
                'apellido_materno.regex' => 'El formato del apellido materno es incorrecto',
                'email.required' => 'El campo correo es requerido',
                'email.unique' => 'Este correo ya se encuentra registrado',
                'email.email' => 'El campo correo no tiene un formato valido',
                'id_estado_civil.required' => 'El campo estado civil es requerido',
                'id_escolaridad.required' => 'El campo escolaridad es requerido',
                'id_etdo_escolaridad.required' => 'El campo estado de escolaridad requerido',
                'cp.required' => 'El codigo postal es requerido',
                'cp.digits' => 'El codigo postal debe ser de 5 digitos',
                'colonia.required' => 'El campo colonia es requerido',
                'calle.required' => 'El campo calle es requerido',
                // 'telefono_fijo.required' => 'El campo telefono fijo es requerido',
                // 'telefono_fijo.digits' => 'Telefono fijo debe ser numerico de 8 digitos',
                'telefono_movil.required' => 'El campo telefono movil es requerido',
                'telefono_movil.digits' => 'Telefono movil debe ser numerico de 10 digitos',
                'tiempo_traslado.required' => 'El campo tiempo de traslado es requerido',
                'tiempo_traslado.integer' => 'El campo tiempo de traslado debe ser numerico',


                'fecha_nacimiento.required' => 'La fecha de nacimiento es requerida',
                'id_pais.required' => 'El pais es requerido',
            ]);
        $precandidato = PreCandidato::find($request->id);
        $update = $precandidato->update([
            'nombre' => $request->nombre,
            'apellido_paterno' => $request->apellido_paterno,
            'apellido_materno' => $request->apellido_materno,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'email' => $request->email,
            'genero' => $request->genero,
            'id_estado_civil' => $request->id_estado_civil ,
            'id_escolaridad' => $request->id_escolaridad,
            'id_etdo_escolaridad' => $request->id_etdo_escolaridad,
            'id_pais' => $request->id_pais,
            'cp' => $request->cp,
            'colonia' => $request->colonia,
            'calle' => $request->calle,
            'numero_exterior' => $request->numero_exterior,
            'numero_interior' => $request->numero_interior,
            'telefono_fijo' => $request->telefono_fijo,
            'telefono_movil' => $request->telefono_movil,
            'tiempo_traslado' => $request->tiempo_traslado,
            'id_medio_traslado' => $request->id_medio_traslado,
            'id_estacion' => $request->metroCercano,
            'estado' => $request->estado,
            'del_mun' => $request->del_mun,
            'ciudad' => $request->ciudad
        ]);
        if ($update) {
            $data = SolicitudRrhh::find($request->id_solicitud_rrhh);
            $data->nombre = $update->nombre;
            $data->apellido_paterno = $update->apellido_paterno;
            $data->apellido_materno = $update->apellido_materno;
            $data->correo = $update->email;
            $data->save();
            return redirect()->route('preCandidatos.index')->with('mensaje', 'Pre candidato actualizado correctamente');
        }
        return redirect()->route('preCandidatos.index')->with('mensaje', 'Error al actualizar Pre Candidato');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $precandidato = PreCandidato::find($request->id);
        $precandidato->update([
            'id_estado' => $request->motivo_baja,
        ]);
        $candidato = Candidato::where('id_precandidato', $request->id)->first();
        if ($candidato) {
            $candidato->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('preCandidatos.index')->with('mensaje', 'Pre candidato actualizado correctamente');
        }
        $seguimiento = Seguimiento::where('id_candidato', $candidato->id)->first();
        if ($seguimiento) {
            $seguimiento->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('preCandidatos.index')->with('mensaje', 'Pre candidato actualizado correctamente');
        }
        $empleado = Empleado::where('id_seguimiento', $seguimiento->id)->first();
        if ($empleado) {
            $empleado->update([
                'id_estado' => $request->motivo_baja,
            ]);

        } else {
            return redirect()->route('preCandidatos.index')->with('mensaje', 'Pre candidato actualizado correctamente');
        }
        return redirect()->route('preCandidatos.index')->with('mensaje', 'Pre candidato actualizado correctamente');
    }

    public function foto()
    {
        $imagenCodificada = file_get_contents("php://input"); //Obtener la imagen
        if (strlen($imagenCodificada) <= 0) exit("No se recibió ninguna imagen");
        //La imagen traerá al inicio data:image/png;base64, cosa que debemos remover
        $imagenCodificadaLimpia = str_replace("data:image/png;base64,", "", urldecode($imagenCodificada));

        //Venía en base64 pero sólo la codificamos así para que viajara por la red, ahora la decodificamos y
        //todo el contenido lo guardamos en un archivo
        $imagenDecodificada = base64_decode($imagenCodificadaLimpia);

        //Calcular un nombre único
        $nombreImagenGuardada = "images/foto_" . uniqid() . ".png";

        //Escribir el archivo
        file_put_contents($nombreImagenGuardada, $imagenDecodificada);

        //Terminar y regresar el nombre de la foto
        exit($nombreImagenGuardada);
    }

    public function getLinea()
    {
        $medioId = Input::get('medioId');
        $lineas = EstacionesLineas::select('lineas')->where('id_transporte',$medioId)->groupBy('lineas')->get();
        return response()->json($lineas);
    }

    public function getMetro()
    {
        $medioId = Input::get('medioId');
        $lineaId = Input::get('lineaId');
        $estaciones = EstacionesLineas::where('lineas',$lineaId)->where('id_transporte',$medioId)->get();
        return response()->json($estaciones);
    }

    public function getCp()
    {
        $cp = Input::get('cp');
        $colonias = CpSepomex::select('asenta','del_mun','estado','ciudad')->where('cp',$cp)->get();
        return response()->json($colonias);
    }
}
