<?php

namespace App\Http\Controllers\Rh;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rh\RhModels\SolicitudRrhh;
use App\Rh\RhLogs\SolicitudRrhhLog;
use App\Rh\RhViews\SolicitudView;
use App\Catalogos\CatalogosModels\BolsaTrabajo;
use App\Catalogos\CatalogosModels\Vacante;
use App\Http\Requests\SolicitudRequest;
use Illuminate\Support\Facades\Auth;

class SolicitudController extends Controller
{


    public function index()
    {
        $solicitudes = SolicitudView::where('estado', 0)->get();
        return view('solicitudes.index', compact('solicitudes'));
    }


    public function create()
    {
        $vacantes = Vacante::get();
        $bolsaTrabajo = BolsaTrabajo::where('activo', 1)->get();
        return view('solicitudes.create', compact('vacantes', 'bolsaTrabajo'));
    }


    public function store(SolicitudRequest $request)
    {
        $solicitud = SolicitudRrhh::create([
            'nombre' => mb_strtoupper($request->nombre,'utf-8'),
            'apellido_paterno' => mb_strtoupper($request->apellido_paterno,'utf-8'),
            'apellido_materno' => mb_strtoupper($request->apellido_materno,'utf-8'),
            'correo' => $request->email,
            'telefono' => $request->telefono,
            'id_reclutador' => Auth::user()->id,
            'estado' => 0,
            'id_bolsa_trabajo' => $request->bolsaTrabajo,
            'id_vacante' => $request->vacante,
        ]);
        return redirect()->route('solicitud.index')->with('mensaje', 'Solicitud creada correctamente');
//              if ($solicitud) {
//                SolicitudRrhhLog::create([
//                    'id' => $solicitud->id,
//                    'nombre' => $solicitud->nombre,
//                    'apellido_paterno' => $solicitud->apellido_paterno,
//                    'apellido_materno' => $solicitud->apellido_materno,
//                    'correo' => $solicitud->correo,
//                    'telefono' => $solicitud->telefono,
//                    'id_reclutador' => $solicitud->id_reclutador,
//                    'estado' => $solicitud->estado,
//                    'id_bolsa_trabajo' => $solicitud->id_bolsa_trabajo,
//                    'id_sesion' => 1,
//                ]);
//                return redirect()->route('solicitud.index')->with('mensaje','Solicitud creada correctamente');
//              }

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellido_paterno' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellido_materno' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|unique:solicitudes_rrhh,correo,' . request()->get("id"),
            'telefono' => 'required|digits_between:8,10',

        ], [
            'nombre.required' => 'El nombre es obligatorio',
            'nombre.regex' => 'El nombre contiene un formato incorrecto',
            'apellido_paterno.required' => 'El apellido paterno es obligatorio',
            'apellido_paterno.regex' => 'El apellido paterno contiene un formato incorrecto',
            'apellido_materno.required' => 'El apellido materno es obligatorio',
            'apellido_materno.regex' => 'El apellido materno contiene un formato incorrecto',
            'email.required' => 'El correo es obligatorio',
            'email.unique' => 'El correo que intenta registrar ya existe',
            'telefono.required' => 'El telefono es obligatorio',
            'telefono.digits_between' => 'El telefono contiene un formato incorrecto',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $solicitud = SolicitudRrhh::find($request->id);
        $solicitud->update([
            'nombre' => strtoupper($request->nombre),
            'apellido_paterno' => strtoupper($request->apellido_paterno),
            'apellido_materno' => strtoupper($request->apellido_materno),
            'telefono' => $request->telefono,
            'correo' => $request->email,
        ]);
        if ($solicitud) {
            SolicitudRrhhLog::create([
                'id' => $solicitud->id,
                'nombre' => $solicitud->nombre,
                'apellido_paterno' => $solicitud->apellido_paterno,
                'apellido_materno' => $solicitud->apellido_materno,
                'correo' => $solicitud->correo,
                'telefono' => $solicitud->telefono,
                'id_reclutador' => $solicitud->id_reclutador,
                'estado' => $solicitud->id_reclutador,
                'id_bolsa_trabajo' => $solicitud->id_bolsa_trabajo,
                'fecha_cita' => $solicitud->fecha_cita,
                'id_sesion' => 1,
            ]);

        }
        return redirect()->route('solicitud.index')->with('mensaje', 'Solicitud actualizada correctamente');
    }


    public function destroy(Request $request)
    {

    }

    public function agendarCita(Request $request)
    {
        $id = $request->id;
        $fecha = $request->fecha;
        $hora = $request->hora;
        $fecha_cita = $fecha . " " . $hora . ":00";
        $data = SolicitudRrhh::find($id);
        $data->fecha_cita = $fecha_cita;
        $saveCita = $data->save();
        if ($saveCita) {
            SolicitudRrhhLog::create([
                'id' => $data->id,
                'nombre' => $data->nombre,
                'apellido_paterno' => $data->apellido_paterno,
                'apellido_materno' => $data->apellido_materno,
                'correo' => $data->correo,
                'telefono' => $data->telefono,
                'id_reclutador' => $data->id_reclutador,
                'estado' => $data->estado,
                'id_bolsa_trabajo' => $data->id_bolsa_trabajo,
                'fecha_cita' => $fecha_cita,
                'id_sesion' => 1,
            ]);
        }
        return redirect()->route('solicitud.index')->with('mensaje', 'Cita agendada');
    }

}
