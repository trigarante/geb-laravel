<?php

namespace App\Http\Controllers\Rh;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rh\RhModels\Candidato;
use App\Rh\RhLogs\CandidatoLog;
use App\Catalogos\CatalogosModels\Competencia;
use App\Rh\RhViews\CandidatoView;
use App\Rh\RhModels\PreCandidato;
use App\Rh\RhModels\Seguimiento;
use App\Rh\RhModels\Empleado;
use App\Rh\RhLogs\PreCandidatoLog;
use App\Catalogos\CatalogosModels\EstadoMotivoRh;
use Illuminate\Support\Facades\Validator;

class CandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $motivos = EstadoMotivoRh::where('activo', 1)->where('candidato',1)->get();
        $competencias = Competencia::where('activo', 1)->get();
        $candidatos = CandidatoView::where('capacitacion',0)->where('empleado',0)->where('id_estado',100)->get();
        return view('candidatos.index', compact('candidatos', 'competencias', 'motivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'competencia' => 'required',
            'calificacion_examen' => 'required|integer',
            'comentarios' => 'required'
        ], [
            'competencia.required' => 'Es requerida una competencia',
            'calificacion_examen.required' => 'Es requerida una calificacion',
            'calificacion_examen.integer' => 'La calificacion debe ser numerica',
            'comentarios.required' => 'Es necesario un comentario   '
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $candidato = Candidato::where('id_precandidato', $request->id)->count();
        if ($candidato == 1) {
            return redirect()->route('preCandidatos.index')->with('error', 'Error el candidato ya tiene seguimiento');
        }
        $save = Candidato::create([
            'id_calificacion_competencias' => $request->competencia,
            'calificacion_examen' => $request->calificacion_examen,
            'id_estado' => 100,
            'id_precandidato' => $request->id,
            'comentarios' => strtoupper($request->comentarios),
        ]);
        if ($save) {
            return redirect()->route('candidato.index')->with('mensaje', 'Candidato creado correctamente');
        } else {
            return redirect()->route('candidato.index')->with('error', 'Error al crear Candidato');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidatos = CandidatoView::where('id_precandidato', $id)->first();
        return view('candidatos.show')->with('candidatos', $candidatos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'competencia' => 'required',
            'calificacion_examen' => 'required|integer',
            'comentarios' => 'required'
        ], [
            'competencia.required' => 'Es requerida una competencia',
            'calificacion_examen.required' => 'Es requerida una calificacion',
            'calificacion_examen.integer' => 'La calificacion debe ser numerica',
            'comentarios.required' => 'Es necesario un comentario   '
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $candidato = Candidato::find($request->id);
        $update = $candidato->update([
            'id_calificacion_competencias' => $request->competencia,
            'calificacion_examen' => $request->calificacion_examen,
            'comentarios' =>strtoupper($request->comentarios),
        ]);
        if ($update) {
            return redirect()->route('candidato.index')->with('mensaje', 'Candidato actualizado correctamente');
        }
        else {
            return redirect()->route('candidato.index')->with('error','Error al actualizar candidato');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $candidato = Candidato::find($request->id);
        $candidato->update([
            'id_estado' => $request->motivo_baja,
        ]);

        $preCandidato = PreCandidato::find($candidato->id_precandidato);
        if ($preCandidato) {
            $preCandidato->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('candidato.index')->with('mensaje', 'Candidato actualizado correctamente');
        }
        $seguimiento = Seguimiento::where('id_candidato', $candidato->id)->first();
        if ($seguimiento) {
            $seguimiento->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('candidato.index')->with('mensaje', 'Candidato actualizado correctamente');
        }
        $empleado = Empleado::where('id_seguimiento', $seguimiento->id)->first();
        if ($empleado) {
            $empleado->update([
                'id_estado' => $request->motivo_baja,
            ]);
        } else {
            return redirect()->route('candidato.index')->with('mensaje', 'Candidato actualizado correctamente');
        }
        return redirect()->route('candidato.index')->with('mensaje', 'Candidato actualizado correctamente');
    }

    public function view($id)
    {
        $candidatos = CandidatoView::find($id);
        return view('candidatos.show', compact('candidatos'));
    }
}
