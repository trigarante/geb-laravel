<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seguimiento;
use App\Seguimientolog;
use App\SeguimientoView;
use App\Candidato;
use App\CandidatoLog;

class SeguimientoController extends Controller
{
    
    public function index()
    {
        $seguimientos = SeguimientoView::get();
        return view('seguimiento.index',compact('seguimientos'));
    }

   
    public function create()
    {
        
    }

   
    public function store(Request $request)
    {
          $save=Seguimiento::create([
            'calificacion_rollplay' => $request->calificacion_rollplay,
            'id_candidato' => $request->id,
            'comentarios' => $request->comentarios,
            'id_estado' => 0,
                    ]);
             if ($save) {
                SeguimientoLog::create([
                    'id' => $save->id,
                    'fecha_ingreso' => $save->fecha_ingreso,
            'calificacion_rollplay' => $save->calificacion_rollplay,
            'id_candidato' => $save->id,
            'comentarios' => $save->comentarios,
            'id_estado' => $save->id_estado,
            'id_sesion' => 0,
                    ]);
                $data = Candidato::find($request->id);
                $data->id_estado = 1;
                $data->save();
                CandidatoLog::create([
            'id' => $data->id,
            'id_calificacion_competencias' => $data->id_calificacion_competencias,
            'calificacion_examen' => $data->calificacion_examen,
            'id_estado' => $data->id_estado,
            'id_precandidato' => $data->id_precandidato,
            'comentarios' => $data->comentarios,
            'fecha_ingreso' => $data->fecha_ingreso,
            'id_sesion' => 0,
            ]);
            }
            return redirect()->route('seguimiento.index');
    }

        public function show($id)
    {
        $seguimiento = SeguimientoView::where('id_candidato',$id)->first();
        return view('seguimiento.show',compact('seguimiento')); 

    }

    
    public function edit($id)
    {
        
    }

    
    public function update(Request $request)
    {
        $seguimiento = Seguimiento::find($request->id);
        $seguimiento->update([
            'calificacion_rollplay' => $request->calificacion,
            'comentarios' => $request->comentarios,
        ]);
        Seguimientolog::create([
                    'id' => $seguimiento->id,
                    'fecha_ingreso' => $seguimiento->fecha_ingreso,
            'calificacion_rollplay' => $seguimiento->calificacion_rollplay,
            'id_candidato' => $seguimiento->id,
            'comentarios' => $seguimiento->comentarios,
            'id_estado' => $seguimiento->id_estado,
            'id_sesion' => 0,
                    ]);
        return redirect()->route('seguimiento.index');
    }

    
    public function destroy($id)
    {
        
    }
}
