<?php

namespace App\Http\Controllers;

use App\EstacionesLineas;
use Illuminate\Http\Request;
use App\EstadoCivil;
use App\Escolaridad;
use App\Pais;
use App\Competencia;
use App\EstadoEscolaridad;
use App\PreCandidato;
use App\PreCandidatoLog;
use App\PreCandidatoView;
use App\EstadoMotivoRh;
use App\ImagenUsuario;
use App\ImagenUsuarioLog;
use App\Http\Requests\PreCandidatoRequest;
use App\SolicitudRrhh;
use App\SolicitudRrhhLog;
use App\MedioTransporte;

class PreCandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $motivos = EstadoMotivoRh::where('activo', 1)->get();
        $competencias = Competencia::where('activo', 1)->get();
        $precandidatos = PreCandidatoView::get();
        return view('preCandidatos.index', compact('precandidatos', 'competencias', 'motivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $estadoCivil = EstadoCivil::where('activo', 1)->get();
        $escolaridad = Escolaridad::where('activo', 1)->get();
        $estadoEscolar = EstadoEscolaridad::where('activo', 1)->get();
        $paises = Pais::where('activo', 1)->get();
        $solicitudDatos = SolicitudRrhh::find($id);
        $medioTransporte = MedioTransporte::get();
        return view('preCandidatos.create')->with(['id' => $id, 'estadoCivil' => $estadoCivil, 'escolaridad' => $escolaridad,
            'estadoEscolar' => $estadoEscolar, 'solicitudDatos' => $solicitudDatos, 'paises' => $paises, 'medios' => $medioTransporte]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PreCandidatoRequest $request)
    {
        $preCandidato = PreCandidato::where('id_solicitud_rrhh', $request->idSolicitud)->count();
        if ($preCandidato == 1) {
            return redirect('/solicitud')->with('error', 'Error solictud asignada a otro precandidato');
        }
        $guardarImagen = ImagenUsuario::create([
            'url' => 'images/1',
        ]);
        if ($guardarImagen) {
            ImagenUsuarioLog::create([
                'id' => $guardarImagen->id,
                'url' => $guardarImagen->url,
                'id_sesion' => 0,
            ]);
        }
        $idImagen = $guardarImagen->id;
        $save = PreCandidato::create([
            'id_solicitud_rrhh' => $request->idSolicitud,
            'id_imagen' => 1,
            'nombre' => $request->nombre,
            'apellido_paterno' => $request->apellido_paterno,
            'apellido_materno' => $request->apellido_materno,
            'fecha_nacimiento' => $request->fechaNacimiento,
            'email' => $request->email,
            'genero' => $request->genero,
            'id_estado_civil' => $request->estadoCivil,
            'id_escolaridad' => $request->escolaridad,
            'id_etdo_escolaridad' => $request->estadoEscolar,
            'id_pais' => $request->pais,
            'cp' => $request->cp,
            'colonia' => $request->colonia,
            'calle' => $request->calle,
            'numero_exterior' => $request->numeroExt,
            'numero_interior' => $request->numeroInt,
            'telefono_fijo' => $request->telFijo,
            'telefono_movil' => $request->telMovil,
            'id_estado' => 0,
            'id_estado_motivo' => 1,
            'tiempo_traslado' => $request->tiempoTraslado,
            'medio_traslado' => $request->medioTraslado,
            'metro_cercano' => $request->metroCercano,
        ]);
        if ($save) {
            PreCandidatoLog::create([
                'id' => $save->id,
                'id_solicitud_rrhh' => $save->id_solicitud_rrhh,
                'id_imagen' => $save->id_imagen,
                'nombre' => $save->nombre,
                'apellido_paterno' => $save->apellido_paterno,
                'apellido_materno' => $save->apellido_materno,
                'fecha_nacimiento' => $save->fecha_nacimiento,
                'email' => $save->email,
                'genero' => $save->genero,
                'id_estado_civil' => $save->id_estado_civil,
                'id_escolaridad' => $save->id_escolaridad,
                'id_etdo_escolaridad' => $save->id_etdo_escolaridad,
                'id_pais' => $save->id_pais,
                'cp' => $save->cp,
                'colonia' => $save->colonia,
                'calle' => $save->calle,
                'numero_exterior' => $save->numero_exterior,
                'numero_interior' => $save->numero_interior,
                'telefono_fijo' => $save->telefono_fijo,
                'telefono_movil' => $save->telefono_movil,
                'id_estado' => $save->id_estado,
                'id_estado_motivo' => $save->id_estado_motivo,
                'tiempo_traslado' => $save->tiempo_traslado,
                'medio_traslado' => $save->medio_traslado,
                'metro_cercano' => $save->metro_cercano,
                'id_sesion' => 0,
            ]);
            $data = SolicitudRrhh::find($request->idSolicitud);
            $data->estado = 1;
            $data->save();
            SolicitudRrhhLog::create([
                'id' => $data->id,
                'nombre' => $data->nombre,
                'apellido_paterno' => $data->apellido_paterno,
                'apellido_materno' => $data->apellido_materno,
                'correo' => $data->correo,
                'telefono' => $data->telefono,
                'id_reclutador' => 0,
                'estado' => $data->estado,
                'id_bolsa_trabajo' => $data->id_bolsa_trabajo,
                'fecha_cita' => $data->fecha_cita,
                'id_sesion' => 1,
            ]);
        }
        return redirect()->route('preCandidatos.index')->with('mensaje', 'Pre candidato creado correctamente');
    }


    public function show($id)
    {
        $precandidato = PreCandidatoView::where('id_solicitud_rrhh', $id)->first();
        return view('preCandidatos.show')->with('precandidato', $precandidato);
    }

    public function edit($id)
    {
        $estadoCivil = EstadoCivil::where('activo', 1)->get();
        $escolaridad = Escolaridad::where('activo', 1)->get();
        $estadoEscolar = EstadoEscolaridad::where('activo', 1)->get();
        $precandidatos = PreCandidatoView::find($id);
        $paises = Pais::where('activo',1)->get();
        return view('preCandidatos.update')->with(['precandidatos' => $precandidatos, 'estadoCivil' => $estadoCivil,
            'escolaridad' => $escolaridad, 'estadoEscolar' => $estadoEscolar, 'paises' => $paises]);
    }

    public function update(Request $request)
    {
        $this->validate(request(), [
            'email' => ['required', 'email', 'unique:precandidato,email,' . request()->get("id")],
            'nombre' => ['required', 'regex:/^[\pL\s\-]+$/u'],
            'apellido_paterno' => ['required', 'regex:/^[\pL\s\-]+$/u'],
            'apellido_materno' => ['required', 'regex:/^[\pL\s\-]+$/u'],
            'fechaNacimiento' => ['required'],
            'estadoCivil' => ['required'],
            'escolaridad' => ['required'],
            'estadoEscolar' => ['required'],
            'pais' => ['required'],
            'cp' => ['required', 'digits:5'],
            'colonia' => ['required', 'string'],
            'calle' => ['required', 'string'],
            'telFijo' => ['required', 'digits:8'],
            'telMovil' => ['required', 'digits:10'],
            'tiempoTraslado' => ['required', 'integer'],
            'medioTraslado' => ['required', 'regex:/^[\pL\s\-]+$/u'],
            'metroCercano' => ['required', 'regex:/^[\pL\s\-]+$/u'],
        ],
            [
                'nombre.required' => 'El campo nombre es requerido',
                'nombre.regex' => 'El formato del nombre es incorrecto',
                'apellido_paterno.required' => 'El campo apellido paterno es requerido',
                'apellido_paterno.regex' => 'El formato del apellido paterno es incorrecto',
                'apellido_materno.required' => 'El campo apellido materno es requerido',
                'apellido_materno.regex' => 'El formato del apellido materno es incorrecto',
                'email.required' => 'El campo correo es requerido',
                'email.unique' => 'Este correo ya se encuentra registrado',
                'email.email' => 'El campo correo no tiene un formato valido',
                'estadoCivil.required' => 'El campo estado civil es requerido',
                'escolaridad.required' => 'El campo escolaridad es requerido',
                'estadoEscolar.required' => 'El campo estado de escolaridad requerido',
                'cp.required' => 'El codigo postal es requerido',
                'cp.digits' => 'El codigo postal debe ser de 5 digitos',
                'colonia.required' => 'El campo colonia es requerido',
                'calle.required' => 'El campo calle es requerido',
                'telFijo.required' => 'El campo telefono fijo es requerido',
                'telFijo.digits' => 'Telefono fijo debe ser numerico de 8 digitos',
                'telMovil.required' => 'El campo telefono movil es requerido',
                'telMovil.digits' => 'Telefono movil debe ser numerico de 10 digitos',
                'tiempoTraslado.required' => 'El campo tiempo de traslado es requerido',
                'tiempoTraslado.integer' => 'El campo tiempo de traslado debe ser numerico',
                'medioTraslado.required' => 'El campo medio de traslado es requerido',
                'metroCercano.required' => 'El campo metro mas cercano es requerido',
                'fechaNacimiento.required' => 'La fecha de nacimiento es requerida',
                'pais.required' => 'El pais es requerido',
            ]);
        $precandidato = PreCandidato::find($request->id);
        $update = $precandidato->update([
            'nombre' => $request->nombre,
            'apellido_paterno' => $request->apellido_paterno,
            'apellido_materno' => $request->apellido_materno,
            'fecha_nacimiento' => $request->fechaNacimiento,
            'email' => $request->email,
            'genero' => $request->genero,
            'id_estado_civil' => $request->estadoCivil,
            'id_escolaridad' => $request->escolaridad,
            'id_etdo_escolaridad' => $request->estadoEscolar,
            'cp' => $request->cp,
            'colonia' => $request->colonia,
            'calle' => $request->calle,
            'numero_exterior' => $request->numeroExt,
            'numero_interior' => $request->numeroInt,
            'telefono_fijo' => $request->telFijo,
            'telefono_movil' => $request->telMovil,
            'tiempo_traslado' => $request->tiempoTraslado,
            'medio_traslado' => $request->medioTraslado,
            'metro_cercano' => $request->metroCercano,
        ]);
        if ($update) {
            PreCandidatoLog::create([
                'id' => $precandidato->id,
                'id_solicitud_rrhh' => $precandidato->id_solicitud_rrhh,
                'id_imagen' => $precandidato->id_imagen,
                'nombre' => $precandidato->nombre,
                'apellido_paterno' => $precandidato->apellido_paterno,
                'apellido_materno' => $precandidato->apellido_materno,
                'fecha_nacimiento' => $precandidato->fecha_nacimiento,
                'email' => $precandidato->email,
                'genero' => $precandidato->genero,
                'id_estado_civil' => $precandidato->id_estado_civil,
                'id_escolaridad' => $precandidato->id_escolaridad,
                'id_etdo_escolaridad' => $precandidato->id_etdo_escolaridad,
                'id_pais' => $precandidato->id_pais,
                'cp' => $precandidato->cp,
                'colonia' => $precandidato->colonia,
                'calle' => $precandidato->calle,
                'numero_exterior' => $precandidato->numero_exterior,
                'numero_interior' => $precandidato->numero_interior,
                'telefono_fijo' => $precandidato->telefono_fijo,
                'telefono_movil' => $precandidato->telefono_movil,
                'id_estado' => $precandidato->id_estado,
                'id_estado_motivo' => $precandidato->id_estado_motivo,
                'tiempo_traslado' => $precandidato->tiempo_traslado,
                'medio_traslado' => $precandidato->medio_traslado,
                'metro_cercano' => $precandidato->metro_cercano,
                'id_sesion' => 0,
            ]);
            $data = SolicitudRrhh::find($request->id_solicitud);
            $data->nombre = $request->nombre;
            $data->apellido_paterno = $request->apellido_paterno;
            $data->apellido_materno = $request->apellido_materno;
            $data->correo = $request->email;
            $data->save();

            SolicitudRrhhLog::create([
                'id' => $data->id,
                'nombre' => $data->nombre,
                'apellido_paterno' => $data->apellido_paterno,
                'apellido_materno' => $data->apellido_materno,
                'correo' => $data->correo,
                'telefono' => $data->telefono,
                'id_reclutador' => 0,
                'estado' => $data->estado,
                'id_bolsa_trabajo' => $data->id_bolsa_trabajo,
                'fecha_cita' => $data->fecha_cita,
                'id_sesion' => 1,
            ]);
        }

        return redirect()->route('preCandidatos.index')->with('mensaje', 'Pre candidato actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $precandidato = PreCandidato::find($request->id);
        $precandidato->update([
            'id_estado_motivo' => $request->motivo_baja,
        ]);
        PreCandidatoLog::create([
            'id' => $precandidato->id,
            'id_solicitud_rrhh' => $precandidato->id_solicitud_rrhh,
            'id_imagen' => $precandidato->id_imagen,
            'nombre' => $precandidato->nombre,
            'apellido_paterno' => $precandidato->apellido_paterno,
            'apellido_materno' => $precandidato->apellido_materno,
            'fecha_nacimiento' => $precandidato->fecha_nacimiento,
            'email' => $precandidato->email,
            'genero' => $precandidato->genero,
            'id_estado_civil' => $precandidato->id_estado_civil,
            'id_escolaridad' => $precandidato->id_escolaridad,
            'id_etdo_escolaridad' => $precandidato->id_etdo_escolaridad,
            'id_pais' => $precandidato->id_pais,
            'cp' => $precandidato->cp,
            'colonia' => $precandidato->colonia,
            'calle' => $precandidato->calle,
            'numero_exterior' => $precandidato->numero_exterior,
            'numero_interior' => $precandidato->numero_interior,
            'telefono_fijo' => $precandidato->telefono_fijo,
            'telefono_movil' => $precandidato->telefono_movil,
            'id_estado' => $precandidato->id_estado,
            'id_estado_motivo' => $precandidato->id_estado_motivo,
            'tiempo_traslado' => $precandidato->tiempo_traslado,
            'medio_traslado' => $precandidato->medio_traslado,
            'metro_cercano' => $precandidato->metro_cercano,
            'id_sesion' => 0,
        ]);
        return redirect()->route('preCandidatos.index')->with('mensaje', 'Pre candidato actualizado correctamente');
    }

    public function foto()
    {
        $imagenCodificada = file_get_contents("php://input"); //Obtener la imagen
        if (strlen($imagenCodificada) <= 0) exit("No se recibió ninguna imagen");
        //La imagen traerá al inicio data:image/png;base64, cosa que debemos remover
        $imagenCodificadaLimpia = str_replace("data:image/png;base64,", "", urldecode($imagenCodificada));

        //Venía en base64 pero sólo la codificamos así para que viajara por la red, ahora la decodificamos y
        //todo el contenido lo guardamos en un archivo
        $imagenDecodificada = base64_decode($imagenCodificadaLimpia);

        //Calcular un nombre único
        $nombreImagenGuardada = "images/foto_" . uniqid() . ".png";

        //Escribir el archivo
        file_put_contents($nombreImagenGuardada, $imagenDecodificada);

        //Terminar y regresar el nombre de la foto
        exit($nombreImagenGuardada);
    }

    public function searchMetro(Request $request)
    {
        $data = EstacionesLineas::select("estacion as name","id as value")->whereRaw('lower(estacion) LIKE "%'.$request->input('query').'%"')->get();

        return response()->json($data);


    }
}
