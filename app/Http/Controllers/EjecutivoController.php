<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ejecutivo;
use App\Rh\RhViews\EmpleadoView;
use App\Extensiones;
use App\Asignaciones;

class EjecutivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = EmpleadoView::paginate(10);
        return view('ejecutivo.index',compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id,$idCampana)
    {
        $extension = Extensiones::where('activo',1)
                                ->where('id_campana',$idCampana)->get();
        $correo = Asignaciones::where('activo',1)
                              ->where('id_campana',$idCampana)->get();
        $empleado = EmpleadoView::find($id);
        return view('ejecutivo.create')->with(['id'=>$id,'empleado'=>$empleado,'extension'=>$extension,'correo'=>$correo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solicitud=  Ejecutivo::create([
            'id_correo' => $request->correo,
            'id_extension' => $request->extension,
           ]);

            // $idSolicitud = $solicitud->id;
            // if ($solicitud) {
            //     $solicitudLog = SolicitudRrhhLog::create([
            //         'id' => $idSolicitud,
            //         'nombre' => $request->nombre,
            //         'apellido_paterno' => $request->apellido_paterno,
            //         'apellido_materno' => $request->apellido_materno,
            //         'correo' => $request->email,
            //         'telefono' => $request->telefono,
            //         'id_reclutador' => 0,
            //         'estado' => 0,
            //         'id_bolsa_trabajo' => $request->bolsaTrabajo,
            //         'id_sesion' => 1,
            //     ]);
                return redirect()->route('solicitud.index')->with('mensaje','Solicitud creada correctamente');
              //}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function nuevaExtension(Request $request)
    {
        $extension = Extensiones::find($request->extNC);
        $extension->update([
            'id_estado' => 2 
        ]);

        return redirect()->route('ejecutivo.create,[$request->id,$request->idCampana]')->with('mensaje','Extensión creada');
    }
}
