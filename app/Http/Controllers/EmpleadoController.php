<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Puesto;
use App\TipoPuesto;
use App\Area;
use App\Empresa;
use App\Seguimiento;
use App\Empleado;
use App\Empleadolog;
use App\EmpleadoView;
use App\Banco;
use App\Http\Requests\EmpleadoRequest;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = EmpleadoView::get();
        return view('empleado.index', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $bancos = Banco::where('activo', 1)->get();
        $puestos = Puesto::where('activo', 1)->get();
        $tipoPuestos = TipoPuesto::where('activo', 1)->get();
        $areas = Area::where('activo', 1)->get();
        $empresas = Empresa::where('activo', 1)->get();
        $id = $request->id;
        return view('empleado.create')->with(['puestos' => $puestos, 'id' => $id, 'tipoPuestos' => $tipoPuestos, 'areas' => $areas,
            'empresas' => $empresas, 'bancos' => $bancos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(EmpleadoRequest $request)
    {
        $empleado = Empleado::where('id_seguimiento', $request->idSeguimiento)->count();
        if ($empleado == 1) {
            return redirect()->route('empleado.index')->with('error', 'Error el empleado ya tiene seguimiento');
        }
        $save = Empleado::create([
            'id_seguimiento' => $request->idSeguimiento,
            'fecha_alta_imss' => $request->fechaImss,
            'id_puesto' => $request->puesto,
            'id_puesto_tipo' => $request->tipoPuesto,
            'puesto_detalle' => $request->detallePuesto,
            'id_area' => $request->area,
            'sueldo_diario' => $request->sueldoDiario,
            'sueldo_mensual' => $request->sueldoMensual,
            'cta_clabe' => $request->cuentaClabe,
            'banco' => $request->banco,
            'id_status' => 1,
            'recontratable' => 1,
            'id_empresa' => $request->empresa,
            'fecha_asigancion' => $request->fechaAsignacion,
            'kpi' => $request->kpi,
        ]);
        EmpleadoLog::create([
            'id' => $save->id,
            'id_seguimiento' => $save->id_seguimiento,
            'fecha_ingreso' => $save->fecha_ingreso,
            'fecha_alta_imss' => $save->fecha_alta_imss,
            'id_puesto' => $save->id_puesto,
            'id_puesto_tipo' => $save->id_puesto_tipo,
            'puesto_detalle' => $save->puesto_detalle,
            'id_area' => $save->id_area,
            'sueldo_diario' => $save->sueldo_diario,
            'sueldo_mensual' => $save->sueldo_mensual,
            'cta_clabe' => $save->cta_clabe,
            'banco' => $save->banco,
            'id_status' => $save->id_status,
            'recontratable' => $save->recontratable,
            'id_empresa' => $save->id_empresa,
            'fecha_asigancion' => $save->fecha_asigancion,
            'id_sesion' => 0
        ]);
        if ($save) {
            $data = Seguimiento::find($request->idSeguimiento);
            $data->id_estado = 1;
            $data->save();

            Seguimientolog::create([
                'id' => $data->id,
                'fecha_ingreso' => $data->fecha_ingreso,
                'calificacion_rollplay' => $data->calificacion_rollplay,
                'id_candidato' => $data->id,
                'comentarios' => $data->comentarios,
                'id_estado' => $data->id_estado,
                'id_sesion' => 0,
            ]);
        }
        return redirect()->route('empleado.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empleado = EmpleadoView::where('id_seguimiento', $id)->first();
        return view('empleado.show', compact('empleado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado = EmpleadoView::find($id);
        $bancos = Banco::where('activo', 1)->get();
        $puestos = Puesto::where('activo', 1)->get();
        $tipoPuestos = TipoPuesto::where('activo', 1)->get();
        $areas = Area::where('activo', 1)->get();
        $empresas = Empresa::where('activo', 1)->get();
        return view('empleado.update')->with(['puestos' => $puestos, 'id' => $id, 'tipoPuestos' => $tipoPuestos, 'areas' => $areas,
            'empresas' => $empresas, 'bancos' => $bancos, 'empleado' => $empleado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $empleado = Empleado::find($request->id);
        $empleado->update([
            'fecha_alta_imss' => $request->fechaImss,
            'puesto_detalle' => $request->detallePuesto,
            'id_puesto' => $request->puesto,
            'id_puesto_tipo' => $request->tipoPuesto,
            'id_area' => $request->area,
            'sueldo_diario' => $request->sueldoDiario,
            'sueldo_mensual' => $request->sueldoMensual,
            'fecha_cambio_sueldo' => $request->fechaCambioSueldo,
            'cta_clabe' => $request->cuentaClabe,
            'id_empresa' => $request->empresa
        ]);
        EmpleadoLog::create([
            'id' => $empleado->id,
            'id_seguimiento' => $empleado->id_seguimiento,
            'fecha_ingreso' => $empleado->fecha_ingreso,
            'fecha_alta_imss' => $empleado->fecha_alta_imss,
            'id_puesto' => $empleado->id_puesto,
            'id_puesto_tipo' => $empleado->id_puesto_tipo,
            'puesto_detalle' => $empleado->puesto_detalle,
            'id_area' => $empleado->id_area,
            'sueldo_diario' => $empleado->sueldo_diario,
            'sueldo_mensual' => $empleado->sueldo_mensual,
            'cta_clabe' => $empleado->cta_clabe,
            'banco' => $empleado->banco,
            'id_status' => $empleado->id_status,
            'recontratable' => $empleado->recontratable,
            'id_empresa' => $empleado->id_empresa,
            'fecha_asigancion' => $empleado->fecha_asigancion,
            'id_sesion' => 0
        ]);
        return redirect()->route('empleado.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
