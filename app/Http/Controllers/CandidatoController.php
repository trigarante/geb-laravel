<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidato;
use App\CandidatoLog;
use App\Competencia;
use App\CandidatoView;
use App\PreCandidato;
use App\PreCandidatoLog;
use App\EstadoMotivoRh;

class CandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $motivos = EstadoMotivoRh::where('activo', 1)->get();
        $competencias = Competencia::where('activo', 1)->get();
        $candidatos = CandidatoView::get();
        return view('candidatos.index', compact('candidatos', 'competencias', 'motivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidato = Candidato::where('id_precandidato', $request->id)->count();
        if ($candidato == 1) {
            return redirect()->route('preCandidatos.index')->with('error', 'Error el precandidato ya tiene seguimiento');
        }
        $save = Candidato::create([
            'id_calificacion_competencias' => $request->competencia,
            'calificacion_examen' => $request->calificacion_examen,
            'id_estado' => 0,
            'id_precandidato' => $request->id,
            'comentarios' => $request->comentarios,
        ]);
        if ($save) {
            CandidatoLog::create([
                'id' => $save->id,
                'id_calificacion_competencias' => $save->id_calificacion_competencias,
                'calificacion_examen' => $save->calificacion_examen,
                'id_estado' => $save->id_estado,
                'id_precandidato' => $save->id_precandidato,
                'comentarios' => $save->comentarios,
                'id_sesion' => 0,
            ]);
            $data = PreCandidato::find($request->id);
            $data->id_estado = 1;
            $data->save();
            PreCandidatoLog::create([
                'id' => $data->id,
                'id_solicitud_rrhh' => $data->id_solicitud_rrhh,
                'id_imagen' => $data->id_imagen,
                'nombre' => $data->nombre,
                'apellido_paterno' => $data->apellido_paterno,
                'apellido_materno' => $data->apellido_materno,
                'fecha_nacimiento' => $data->fecha_nacimiento,
                'email' => $data->email,
                'genero' => $data->genero,
                'id_estado_civil' => $data->id_estado_civil,
                'id_escolaridad' => $data->id_escolaridad,
                'id_etdo_escolaridad' => $data->id_etdo_escolaridad,
                'id_pais' => $data->id_pais,
                'cp' => $data->cp,
                'colonia' => $data->colonia,
                'calle' => $data->calle,
                'numero_exterior' => $data->numero_exterior,
                'numero_interior' => $data->numero_interior,
                'telefono_fijo' => $data->telefono_fijo,
                'telefono_movil' => $data->telefono_movil,
                'id_estado' => $data->id_estado,
                'id_estado_motivo' => $data->id_estado_motivo,
                'tiempo_traslado' => $data->tiempo_traslado,
                'medio_traslado' => $data->medio_traslado,
                'metro_cercano' => $data->metro_cercano,
                'id_sesion' => 0,
            ]);
            return redirect()->route('candidato.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidatos = CandidatoView::where('id_precandidato', $id)->first();
        return view('candidatos.show')->with('candidatos', $candidatos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $candidato = Candidato::find($request->id);
        $candidato->update([
            'id_calificacion_competencias' => $request->competencia,
            'calificacion_examen' => $request->calificacion_examen,
            'comentarios' => $request->comentarios,
        ]);
        if ($candidato) {
            CandidatoLog::create([
                'id' => $candidato->id,
                'id_calificacion_competencias' => $candidato->id_calificacion_competencias,
                'calificacion_examen' => $candidato->calificacion_examen,
                'id_estado' => $candidato->id_estado,
                'id_precandidato' => $candidato->id_precandidato,
                'comentarios' => $candidato->comentarios,
                'id_sesion' => 0,
            ]);
        }

        return redirect()->route('candidato.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
