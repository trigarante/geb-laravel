<?php

namespace App\Http\Controllers;

use App\Ejecutivo;
use Illuminate\Http\Request;
use App\SolicitudRrhh;
use App\SolicitudRrhhLog;
use App\BolsaTrabajo;
use App\Http\Requests\SolicitudRequest;

class SolicitudController extends UserController
{


    public function index()
    {
         $solicitudes = SolicitudRrhh::get();
         return view('solicitudes.index')->with('solicitudes',$solicitudes);
    }

    
    public function create()
    {
        $bolsaTrabajo = BolsaTrabajo::where('activo',1)->get();
        return view('solicitudes.create')->with('bolsaTrabajo',$bolsaTrabajo);
    }

    
    public function store(SolicitudRequest $request)
    {
              $solicitud=  SolicitudRrhh::create([
            'nombre' => $request->nombre,
            'apellido_paterno' => $request->apellido_paterno,
            'apellido_materno' => $request->apellido_materno,
            'correo' => $request->email,
            'telefono' => $request->telefono,
            'id_reclutador' => 0,
            'estado' => 0,
            'id_bolsa_trabajo' => $request->bolsaTrabajo,
                    ]);
              $idSolicitud = $solicitud->id;
              if ($solicitud) {
                $solicitudLog = SolicitudRrhhLog::create([
                    'id' => $idSolicitud,
                    'nombre' => $request->nombre,
                    'apellido_paterno' => $request->apellido_paterno,
                    'apellido_materno' => $request->apellido_materno,
                    'correo' => $request->email,
                    'telefono' => $request->telefono,
                    'id_reclutador' => 0,
                    'estado' => 0,
                    'id_bolsa_trabajo' => $request->bolsaTrabajo,
                    'id_sesion' => 1,
                ]);
                return redirect()->route('solicitud.index')->with('mensaje','Solicitud creada correctamente');
              }
            
    }

    
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    
    public function update(Request $request)
    {    
         $this->validate(request(), [
            'email' => ['required', 'email', 'unique:solicitudes_rrhh,correo,'.request()->get("id")]
        ]);
        $solicitud = SolicitudRrhh::find($request->id);
        $solicitud->update([
            'nombre' => $request->nombre,
            'apellido_paterno' => $request->apellido_paterno,
            'apellido_materno' => $request->apellido_materno,
            'telefono' => $request->telefono,
            'correo' => $request->email,
        ]);
        if ($solicitud) {
            SolicitudRrhhLog::create([
                'id' => $solicitud->id,
                'nombre' => $solicitud->nombre,
                'apellido_paterno' => $solicitud->apellido_paterno,
                'apellido_materno' => $solicitud->apellido_materno,
                'correo' => $solicitud->correo,
                'telefono' => $solicitud->telefono,
                'id_reclutador' => 0,
                'estado' => 0,
                'id_bolsa_trabajo' => $solicitud->id_bolsa_trabajo,
                'fecha_cita' => $solicitud->fecha_cita,
                'id_sesion' => 1,
            ]);
            
        }
        return redirect()->route('solicitud.index')->with('mensaje','Solicitud actualizada correctamente');
    }

    
    public function destroy(Request $request)
    {
        
    }

    public function agendarCita(Request $request)
    {
        $id=$request->id;
        $fecha = $request->fecha;
        $hora = $request->hora;
        $fecha_cita = $fecha." ".$hora.":00";
        $data = SolicitudRrhh::find($id);
        $data->fecha_cita = $fecha_cita;
        $saveCita = $data->save ();
        if ($saveCita) {
            SolicitudRrhhLog::create([
                'id' => $data->id,
                'nombre' => $data->nombre,
                'apellido_paterno' => $data->apellido_paterno,
                'apellido_materno' => $data->apellido_materno,
                'correo' => $data->correo,
                'telefono' => $data->telefono,
                'id_reclutador' => 0,
                'estado' => 0,
                'id_bolsa_trabajo' => $data->id_bolsa_trabajo,
                'fecha_cita' => $fecha_cita,
                'id_sesion' => 1,
            ]);
        }
        return redirect()->route('solicitud.index')->with('mensaje','Cita agendada');
    }

}
