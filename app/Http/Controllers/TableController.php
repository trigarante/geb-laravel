<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PreCandidatoView;
use Datatables;

class TableController extends Controller
{
    public function index()
    {
    	return view('pruebaTable');
    }

    public function getdata()
    {
    	$precandidatos = PreCandidatoView::get();
    	return Datatables::of($precandidatos)->make(true);
    }
}
