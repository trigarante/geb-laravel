<?php

namespace App\Http\Middleware;

use Closure;

class InventarioRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->grupo_usuarios->inventarios == 0) {
            return redirect('/');
        }

        return $next($request);
    }
}
