<?php

namespace App\Http\Middleware;

use Closure;

class UsuariosRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->grupo_usuarios->usuarios != 1) {
            return redirect('/');
        }

        return $next($request);
    }
}
