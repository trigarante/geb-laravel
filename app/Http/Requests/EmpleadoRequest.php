<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'puesto' => 'required',
            'tipoPuesto' => 'required',
            'subArea' => 'required',
            'empresa' => 'required',
            'banco' => 'required',
            'cuentaClabe' => 'required|regex:/\d{18}/',
            'sueldoDiario' => 'integer',
            'sueldoMensual' => 'integer',
            'curp' => 'regex:/^[\w\s-]*$/|required',
            'rfc' =>  'regex:/^[\w\s-]*$/|required',
        ];
    }

    public function messages()
{
    return [
        'puesto.required' => 'El campo puesto es requerido',
        'tipoPuesto.required' => 'El campo tipo de puesto es requerido',
        'subArea.required' => 'El campo sub area es requerido',
        'empresa.required' => 'El campo empresa es requerido',
        'banco.required' => 'El campo banco es requerido',
        'cuentaClabe.required' => 'El campo cuenta CLABE es requerido',
        'cuentaClabe.regex' => 'La cuenta CLABE no tiene un formato correcto',
        'sueldoDiario.integer' => 'El sueldo diario tiene que ser un valor numerico',
        'sueldoMensual.integer' => 'El sueldo mensual tiene que ser un valor numerico',
        'curp.regex' => 'El formato del curp es incorrecto',
        'curp.required' => 'El campo Curp es requerido',
        'rfc.regex' => 'El formato del rfc es incorrecto',
        'rfc.required' => 'El campo rfc es requerido',


    ];
}
}
