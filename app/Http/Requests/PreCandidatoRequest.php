<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PreCandidatoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellido_paterno' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellido_materno' => 'required|regex:/^[\pL\s\-]+$/u',
            'fechaNacimiento' => 'required',
            'email' => 'required|unique:precandidato,email|email',
            'estadoCivil' => 'required',
            'escolaridad' => 'required',
            'estadoEscolar' => 'required',
            'pais' => 'required',
            'cp' => 'required|digits:5',
            'colonia' => 'required',
            'calle' => 'regex:/^[\w\s-]*$/|required',
            'telFijo' => 'nullable|digits:8',
            'telMovil' => 'required|digits:10',
            'tiempoTraslado' => 'required|integer',
            'medioTraslado' => 'required',
            'metroCercano' => 'required',
            'numeroExt' => 'required',
            'genero' => 'required'
        ];
    }

        public function messages()
{
    return [
        'nombre.required' => 'El campo nombre es requerido',
        'nombre.regex' => 'El formato del nombre es incorrecto',
        'apellido_paterno.required' => 'El campo apellido paterno es requerido',
        'apellido_paterno.regex' => 'El formato del apellido paterno es incorrecto',
        'apellido_materno.required' => 'El campo apellido materno es requerido',
        'apellido_materno.regex' => 'El formato del apellido materno es incorrecto',
        'email.required' => 'El campo correo es requerido',
        'email.unique' => 'Este correo ya se encuentra registrado',
        'email.email' => 'El campo correo no tiene un formato valido',
        'estadoCivil.required' => 'El campo estado civil es requerido',
        'escolaridad.required' => 'El campo escolaridad es requerido',
        'estadoEscolar.required' => 'El campo estado de escolaridad requerido',
        'cp.required' => 'El codigo postal es requerido',
        'colonia.required' => 'El campo colonia es requerido',
        'calle.required' => 'El campo calle es requerido',
        'calle.regex' => 'El formato del campo calle es incorrecto',
        'telFijo.digits' => 'Telefono fijo debe ser numerico de 8 digitos',
         'telMovil.required' => 'El campo telefono movil es requerido',
        'telMovil.digits' => 'Telefono movil debe ser numerico de 10 digitos',
        'tiempoTraslado.required' => 'El campo tiempo de traslado es requerido',
        'tiempoTraslado.integer' => 'El campo tiempo de traslado debe ser numerico',
        'medioTraslado.required' => 'El campo medio de traslado es requerido',
        'metroCercano.required' => 'El campo metro mas cercano es requerido',
        'fechaNacimiento.required' => 'La fecha de nacimiento es requerida',
        'pais.required' => 'El pais es requerido',
        'numeroExt.required' => 'El numero exterior es requerido',
        'genero.required' => 'El genero es requerido'
            ];
}
}
