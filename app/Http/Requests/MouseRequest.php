<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest; 

class MouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'num_folio' => 'regex:/^[\w-]*$/|min:6|max:22|required',
            'marca' => 'required',
        ];
            
    }

    public function messages()
    {
        return [
            'num_folio.regex' => 'El numero de folio solo permite caracteres alfanumericos',
            'num_folio.required' => 'Se requiere un numero  de folio',
            'num_folio.min' => 'El numero de folio debe tener como minimo 6 caracteres',
            'num_folio.max' => 'El numero de folio debe tener como maximo 22 caracteres',
            'marca.required' => 'Se requiere de la marca',
            
        ];
    }
}
