<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CpuRequest extends FormRequest 
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero_serie' => 'regex:/^[\w-]*$/|min:6|max:22|required',
            'porcesador' => 'required',
            'hdd' => 'required',
            'ram' => 'required',
            'marca' => 'required',
            'modelo' => 'required',
        ];
            
    }
    public function messages()
    {
        return [
              'numero_serie.regex' => 'El numero de serie solo permite caracteres alfanumericos',
              'numero_serie.min' => ' El numero de serie debe tener como minimo 6 caracteres',
              'numero_serie.max' => ' El numero de serie debe tener como maximo 10 caracteres',
              'numero_serie.required' => 'Se requiere un numero  de serie',
              'porcesador.required' => 'Se requiere porcesador',
              'hdd.required' => 'Se requiere  HDD',
              'ram.required' => 'Se requiere  RAM',
              'marca.required' => 'Se requiere la marca',
              'modelo.required' => 'Se requiere el modélo',
            
        ];
    }
}
