<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TabletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'num_folio' => 'regex:/^[\w-]*$/|min:6|max:22|required',
            'marca' => 'required',
            'telefono' => 'required|digits_between:8,10',
        ];
            
    }

    public function messages()
    {
        return [
            'num_folio.regex' => 'El numero de folio solo permite caracteres alfanumericos',
            'num_folio.required' => 'Se requiere un numero  de folio',
            'num_folio.min' => 'El numero de folio debe tener como minimo 6 caracteres',
            'num_folio.max' => 'El numero de folio debe tener como maximo 22 caracteres',
            'marca.required' => 'Se requiere de la marca',
            'telefono.required' => 'Se requiere un telefono',
            'telefono.digits_between' => 'El telefono tiene que ser numerico entre 8 y 10 digitos',
            
        ];
    }
}
