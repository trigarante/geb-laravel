<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MonitorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
           'num_serie' => 'regex:/^[\w-]*$/|min:6|max:22|required',
            'marca' => 'required',

        ];
    }

    public function messages()
{
    return [ 
        'num_serie.regex' => 'El numero de serie solo permite caracteres alfanumericos',
        'num_serie.required' => 'se requiere un numero  de serie',
        'num_serie.min' => 'el numero de serie debe tener como minimo 6 caracteres',
        'num_serie.max' => 'el numero de serie debe tener como maximo 22 caracteres',
        'marca.required' => 'Se requiere marca'

     
    ];
}
}
