<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Description of InventarioRequest
 *
 * @author cesar
 */
class InventarioRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
//        return [
//            'nombre' => 'required|string',
//            'apellido_paterno' => 'required|string',
//            'apellido_materno' => 'required|string',
//            'email' => 'required|unique:solicitudes_rrhh,correo|email',
//            'telefono' => 'required|digits_between:8,10',
//        ];
    }

    public function messages() {
//        return [
//            'nombre.required' => 'el campo nombre es requerido',
//            'apellido_paterno.required' => 'el campo apellido paterno es requerido',
//            'apellido_materno.required' => 'el campo apellido materno es requerido',
//            'nombre.string' => 'el campo nombre no puede contener numeros',
//            'apellido_paterno.string' => 'el campo apellido paterno no puede contener numeros',
//            'apellido_materno.string' => 'el campo apellido materno no puede contener numeros',
//            'email.required' => 'el campo correo es requerido',
//            'email.unique' => 'este correo ya se encuentra registrado',
//            'email.email' => 'el campo correo no tiene un formato valido',
//            'telefono.required' => 'el campo telefono es requerido',
//            'telefono.digits_between' => 'el campo telefono tiene que ser numerico de 8 o 10 digitos',
//        ];
    }

}
