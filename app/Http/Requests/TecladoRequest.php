<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TecladoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */ 
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'num_folio' => 'regex:/^[\w-]*$/|min:6|max:22|required',
        ];
            
    }

    public function messages()
    {
        return [
            'num_folio.regex' => 'El numero de folio solo permite caracteres alfanumericos',
            'num_folio.required' => 'Se requiere un numero  de folio',
            'num_folio.min' => 'el numero de folio debe tener como minimo 6 caracteres',
            'num_folio.max' => 'el numero de folio debe tener como maximo 22 caracteres',
            
        ];
    }
}
