<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SolicitudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellido_paterno' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellido_materno' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|unique:solicitudes_rrhh,correo|email',
            'telefono' => 'required|digits_between:8,10',
            'bolsaTrabajo' => 'required',
            'vacante' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Se requiere un nombre',
            'apellido_paterno.required' => 'Se requiere un apellido paterno',
            'apellido_materno.required' => 'Se requiere un apellido materno',
            'nombre.regex' => 'El formato del nombre es invalido',
            'apellido_paterno.regex' => 'El formato del apellido paterno es invalido',
            'apellido_materno.regex' => 'El formato del apellido materno es invalido',
            'email.required' => 'Se requiere un correo',
            'email.unique' => 'El correo ya se encuentra registrado',
            'email.email' => 'El correo contiene un formato invalido',
            'telefono.required' => 'Se requiere un telefono',
            'telefono.digits_between' => 'El telefono tiene que ser numerico entre 8 y 10 digitos',
            'bolsaTrabajo.required' => 'Especifique una bolsa de trabajo',
            'vacante.required' => 'Se requiere una vacante'
        ];
    }
}
