<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InventarioEmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'puesto' => 'required',
            'tipoPuesto' => 'required',
            'area' => 'required',
            'empresa' => 'required',
            'banco' => 'required',
            'cuentaClabe' => 'regex:/\d{18}/',
        ];
    }
}
