<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPermiso extends Model
{
    protected  $table = 'tipo_permiso';
    public $timestamps = false;
    protected $fillable = [
        'id','tipo','descripcion',
    ];
}
