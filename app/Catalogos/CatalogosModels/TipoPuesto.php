<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class TipoPuesto extends Model
{
         protected $table= 'tipo_puesto';

    public $timestamps = false;

    protected $fillable = ['id','nombre','activo'];
}
