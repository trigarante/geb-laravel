<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class CpSepomex extends Model
{
protected $table = 'cp_sepomex';

public $timestamps = false;
}
