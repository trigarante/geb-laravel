<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'paises';

    public $timestamps = false;

    protected $fillable = ['id','nombre','activo'];
}
