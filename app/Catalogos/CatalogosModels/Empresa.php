<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
         protected $table= 'empresa';

    public $timestamps = false;

    protected $fillable = ['id','nombre','id_grupo','descripcion','activo'];
}
