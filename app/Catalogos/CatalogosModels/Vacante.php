<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class Vacante extends Model
{
protected $table = 'vacantes';

public $timestamps = false;

protected $fillable = ['nombre','id_subarea','id_puesto','id_tipo_puesto','id_tipo'];

    public function subarea()
    {
        return $this->belongsTo(SubArea::class,'id_subarea');
    }

    public function puesto()
    {
        return $this->belongsTo(Puesto::class,'id_puesto');
    }

    public function tipo_puesto()
    {
        return $this->belongsTo(TipoPuesto::class,'id_tipo_puesto');
    }

    public function tipo_usuario()
    {
        return $this->belongsTo(TipoUsuario::class,'id_tipo');
    }
}
