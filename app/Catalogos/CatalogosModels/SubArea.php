<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class SubArea extends Model
{
    protected $table = 'subarea';
    public $timestamps = false;
    protected $fillable = ['id_area','subarea','descripcion'];
}
