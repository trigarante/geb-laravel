<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class Competencia extends Model
{
    protected $table= 'competencias';

    public $timestamps = false;

    protected $fillable = ['id','escala','estadio','descripcion','activo'];
}
