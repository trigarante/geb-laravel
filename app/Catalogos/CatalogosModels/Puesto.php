<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class Puesto extends Model
{
     protected $table= 'puesto';

    public $timestamps = false;

    protected $fillable = ['id','nombre','activo'];
}
