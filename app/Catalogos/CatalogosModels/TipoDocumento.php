<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;


class TipoDocumento extends Model
{
protected $table = 'tipo_documentos';

public $timestamps = false;

protected $fillable = ['nombre','id_pais','activo','descripcion'];

    public function paises()
    {
        return $this->belongsTo(Pais::class,'id_pais');
    }
}
