<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    protected $table= 'tipo_usuario';
}
