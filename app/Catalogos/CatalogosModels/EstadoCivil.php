<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    protected $table = 'estado_civil';

    public $timestamps = false;

    protected $fillable = ['id','descripcion','activo'];
}
