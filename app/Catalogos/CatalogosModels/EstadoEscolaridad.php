<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class EstadoEscolaridad extends Model
{
    protected $table = 'estado_escolaridad';

    public $timestamps = false;

    protected $fillable = ['id','estado','activo'];
}
