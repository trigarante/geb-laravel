<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class EstadoMotivoRh extends Model
{
         protected $table= 'estado_motivo_rh';

    public $timestamps = false;

    protected $fillable = ['id','estado','id_estado_rh','activo','pre-candidato','candidato','seguimiento','empleado'];
}
