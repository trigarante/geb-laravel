<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class BolsaTrabajo extends Model
{
    protected $table = 'bolsa_trabajo';

    public $timestamps = false;

    protected $fillable = ['id','nombre','tipo','activo'];
}
