<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class Escolaridad extends Model
{
    protected $table = 'escolaridad';

    public $timestamps = false;

    protected $fillable = ['id','nivel','activo'];
}
