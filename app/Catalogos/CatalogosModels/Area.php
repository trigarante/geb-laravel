<?php

namespace App\Catalogos\CatalogosModels;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';

    public $timestamps = false;

    protected $fillable = ['id', 'nombre', 'descripcion', 'id_empresa', 'activo'];
}
