<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class EstadoEscolaridadLog extends Model
{
	protected $connection = 'logs';

    protected $table = 'estado_escolaridad';

    public $timestamps = false;

    protected $fillable = ['id','estado','activo'];
}
