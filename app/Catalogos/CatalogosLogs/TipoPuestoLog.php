<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class TipoPuestoLog extends Model
{

	protected $connection = 'logs';

      protected $table= 'tipo_puesto';

    public $timestamps = false;

    protected $fillable = ['id','nombre','activo','id_sesion','fecha_cambio'];
}
