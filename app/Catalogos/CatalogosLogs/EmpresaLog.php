<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class EmpresaLog extends Model
{
	protected $connection = 'logs';
	
    protected $table= 'empresa';

    public $timestamps = false;

    protected $fillable = ['id','nombre','id_grupo','descripcion','activo','id_sesion', 'fecha_cambio'];
}
