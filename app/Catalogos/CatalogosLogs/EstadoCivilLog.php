<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class EstadoCivilLog extends Model
{
	protected $connected = 'logs';
     protected $table = 'estado_civil';

    public $timestamps = false;

    protected $fillable = ['id','descripcion','activo','id_sesion','fecha_cambio'];
}
