<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class EscolaridadLog extends Model
{
	protected $connection = 'logs';
	
    protected $table = 'escolaridad';

    public $timestamps = false;

    protected $fillable = ['id','nivel','activo','id_sesion', 'fecha_cambio'];
}
