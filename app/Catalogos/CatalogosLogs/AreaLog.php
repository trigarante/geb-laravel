<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class AreaLog extends Model
{
    protected $connection = 'logs';

    protected $table = 'area';

    public $timestamps = false;

    protected $fillable = ['id', 'nombre', 'descripcion', 'id_empresa', 'activo', 'id_sesion', 'fecha_cambio'];
}
