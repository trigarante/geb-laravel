<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class BolsaTrabajoLog extends Model
{
	protected $connection = 'logs';

    protected $table = 'bolsa_trabajo';

    public $timestamps = false;

    protected $fillable = ['id','nombre','tipo','activo','id_sesion','fecha_cambio'];
}
