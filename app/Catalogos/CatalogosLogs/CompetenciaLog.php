<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class CompetenciaLog extends Model
{
	protected $connection = 'logs';
	
     protected $table= 'competencias';

    public $timestamps = false;

    protected $fillable = ['id','escala','estadio','descripcion','activo','id_sesion', 'fecha_cambio'];
}
