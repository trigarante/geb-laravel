<?php

namespace App\Catalogos\CatalogosLogs;

use Illuminate\Database\Eloquent\Model;

class EstadoMotivoRhLog extends Model
{
	protected $connection = 'logs';
	
     protected $table= 'estado_motivo_rh';

    public $timestamps = false;

    protected $fillable = ['id','estado','id_estado_rh','activo','id_sesion','fecha_cambio'];
}
