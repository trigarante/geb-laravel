<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonitorView extends Model {

    protected $table = 'monitor_view';
    public $timestamps = false;
    protected $fillable = ['id', 'num_serie', 'marca', 'dimensiones', 'id_estado','estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega','entregado', 'activo'];

}