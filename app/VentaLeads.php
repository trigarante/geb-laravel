<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaLeads extends Model
{
    protected $table= 'venta_leads';

    public $timestamps = false;

    protected $fillable = ['id','id_aseguradora','id_lead_aseguradora','fecha_envio','id_solicitud','id_cotizacion_a','status',
    'fecha_venta','num_poliza','referencia','prima_expedida'];
}
