<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extensiones extends Model
{
    protected $table= 'extenciones';

    protected $fillable = ['id','id_campana','descrip','activo'];
}
