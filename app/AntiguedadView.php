<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntiguedadView extends Model
{
    protected $table= 'antiguedad_view';

    public $timestamps = false;

    protected $fillable = ['nombre', 'apellido_paterno', 'apellido_materno', 'area', 'campana', 'fecha_actual', 'fecha_ingreso', 'antiguedad_dias', 'antiguedad_semanas', 'antiguedad_meses', 'antiguedad_anios'];
}
