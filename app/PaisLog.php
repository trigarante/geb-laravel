<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaisLog extends Model
{
	protected $connection = 'logs';

    protected $table = 'paises';

    public $timestamps = false;

    protected $fillable = ['id','nombre','activo','id_sesion','fecha_cambio'];
}
