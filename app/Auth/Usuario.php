<?php

namespace App\Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\GrupoUsuarios;

class Usuario extends Authenticatable
{
    use Notifiable;

protected $table = 'usuarios';

public $timestamps = false;

protected $fillable = ['usuario','password','id_grupo','id_tipo','token'];

    public function getRememberToken()
    {
        return $this->token;
    }
    public function setRememberToken($value)
    {
        $this->token = $value;
    }
    public function getRememberTokenName()
    {
        return 'token';
    }

    public function grupo_usuarios()
    {
        return $this->belongsTo(GrupoUsuarios::class,'id_grupo');
    }
}
