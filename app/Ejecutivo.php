<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Ejecutivo extends Authenticatable
{
    use Notifiable;

    protected $table = 'ejecutivo';
    public $timestamps = false;
    protected $fillable = ['id','usuario','password','token'];

    public function getRememberToken()
    {
        return $this->token;
    }
    public function setRememberToken($value)
    {
        $this->token = $value;
    }
    public function getRememberTokenName()
    {
        return 'token';
    }

    public function grupo_usuarios()
    {
        return $this->belongsTo(GrupoUsuarios::class,'id_grupo');
    }


}
