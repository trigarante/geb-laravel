<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoUsuarios extends Model
{
    protected $table = 'grupo_usuarios';
    public $timestamps = false;
    protected $fillable = ['id_subarea', 'id_puesto','id_tipo_puesto','descripcion', 'grupo', 'vacantes', 'precandidatos', 'candidato', 'seguimiento',
        'empleados', 'usuarios', 'inventarios','contactos','clientes','agentes_externos','polizas','pagos','autorizacion',
        'verificaciones','ec','carga_polizas','contabilidad','nomina','transferencias','transferencias_masivas',
        'siniestros','penalizaciones','catalogos'];
}
