<?php

namespace App\Asignaciones\AsignacionesModels;

use App\GrupoUsuarios;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Ejecutivo extends Authenticatable
{
    use Notifiable;

    protected $table = 'ejecutivo';
    public $timestamps = false;
    protected $fillable = ['id_empleado','estado','fecha_baja','id_correo','id_extension','id_protocolo','id_perfil_marcacion','id_campana',
        'id_usuario'];
}
