<?php

namespace App\Asignaciones\AsignacionesModels;

use Illuminate\Database\Eloquent\Model;

class Extension extends Model
{
    protected $table = 'extenciones';
    public $timestamps = false;
    protected $fillable = [
        'id','id_campaña','id_estado'
    ];
}
