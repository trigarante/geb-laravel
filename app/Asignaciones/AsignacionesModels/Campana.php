<?php

namespace App\Asignaciones\AsignacionesModels;

use Illuminate\Database\Eloquent\Model;

class Campana extends Model
{
    protected $table = 'campana';
    public $timestamps = false;
    protected $fillable = [
        'nombre','activo'
    ];
}
