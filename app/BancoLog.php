<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BancoLog extends Model
{
	protected $connection = 'logs';
	
    protected $table= 'bancos';

    public $timestamps = false;

    protected $fillable = ['id','nombre','activo','id_sesion','fecha_cambio'];
}
