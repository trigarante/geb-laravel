<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpleadosSinUser extends Model
{
    protected $table= 'empleado_sinuser';

    public $timestamps = false;

    protected $fillable = ['id','id_area','id_campana','nombre','apellido_paterno','apellido_materno','area',
    					   'campana'];
}
