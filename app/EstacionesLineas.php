<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstacionesLineas extends Model
{
    protected $table = 'estaciones_lineas';
    public $timestamps = false;
    protected $fillable = ['id','id_transporte','estacion','lineas'];
}
