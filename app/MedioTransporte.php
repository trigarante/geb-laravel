<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedioTransporte extends Model
{
    protected $table = 'medio_transporte';
    public $timestamps = false;
    protected $fillable = ['id','medio','tipo'];
}
