<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monitor extends Model {

    protected $table = 'monitor';
    public $timestamps = false;
    protected $fillable = ['id', 'num_serie', 'marca', 'dimensiones', 'id_estado', 'fecha_recepcion', 'fecha_salida', 'id_entrega', 'activo'];

}
