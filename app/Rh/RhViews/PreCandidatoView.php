<?php

namespace App\Rh\RhViews;

use Illuminate\Database\Eloquent\Model;

class PreCandidatoView extends Model
{
    protected $table = 'precandidato_view';
}
