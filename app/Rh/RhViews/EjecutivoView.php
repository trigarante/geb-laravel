<?php

namespace App\Rh\RhViews;

use Illuminate\Database\Eloquent\Model;

class EjecutivoView extends Model
{
protected $table = 'ejecutivo_view';

public $timestamps = false;

protected $fillable = [];
}
