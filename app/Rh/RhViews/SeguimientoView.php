<?php

namespace App\Rh\RhViews;

use Illuminate\Database\Eloquent\Model;

class SeguimientoView extends Model
{
    protected $table = 'seguimiento_view';
}
