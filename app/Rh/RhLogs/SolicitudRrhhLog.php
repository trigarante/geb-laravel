<?php

namespace App\Rh\RhLogs;

use Illuminate\Database\Eloquent\Model;

class SolicitudRrhhLog extends Model
{
    protected $connection = 'logs';

    protected $table = 'solicitudes_rrhh';

    public $timestamps = false;

    protected $fillable = [
    	'id','nombre', 'apellido_paterno', 'apellido_materno','telefono','correo','fecha','id_reclutador','estado',
        'fecha_cita','id_bolsa_trabajo','id_sesion','fecha_cambio',
    ];
}
