<?php

namespace App\Rh\RhLogs;

use Illuminate\Database\Eloquent\Model;

class SeguimientoLog extends Model
{
	protected $connection = 'logs';

    protected $table= 'seguimiento';

    public $timestamps = false;

    protected $fillable = ['id','fecha_ingreso','calificacion_rollplay','id_candidato','comentarios',
    'id_estado','id_sesion','fecha_cambio'];
}
