<?php

namespace App\Rh\RhLogs;

use Illuminate\Database\Eloquent\Model;

class EmpleadoLog extends Model
{
    protected $connection ='logs';
    protected $table= 'empleado';

    public $timestamps = false;

    protected $fillable = ['id','id_seguimiento','fecha_ingreso','fecha_alta_imss','puesto_detalle','id_puesto',
    'id_puesto_tipo', 'id_area','sueldo_diario','sueldo_mensual','fecha_cambio_sueldo','cta_clabe','banco','id_status',
    'fecha_baja','recontratable', 'id_empresa','fecha_asigancion','id_sesion','fecha_cambio'];
}
