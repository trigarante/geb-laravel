<?php

namespace App\Rh\RhLogs;

use Illuminate\Database\Eloquent\Model;

class CandidatoLog extends Model
{
	protected $connection = 'logs';

    protected $table= 'candidato';

    public $timestamps = false;

    protected $fillable = ['id','id_calificacion_competencias','calificacion_examen','id_estado','id_precandidato',
    'comentarios', 'fecha_ingreso', 'id_sesion', 'fecha_cambio'];
}
