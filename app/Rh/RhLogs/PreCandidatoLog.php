<?php

namespace App\Rh\RhLogs;

use Illuminate\Database\Eloquent\Model;

class PreCandidatoLog extends Model
{
     protected $connection = 'logs';

       protected $table = 'precandidato';

    public $timestamps = false;

    protected $fillable = [
        'id','id_solicitud_rrhh','id_imagen','nombre','apellido_paterno','apellido_materno','fecha_nacimiento','email','genero',
        'id_estado_civil','id_escolaridad','id_etdo_escolaridad','id_pais','cp','colonia','calle','numero_exterior',
        'numero_interior','telefono_fijo','telefono_movil','id_estado','id_estado_motivo','fecha_creacion','fecha_baja',
        'tiempo_traslado','medio_traslado','metro_cercano','id_sesion','fecha_cambio'
    ];
}
