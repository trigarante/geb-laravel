<?php

namespace App\Rh\RhModels;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    protected $table = 'asignaciones';
    public $timestamps = false;
    protected $fillable = [
        'correo','id_campaña','activo'
    ];
}
