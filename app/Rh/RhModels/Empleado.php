<?php

namespace App\Rh\RhModels;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
         protected $table= 'empleado';

    public $timestamps = false;

    protected $fillable = ['id','id_candidato','fecha_ingreso','fecha_alta_imss','puesto_detalle','id_puesto',
    'id_puesto_tipo', 'id_subarea','sueldo_diario','sueldo_mensual','fecha_cambio_sueldo','cta_clabe','banco','id_estado',
    'fecha_baja','recontratable', 'id_empresa','fecha_asigancion','kpi','tipo_usuario'];
}
