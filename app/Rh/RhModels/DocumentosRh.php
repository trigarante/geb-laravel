<?php

namespace App\Rh\RhModels;

use Illuminate\Database\Eloquent\Model;

class DocumentosRh extends Model
{
protected $table = 'documentos_rrhh';

public $timestamps = false;

protected $fillable = ['documento','id_documento','id_empleado'];
}
