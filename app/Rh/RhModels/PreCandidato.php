<?php

namespace App\Rh\RhModels;

use App\EstacionesLineas;
use Illuminate\Database\Eloquent\Model;

class PreCandidato extends Model
{
    protected $table = 'precandidato';

    public $timestamps = false;
    protected $fillable = [
        'id','id_solicitud_rrhh','id_imagen','nombre','apellido_paterno','apellido_materno','fecha_nacimiento','email','genero',
        'id_estado_civil','id_escolaridad','id_etdo_escolaridad','id_pais','cp','colonia','calle','numero_exterior',
        'numero_interior','telefono_fijo','telefono_movil','id_estado','fecha_creacion','fecha_baja',
        'tiempo_traslado','id_medio_traslado','id_estacion','estado','del_mun','ciudad'
    ];
}
