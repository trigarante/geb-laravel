<?php

namespace App\Rh\RhModels;

use Illuminate\Database\Eloquent\Model;

class SolicitudRrhh extends Model
{
    protected $table = 'solicitudes_rrhh';

    public $timestamps = false;

    protected $fillable = [
        'nombre', 'apellido_paterno', 'apellido_materno','telefono','correo','fecha','id_reclutador','estado',
        'fecha_cita','id_bolsa_trabajo','id_vacante',
    ];
}
