<?php

namespace App\Rh\RhModels;

use Illuminate\Database\Eloquent\Model;

class Capacitacion extends Model
{
    protected $table = 'capacitacion';

    public $timestamps = false;

    protected $fillable = ['id_candidato','calificacion','comentarios','id_capacitador'];
}
