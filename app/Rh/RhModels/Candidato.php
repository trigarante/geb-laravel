<?php

namespace App\Rh\RhModels;

use Illuminate\Database\Eloquent\Model;

class Candidato extends Model
{
        protected $table= 'candidato';

    public $timestamps = false;

    protected $fillable = ['id','id_calificacion_competencias','calificacion_examen','id_estado','id_precandidato','comentarios',
    'fecha_ingreso'];
}
