<?php

namespace App\Rh\RhModels;

use Illuminate\Database\Eloquent\Model;

class Administrativo extends Model
{
    protected $table = 'administrativo';
    public $timestamps = false;
    protected $fillable = [
        'id_empleado','estado','id_area','fecha_baja','id_correo','id_usuario'
    ];

    public function asignaciones()
    {
        return $this->belongsTo(Asignacion::class,'id_correo');
    }
}
