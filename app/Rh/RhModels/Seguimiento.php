<?php

namespace App\Rh\RhModels;

use Illuminate\Database\Eloquent\Model;

class Seguimiento extends Model
{
     protected $table= 'seguimiento';

    public $timestamps = false;

    protected $fillable = ['calificacion_rollplay','id_capacitacion','comentarios',
    'id_estado','id_coach'];
}
