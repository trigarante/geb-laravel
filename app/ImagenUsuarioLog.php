<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenUsuarioLog extends Model
{
	protected $connection = 'logs';

     protected $table = 'imagen_usuario';

    public $timestamps = false;

    protected $fillable = ['id','url', 'id_sesion', 'fecha_cambio'];
}
