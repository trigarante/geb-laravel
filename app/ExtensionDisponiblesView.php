<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtensionDisponiblesView extends Model
{
 protected $table= 'ExtensionDisponiblesView';

    public $timestamps = false;

    protected $fillable = ['id','id_campana','descript','activo'];
   
}
 

