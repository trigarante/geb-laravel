<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeguimientoReporteView extends Model
{
    protected $table = 'seguimiento_view';
    public $timestamps = false;

   protected $fillable = ['id', 'id_precandidato', 'nombre', 'apellido_paterno', 'apellido_materno', 
       'email', 'telefono_movil', 'calificacion_rollplay', 'comentarios', 'fecha_ingreso', 'id_estado', 
       'id_candidato', 'estado', 'siguiente'];
}
