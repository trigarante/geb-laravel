<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table = 'grupo';
    public $timestamps = false;
    protected $fillable = ['descripcion','id_pais','nombre','activo'];
    protected $hidden = ['id'];
}
