<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubArea extends Model
{
    protected $table = 'subarea';
    public $timestamps = false;
    protected $fillable = ['id','id_area','subarea','descripcion'];
}
