<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImssView extends Model
{
    protected $table= 'imss_view';

    public $timestamps = false;

    protected $fillable = ['id', 'id_precandidato', 'id_seguimiento', 'nombre', 'apellido_paterno', 'apellido_materno', 
        'email', 'telefono_movil', 'colonia', 'fecha_nacimiento', 'Edad', 'cp', 'estado', 'del_mun', 'ciudad', 'escolaridad', 'fecha_alta_imss', 
        'fecha_ingreso', 'puesto_detalle', 'id_puesto', 'puesto', 'id_puesto_tipo', 'puesto_tipo', 'id_area', 'area', 'sueldo_diario', 
        'sueldo_mensual', 'fecha_cambio_sueldo', 'cta_clabe', 'banco', 'id_estado', 'fecha_baja', 'recontratable', 'id_empresa', 
        'id_campana', 'campana', 'empresa', 'estado_rh'];
}
