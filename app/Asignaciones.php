<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignaciones extends Model
{
   protected $table = 'asignaciones';
       public $timestamps = false;

    protected $fillable = ['id','correo','id_campana','activo'];
    
}
