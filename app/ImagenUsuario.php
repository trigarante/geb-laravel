<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenUsuario extends Model
{
    protected $table = 'imagen_usuario';

    public $timestamps = false;

    protected $fillable = ['id','url'];
}
