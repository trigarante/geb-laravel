<?php
use Illuminate\Support\Facades\Input;


// login
Route::get('/', function () {
    return view('login');
})->name('/');

// vista principal
Route::group(['middleware' => ['auth']], function () {
    Route::get('/principal', function () {
        return view('vista');
    })->name('principal');
});

// solicitud
Route::group(['middleware' => ['auth', 'solicitud']], function () {
    Route::resource('solicitud', 'Rh\SolicitudController');
    Route::post('/agendarCita', 'Rh\SolicitudController@agendarCita')->name('solicitud.agendarCita');
});

// preCandidatos
Route::group(['middleware' => ['auth', 'precandidato']], function () {
    Route::get('/preCandidatos/create/{id}', 'Rh\PreCandidatoController@create')->name('preCandidatos.create');
    Route::post('/preCandidatos/store', 'Rh\PreCandidatoController@store')->name('preCandidatos.store');
    Route::get('/preCandidatos', 'Rh\PreCandidatoController@index')->name('preCandidatos.index');
    Route::get('/preCandidatos/edit/{id}', 'Rh\PreCandidatoController@edit')->name('preCandidatos.edit');
    Route::post('/preCandidatos/update', 'Rh\PreCandidatoController@update')->name('preCandidatos.update');
    Route::post('/preCandidatos/delete', 'Rh\PreCandidatoController@destroy')->name('preCandidatos.destroy');
    Route::get('/preCandidatos/show/{id}', 'Rh\PreCandidatoController@show')->name('preCandidatos.show');
    Route::get('/preCandidatos/view/{id}', 'Rh\PreCandidatoController@view')->name('preCandidatos.view');
    Route::post('/preCandidatos/foto', 'Rh\PreCandidatoController@foto')->name('preCandidatos.foto');
    Route::get('/preCandidatos/searchMetro', 'Rh\PreCandidatoController@searchMetro')->name('searchMetro');
});

// candidato
Route::group(['middleware' => ['auth', 'candidato']], function () {
    Route::get('/candidato', 'Rh\CandidatoController@index')->name('candidato.index');
    Route::post('/candidato/store', 'Rh\CandidatoController@store')->name('candidato.store');
    Route::get('/candidato/show/{id}', 'Rh\CandidatoController@show')->name('candidato.show');
    Route::get('/candidato/view/{id}', 'Rh\CandidatoController@view')->name('candidato.view');
    Route::post('/candidato/update', 'Rh\CandidatoController@update')->name('candidato.update');
    Route::post('/candidato/destroy', 'Rh\CandidatoController@destroy')->name('candidato.destroy');
});  


// seguimiento
Route::group(['middleware' => ['auth', 'seguimiento']], function () {
    Route::post('/seguimiento/store', 'Rh\SeguimientoController@store')->name('seguimiento.store');
    Route::post('/seguimiento/update', 'Rh\SeguimientoController@update')->name('seguimiento.update');
    Route::get('/seguimiento', 'Rh\SeguimientoController@index')->name('seguimiento.index');
    Route::get('/seguimiento/show/{id}', 'Rh\SeguimientoController@show')->name('seguimiento.show');
    Route::get('/seguimiento/view/{id}', 'Rh\SeguimientoController@view')->name('seguimiento.view');
    Route::post('/seguimiento/destroy', 'Rh\SeguimientoController@destroy')->name('seguimiento.destroy');
});

// empleado
Route::group(['middleware' => ['auth', 'empleado']], function () {
    Route::get('/empleado', 'Rh\EmpleadoController@index')->name('empleado.index');
    Route::get('/empleado/create/{id}', 'Rh\EmpleadoController@create')->name('empleado.create');
    Route::get('/empleado/edit/{id}', 'Rh\EmpleadoController@edit')->name('empleado.edit');
    Route::post('/empleado/store', 'Rh\EmpleadoController@store')->name('empleado.store');
    Route::post('/empleado/update', 'Rh\EmpleadoController@update')->name('empleado.update');
    Route::post('/empleado/destroy', 'Rh\EmpleadoController@destroy')->name('empleado.destroy');
    Route::get('/empleado/show/{id}', 'Rh\EmpleadoController@show')->name('empleado.show');
    Route::get('/empleado/view/{id}', 'Rh\EmpleadoController@view')->name('empleado.view');
});

// catalogos
Route::group(['middleware' => ['auth', 'catalogos']], function () {
    // catalogos index
    Route::get('/catalogos/area', 'CatalogosController@areaIndex')->name('catalogos.area.index');
    Route::get('/catalogos/bolsa', 'CatalogosController@bolsaIndex')->name('catalogos.bolsa.index');
    Route::get('/catalogos/competencias', 'CatalogosController@competenciaIndex')->name('catalogos.competencias.index');
    Route::get('/catalogos/empresa', 'CatalogosController@empresaIndex')->name('catalogos.empresa.index');
    Route::get('/catalogos/escolaridad', 'CatalogosController@escolaridadIndex')->name('catalogos.escolaridad.index');
    Route::get('/catalogos/etdoCivil', 'CatalogosController@estadoCivilIndex')->name('catalogos.etdoCivil.index');
    Route::get('/catalogos/etdoEscolar', 'CatalogosController@etdoEscolaridadIndex')->name('catalogos.etdoEscolar.index');
    Route::get('/catalogos/etdoMotivo', 'CatalogosController@etdoMotivoIndex')->name('catalogos.etdoMotivo.index');
    Route::get('/catalogos/puesto', 'CatalogosController@puestoIndex')->name('catalogos.puesto.index');
    Route::get('/catalogos/tipoPuesto', 'CatalogosController@tipoPuestoIndex')->name('catalogos.tipoPuesto.index');
    Route::get('/catalogos/pais', 'CatalogosController@paisIndex')->name('catalogos.pais.index');
    Route::get('/catalogos/tipoDocumento', 'CatalogosController@tipoDocumentoIndex')->name('catalogos.tipoDocumento.index');
    Route::get('/catalogos/vacante', 'CatalogosController@vacanteIndex')->name('catalogos.vacante.index');
// catalogos delete
    Route::get('/catalogos/destroy', 'CatalogosController@destroy')->name('catalogos.destroy');
// catalogos save
    Route::post('/catalogos/saveArea', 'CatalogosController@saveArea')->name('catalogos.saveArea');
    Route::post('/catalogos/saveBolsa', 'CatalogosController@saveBolsa')->name('catalogos.saveBolsa');
    Route::post('/catalogos/saveCompetencia', 'CatalogosController@saveCompetencia')->name('catalogos.saveCompetencia');
    Route::post('/catalogos/saveEmpresa', 'CatalogosController@saveEmpresa')->name('catalogos.saveEmpresa');
    Route::post('/catalogos/saveEscolaridad', 'CatalogosController@saveEscolaridad')->name('catalogos.saveEscolaridad');
    Route::post('/catalogos/saveEtdoCivil', 'CatalogosController@saveEtdoCivil')->name('catalogos.saveEtdoCivil');
    Route::post('/catalogos/saveEtdoEscolar', 'CatalogosController@saveEtdoEscolar')->name('catalogos.saveEtdoEscolar');
    Route::post('/catalogos/saveEtdoMotivo', 'CatalogosController@saveEtdoMotivo')->name('catalogos.saveEtdoMotivo');
    Route::post('/catalogos/savePuesto', 'CatalogosController@savePuesto')->name('catalogos.savePuesto');
    Route::post('/catalogos/saveTipoPuesto', 'CatalogosController@saveTipoPuesto')->name('catalogos.saveTipoPuesto');
    Route::post('/catalogos/savePais', 'CatalogosController@savePais')->name('catalogos.savePais');
    Route::post('/catalogos/saveTipoDocumento', 'CatalogosController@saveTipoDocumento')->name('catalogos.saveTipoDocumento');
// catalogos update
    Route::post('/catalogos/updateArea', 'CatalogosController@updateArea')->name('catalogos.updateArea');
    Route::post('/catalogos/updateBolsa', 'CatalogosController@updateBolsa')->name('catalogos.updateBolsa');
    Route::post('/catalogos/updateCompetencia', 'CatalogosController@updateCompetencia')->name('catalogos.updateCompetencia');
    Route::post('/catalogos/updateEmpresa', 'CatalogosController@updateEmpresa')->name('catalogos.updateEmpresa');
    Route::post('/catalogos/updateEscolaridad', 'CatalogosController@updateEscolaridad')->name('catalogos.updateEscolaridad');
    Route::post('/catalogos/updateEtdoCivil', 'CatalogosController@updateEtdoCivil')->name('catalogos.updateEtdoCivil');
    Route::post('/catalogos/updateEtdoEscolar', 'CatalogosController@updateEtdoEscolar')->name('catalogos.updateEtdoEscolar');
    Route::post('/catalogos/updateEtdoMotivo', 'CatalogosController@updateEtdoMotivo')->name('catalogos.updateEtdoMotivo');
    Route::post('/catalogos/updatePuesto', 'CatalogosController@updatePuesto')->name('catalogos.updatePuesto');
    Route::post('/catalogos/updateTipoPuesto', 'CatalogosController@updateTipoPuesto')->name('catalogos.updateTipoPuesto');
});
//capacitacion
Route::get('/capacitacion','Rh\CapacitacionController@index')->name('capacitacion.index');
Route::post('/capacitcacion/store','Rh\CapacitacionController@store')->name('capacitacion.store');
Route::post('/capacitacion/update', 'Rh\CapacitacionController@update')->name('capacitacion.update');
//administrativo
Route::post('/administrativo/store','Rh\AdministrativoController@store')->name('administrativo.store');
//asignaciones
Route::get('/asignacion','Rh\AsignacionController@index')->name('asignacion.index');
Route::post('/asignacion/administrativo/save','Rh\AsignacionController@saveAdministrativo')->name('asignacion.administrativo.save');
Route::post('/asignacion/ejecutivo/save','Rh\AsignacionController@saveEjecutivo')->name('asignacion.ejecutivo.save');


////////////////////INVENTARIO///////////////
Route::group(['middleware' => ['auth', 'inventarios']], function () {
Route::get('/inventario/ejecutivos', 'Inventario\InventarioEmpleadoController@index')->name('inventario.index');
Route::get('/inventario/create/{id}', 'Inventario\InventarioEmpleadoController@create')->name('inventario.create');
Route::get('/inventario/store', 'Inventario\InventarioEmpleadoController@store')->name('inventario.store');
Route::post('/inventario/asignar', 'Inventario\InventarioEmpleadoController@asignar')->name('inventario.asignar');


Route::get('/inventario/administrativos', 'Inventario\InventarioAdminisController@index')->name('inventario.admin.index');
Route::get('/inventario/administrativo/{id}', 'Inventario\InventarioAdminisController@create')->name('inventario.createad');

Route::post('/inventario/asignar', 'Inventario\InventarioAdminisController@asignar')->name('inventario.asignar');

Route::get('/inventario/monitor', 'Inventario\MonitorController@monitorIndex')->name('inventario.monitor.index');
Route::post('/inventario/saveMonitor', 'Inventario\MonitorController@saveMonitor')->name('monitor.saveMonitor');
Route::post('/inventario/updateMonitor', 'Inventario\MonitorController@updateMonitor')->name('monitor.updateMonitor');
Route::get('/inventario/destroyMonitor', 'Inventario\MonitorController@destroyMonitor')->name('monitor.destroyMonitor');

Route::get('/inventario/cpu', 'Inventario\CpuController@cpuIndex')->name('inventario.cpu.index');
Route::post('/inventario/saveCpu', 'Inventario\CpuController@saveCpu')->name('cpu.saveCpu');
Route::post('/inventario/updateCpu', 'Inventario\CpuController@updateCpu')->name('cpu.updateCpu');
Route::get('/inventario/destroyCpu', 'Inventario\CpuController@destroyCpu')->name('cpu.destroy');

Route::get('/inventario/teclado', 'Inventario\TecladoController@tecladoIndex')->name('inventario.teclado.index');
Route::post('/inventario/updateTeclado', 'Inventario\TecladoController@updateTeclado')->name('teclado.updateTeclado');
Route::post('/inventario/saveTeclado', 'Inventario\TecladoController@saveTeclado')->name('teclado.saveTeclado');
Route::get('/inventario/destroyTeclado', 'Inventario\TecladoController@destroyTeclado')->name('teclado.destroyTeclado'); 

Route::get('/inventario/mouse', 'Inventario\MouseController@mouseIndex')->name('inventario.mouse.index');
Route::post('/inventario/saveMouse','Inventario\MouseController@saveMouse')->name('mouse.saveMouse');
Route::post('/inventario/updateMouse','Inventario\MouseController@updateMouse')->name('mouse.updateMouse');
Route::get('/inventario/destroyMouse','Inventario\MouseController@destroyMouse')->name('mouse.destroyMouse');


Route::get('/inventario/tablet', 'Inventario\TabletController@tabletIndex')->name('inventario.tablet.index');
Route::post('/inventario/saveTablet','Inventario\TabletController@saveTablet')->name('tablet.saveTablet');
Route::post('/inventario/updateTablet','Inventario\TabletController@updateTablet')->name('tablet.updateTablet');
Route::get('/inventario/destroyTablet','Inventario\TabletController@destroyTablet')->name('tablet.destroyTablet');

Route::get('/inventario/diadema', 'Inventario\DiademaController@diademaIndex')->name('inventario.diadema.index');
Route::post('/inventario/saveDiadema','Inventario\DiademaController@saveDiadema')->name('diadema.saveDiadema');
Route::post('/inventario/updateDiadema','Inventario\DiademaController@updateDiadema')->name('diadema.updateDiadema');
Route::get('/inventario/destroyDiadema','Inventario\DiademaController@destroyDiadema')->name('diadema.destroyDiadema');
});
/////////////////////////////////////////////

//reportes
Route::group(['middleware' => ['auth', 'reportes']], function () {
    Route::post('/precandidato_reporte', 'Reportes\PrecandidatoReporteController@index')->name('precandidatoreporte.index');
Route::get('/precandidato_reporte/indexreportefechas', 'Reportes\PrecandidatoReporteController@indexreportefechas')->name('precandidato_reporte.indexreportefechas');
Route::get('/candidatos_reporte/indexreportefechas', 'Reportes\CandidatoReporteController@indexreportefechas')->name('candidatos_reporte.indexreportefechas');
Route::get('/empleado_reporte/indexreporteantiguedad', 'Reportes\AntiguedadReporteController@indexReporteAntiguedad')->name('empleado_reporte.indexreporteantiguedad');
Route::get('/empleado_reporte/indexreportedemografico', 'Reportes\ReporteDemograficoController@indexReporteFechas')->name('empleado_reporte.indexreportedemografico');
Route::get('/empleado_reporte/indexreporteimss', 'Reportes\ImssViewController@indexReporteImss')->name('empleado_reporte.indexreporteimss');
Route::get('/empleado_reporte/indexreporteseguimiento', 'Reportes\SeguimientoReporteController@indexReporteSeguimiento')->name('empleado_reporte.indexreporteseguimiento');
Route::get('/empleado_reporte/indexreportebajaempleado', 'Reportes\BajasEmpleadoReporteController@indexReporteBajasEmpleado')->name('empleado_reporte.indexreportebajaempleado');
Route::get('/empleado_reporte/indexreporteempleado', 'Reportes\EmpleadoReporteController@indexReporteEmpleado')->name('empleado_reporte.indexreporteempleado');
});

//super usuario
Route::get('/superUsuario/grupos', 'SuperUsuario\SuperUsuarioController@indexGrupo')->name('superUsuario.grupoIndex');
Route::post('/superUsuario/grupos/save', 'SuperUsuario\SuperUsuarioController@saveGrupo')->name('superUsuario.grupoSave');
Route::get('/superUsuario/permisos/{id}', 'SuperUsuario\SuperUsuarioController@permisos')->name('superUsuario.permisos');
Route::post('/superUsuario/permisos', 'SuperUsuario\SuperUsuarioController@savePermisos')->name('superUsuario.savePermisos');
// excel
Route::get('/importIndex', 'ExcelController@index')->name('import.index');
Route::post('/importIndex', 'ExcelController@action')->name('import.upload');

Route::get('/ajax-metroCercano', function (){
   $medioId = Input::get('medioId');
   $estaciones = \App\EstacionesLineas::where('id_transporte',$medioId)->orderBy('estacion')->get();
   return Response::json($estaciones);
})->name('metro.ajax');

Route::get('/ajax-linea', 'Rh\PreCandidatoController@getLinea');
Route::get('/ajax-metroCercano', 'Rh\PreCandidatoController@getMetro');
Route::get('/ajax-buscarCp', 'Rh\PreCandidatoController@getCp');
Route::get('/ajax-extension', 'Rh\AsignacionController@getExtension');
Auth::routes();

